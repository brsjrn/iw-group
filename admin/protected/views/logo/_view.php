<?php
/* @var $this LogoController */
/* @var $data Logo */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->image_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordre')); ?>:</b>
	<?php echo CHtml::encode($data->ordre); ?>
	<br />


</div>