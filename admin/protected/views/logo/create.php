<?php
/* @var $this LogoController */
/* @var $model Logo */

$this->breadcrumbs=array(
	'Logos'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Logo', 'url'=>array('admin')),
);
?>

<div id="top_admin_model">
	<h1>Create Logo<span class="back_admin"><?php echo CHtml::link('back', array('Logo/admin')); ?></span></h1>

	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">create</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array('model'=>$model, 'listOrders' => $listOrders)); ?></div>