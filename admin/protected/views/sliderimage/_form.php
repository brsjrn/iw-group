<?php
/* @var $this SliderimageController */
/* @var $model Sliderimage */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'sliderimage-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

			<div class="row hidden_row">
				<?php echo $form->labelEx($model,'ref_uniqId'); ?>
				<?php echo $form->textField($model,'ref_uniqId', array(
			        'value'=>$parent->uniqId
				)); ?>
				<?php echo $form->error($model,'ref_uniqId'); ?>
				<div class="clear"></div>
			</div>

			<div class="row">
				<?php echo $form->labelEx($model,'image'); ?>
				<?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'image'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Sliderimage/' . $model->image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>
					<div class="row hidden_row">
				<?php echo $form->labelEx($model,'image_mobile'); ?>
				<?php echo $form->fileField($model,'image_mobile',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'image_mobile'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->image_mobile != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Sliderimage/' . $model->image_mobile,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>
			        			
										<div class="row">
						<?php echo $form->labelEx($model,'order_slide'); ?>
						<?php echo $form->textField($model,'order_slide'); ?>
						<?php echo $form->error($model,'order_slide'); ?>
						<div class="clear"></div>
					</div>
						<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	    
</script>
