<?php
/* @var $this SliderimageController */
/* @var $model Sliderimage */

$this->breadcrumbs=array(
	'Sliderimages'=>array('admin'),
	$model->id,
);
?>

<div id="top_admin_model">
	<h1>Sliderimage #<?php echo $model->id; ?><span class="back_admin"><?php echo CHtml::link('back', array('Sliderimage/admin')); ?></span></h1>
	
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Sliderimage/update', 'id'=>$model->id), array('class'=>'btn_model glyphicon glyphicon-edit')); ?>		<?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>		<?php echo CHtml::link('', array('Sliderimage/create'), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
<div class="view_attribute"><div class="view_attribute_name">id</div><div class="view_attribute_value"><?php echo isset($model->id) ? $model->id : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">image</div><div class="view_attribute_value view_attribute_image"><?php echo isset($model->image) ? CHtml::image("../../../images/Sliderimage/". $model->image,"image",array("class"=>"image_preview")):"-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">image_mobile</div><div class="view_attribute_value view_attribute_image"><?php echo isset($model->image_mobile) ? CHtml::image("../../../images/Sliderimage/". $model->image_mobile,"image",array("class"=>"image_preview")):"-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">ref_uniqId</div><div class="view_attribute_value"><?php echo isset($model->ref_uniqId) ? $model->ref_uniqId : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">order</div><div class="view_attribute_value"><?php echo isset($model->order) ? $model->order : "-"; ?></div><div class="clear"></div></div></div>