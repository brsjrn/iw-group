<?php
/* @var $this SliderimageController */
/* @var $model Sliderimage */

if($parentType == 'work') {
	$this->breadcrumbs=array(
		'Slider image'=>array('Work/admin'),
		$parent->title=>array('Work/view', 'id'=>$parent->id),
	    'Slider image',
	);
} else {
	$this->breadcrumbs=array(
		'Slider image'=>array('News/admin'),
		$parent->title=>array('News/view', 'id'=>$parent->id),
	    'Slider image',
	);
}

$this->menu=array(
	array('label'=>'Add slider image', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#sliderimage-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Images
		<span class="back_admin">
			<?php 
				if($parentType == 'work') {
					echo CHtml::link($parent->title, array('Work/view', 'id'=>$parent->id)); 
				} else {
					echo CHtml::link($parent->title, array('News/view', 'id'=>$parent->id)); 
				}
			?>
		</span>
	</h1>

	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Sliderimage/create', 'refModelId'=>$parent->uniqId), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sliderimage-grid',
	'dataProvider'=>$model->getRefModel(array('condition'=>'ref_uniqId='. $parent->uniqId)),
	'filter'=>$model,
	'columns'=>array(
		array(
            'name'=>'image',
            'type'=>'html',
            'header' => 'Image',
            'value'=>'CHtml::image(Yii::app()->baseUrl . "/../images/Sliderimage/" . $data->image,"",array("style"=>"width:auto;height:200px;"))'
        ),
		//'image',
		//'image_mobile',
		//'ref_uniqId',
		'order_slide',
		array(
			'class'=>'CButtonColumn',
			//'template'=>'{delete}',
		),
	),
)); ?>
</div>