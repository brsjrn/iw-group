<?php
/* @var $this SliderimageController */
/* @var $data Sliderimage */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image')); ?>:</b>
	<?php echo CHtml::encode($data->image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image_mobile')); ?>:</b>
	<?php echo CHtml::encode($data->image_mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ref_uniqId')); ?>:</b>
	<?php echo CHtml::encode($data->ref_uniqId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order')); ?>:</b>
	<?php echo CHtml::encode($data->order); ?>
	<br />


</div>