<?php
/* @var $this SliderimageController */
/* @var $model Sliderimage */

if($parentType == 'work') {
	$this->breadcrumbs=array(
	    'Work'=>array('Work/admin'),
		$parent->title=>array('Work/view', 'id'=>$parent->id),
		'Slider image'=>array('adminById', 'refModelId'=>$parent->uniqId),
	    'Add',
	);
} else {
	$this->breadcrumbs=array(
	    'News'=>array('News/admin'),
		$parent->title=>array('News/view', 'id'=>$parent->id),
		'Slider image'=>array('adminById', 'refModelId'=>$parent->uniqId),
	    'Add',
	);
}

$this->menu=array(
	array('label'=>'Manage slider images', 'url'=>array('admin')),
);
?>

<div id="top_admin_model">
	<h1>Add slider images
		<span class="back_admin">
			<?php 
				echo CHtml::link('retour', array('Sliderimage/adminById', 'refModelId'=>$parent->uniqId));
			?>
		</span></h1>

	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">create</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array('model'=>$model, 'parent'=>$parent, 'parentType' => $parentType)); ?></div>