<?php
/* @var $this SliderimageController */
/* @var $model Sliderimage */

if($parentType == 'work') {
	$this->breadcrumbs=array(
	    'Work'=>array('Work/admin'),
		$parent->title=>array('Work/view', 'id'=>$parent->id),
		'Slider image'=>array('adminById', 'refModelId'=>$parent->uniqId),
	    'Update',
	);
} else {
	$this->breadcrumbs=array(
	    'News'=>array('News/admin'),
		$parent->title=>array('News/view', 'id'=>$parent->id),
		'Slider image'=>array('adminById', 'refModelId'=>$parent->uniqId),
	    'Update',
	);
}
?>

<div id="top_admin_model">
	<h1>Update slider image #<?php echo $model->id; ?><span class="back_admin"><?php echo CHtml::link('retour', array('Sliderimage/adminById', 'refModelId'=>$parent->uniqId)); ?></span></h1>
	
	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">save</div>
		<?php echo CHtml::link('', array('Sliderimage/view', 'id'=>$model->id), array('class'=>'btn_model glyphicon glyphicon-eye-open')); ?>		<?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>		<?php echo CHtml::link('', array('Sliderimage/create'), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array('model'=>$model, 'parent'=>$parent, 'parentType' => $parentType)); ?></div>