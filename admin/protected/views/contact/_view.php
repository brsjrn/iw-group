<?php
/* @var $this ContactController */
/* @var $data Contact */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city')); ?>:</b>
	<?php echo CHtml::encode($data->city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adress1')); ?>:</b>
	<?php echo CHtml::encode($data->adress1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('adress2')); ?>:</b>
	<?php echo CHtml::encode($data->adress2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordre')); ?>:</b>
	<?php echo CHtml::encode($data->ordre); ?>
	<br />


</div>