<?php
/* @var $this ContactController */
/* @var $model Contact */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contact-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

					<div class="row">
						<?php echo $form->labelEx($model,'city'); ?>
						<?php echo $form->textField($model,'city',array('size'=>60,'maxlength'=>63)); ?>
						<?php echo $form->error($model,'city'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'adress1'); ?>
						<?php echo $form->textField($model,'adress1',array('size'=>60,'maxlength'=>255)); ?>
						<?php echo $form->error($model,'adress1'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'adress2'); ?>
						<?php echo $form->textField($model,'adress2',array('size'=>60,'maxlength'=>255)); ?>
						<?php echo $form->error($model,'adress2'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'phone'); ?>
						<?php echo $form->textField($model,'phone',array('size'=>31,'maxlength'=>31)); ?>
						<?php echo $form->error($model,'phone'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'ordre'); ?>
						<?php 
										if($model->isNewRecord) {
					                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array(count($listOrders)=>array('selected'=>true))));
					                    } else {
					                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array($model->ordre=>array('selected'=>true))));
					                    }
									?>						<?php echo $form->error($model,'ordre'); ?>
						<div class="clear"></div>
					</div>
					<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	    
</script>
