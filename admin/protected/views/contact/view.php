<?php
/* @var $this ContactController */
/* @var $model Contact */

$this->breadcrumbs=array(
	'Contacts'=>array('admin'),
	$model->id,
);
?>

<div id="top_admin_model">
	<h1>Contact #<?php echo $model->id; ?><span class="back_admin"><?php echo CHtml::link('back', array('Contact/admin')); ?></span></h1>
	
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Contact/update', 'id'=>$model->id), array('class'=>'btn_model glyphicon glyphicon-edit')); ?>		<?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>		<?php echo CHtml::link('', array('Contact/create'), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
<div class="view_attribute"><div class="view_attribute_name">id</div><div class="view_attribute_value"><?php echo isset($model->id) ? $model->id : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">city</div><div class="view_attribute_value"><?php echo isset($model->city) ? $model->city : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">adress1</div><div class="view_attribute_value"><?php echo isset($model->adress1) ? $model->adress1 : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">adress2</div><div class="view_attribute_value"><?php echo isset($model->adress2) ? $model->adress2 : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">phone</div><div class="view_attribute_value"><?php echo isset($model->phone) ? $model->phone : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">ordre</div><div class="view_attribute_value"><?php echo isset($model->ordre) ? $model->ordre : "-"; ?></div><div class="clear"></div></div></div>