<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'shartDescription_text'); ?>
		<?php echo $form->textArea($model,'shartDescription_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'refCategory'); ?>
		<?php echo $form->textField($model,'refCategory'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'refAutor'); ?>
		<?php echo $form->textField($model,'refAutor'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dispatchCover_image'); ?>
		<?php echo $form->textField($model,'dispatchCover_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dispatchCoverMobile_mage'); ?>
		<?php echo $form->textField($model,'dispatchCoverMobile_mage',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'insideCover_image'); ?>
		<?php echo $form->textField($model,'insideCover_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'insideCoverMobile_image'); ?>
		<?php echo $form->textField($model,'insideCoverMobile_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'text_1'); ?>
		<?php echo $form->textArea($model,'text_1',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'text_2'); ?>
		<?php echo $form->textArea($model,'text_2',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'link_url'); ?>
		<?php echo $form->textField($model,'link_url',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'link_text'); ?>
		<?php echo $form->textArea($model,'link_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ordre'); ?>
		<?php echo $form->textField($model,'ordre'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->