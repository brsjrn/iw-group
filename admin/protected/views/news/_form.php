<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'news-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

					<div class="row">
						<?php echo $form->labelEx($model,'title'); ?>
						<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>127)); ?>
						<?php echo $form->error($model,'title'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'date'); ?>
						<?php 
										$this->widget('zii.widgets.jui.CJuiDatePicker', array(
					                        'model' => $model,
					                        'attribute' => 'date',
					                        'language' => 'en',
					                        'options' => array(
					                            'showOn' => 'both', 
					                            'dateFormat' => 'yy-mm-dd',
					                        ),
					                        'htmlOptions' => array(
					                            'size' => '10',         // textField size
					                            'maxlength' => '10',    // textField maxlength
					                        ),
					                    ));
									?>						<?php echo $form->error($model,'date'); ?>
						<div class="clear"></div>
					</div>
								<div class="row">
					<?php echo $form->labelEx($model,'shartDescription_text'); ?>
					<?php echo $form->textArea($model,'shartDescription_text',array('id'=>'editorshartDescription_text')); ?>
					<?php echo $form->error($model,'shartDescription_text'); ?>
					<div class="clear"></div>
				</div>
								<div class="row">
						<?php echo $form->labelEx($model,'refCategory'); ?>
						<?php echo $form->dropDownList($model,'refCategory', $listCategories,
						    array(
						        'empty'=>'-- Select a category'
						    )
						); ?>
						<?php echo $form->error($model,'refCategory'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'refAutor'); ?>
						<?php echo $form->dropDownList($model,'refAutor', $listAuthors,
						    array(
						        'empty'=>'-- Select an author'
						    )
						); ?>
						<?php echo $form->error($model,'refAutor'); ?>
						<div class="clear"></div>
					</div>
								<div class="row">
				<?php echo $form->labelEx($model,'dispatchCover_image'); ?>
				<?php echo $form->fileField($model,'dispatchCover_image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'dispatchCover_image'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->dispatchCover_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/News/' . $model->dispatchCover_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>
							<div class="row hidden_row">
						<?php echo $form->labelEx($model,'dispatchCoverMobile_mage'); ?>
						<?php echo $form->textField($model,'dispatchCoverMobile_mage',array('size'=>60,'maxlength'=>255)); ?>
						<?php echo $form->error($model,'dispatchCoverMobile_mage'); ?>
						<div class="clear"></div>
					</div>
								<div class="row">
				<?php echo $form->labelEx($model,'insideCover_image'); ?>
				<?php echo $form->fileField($model,'insideCover_image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'insideCover_image'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->insideCover_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/News/' . $model->insideCover_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>
					<div class="row hidden_row">
				<?php echo $form->labelEx($model,'insideCoverMobile_image'); ?>
				<?php echo $form->fileField($model,'insideCoverMobile_image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'insideCoverMobile_image'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->insideCoverMobile_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/News/' . $model->insideCoverMobile_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>
						<div class="row">
					<?php echo $form->labelEx($model,'text_1'); ?>
					<?php echo $form->textArea($model,'text_1',array('id'=>'editortext_1')); ?>
					<?php echo $form->error($model,'text_1'); ?>
					<div class="clear"></div>
				</div>
					
				<!-- TEXT 2 -->
				<div class="row">
					<label>Has text 2</label>
					<input type="checkbox" id="checkText2" class="checkHasTarget">
				</div>

				<div class="hidden-area" id="checkText2_target">
					<div class="row">
						<?php echo $form->labelEx($model,'text_2'); ?>
						<?php echo $form->textArea($model,'text_2',array('id'=>'editortext_2', 'class' => 'hidden-elt')); ?>
						<?php echo $form->error($model,'text_2'); ?>
						<div class="clear"></div>
					</div>
				</div>
				<!-- EN TEXT 2 -->

				<!-- TEXT 2 -->
				<div class="row">
					<label>Has link</label>
					<input type="checkbox" id="checkLink" class="checkHasTarget">
				</div>

				<div class="hidden-area" id="checkLink_target">

					<div class="row">
						<?php echo $form->labelEx($model,'link_url'); ?>
						<?php echo $form->textField($model,'link_url',array('size'=>60,'maxlength'=>255, 'class' => 'hidden-elt')); ?>
						<?php echo $form->error($model,'link_url'); ?>
						<div class="clear"></div>
					</div>

					<div class="row">
						<?php echo $form->labelEx($model,'link_text'); ?>
						<?php echo $form->textArea($model,'link_text',array('id'=>'editorlink_text', 'class' => 'hidden-elt')); ?>
						<?php echo $form->error($model,'link_text'); ?>
						<div class="clear"></div>
					</div>
				</div>

					<div class="row">
						<?php echo $form->labelEx($model,'ordre'); ?>
						<?php 
										if($model->isNewRecord) {
					                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array(count($listOrders)=>array('selected'=>true))));
					                    } else {
					                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array($model->ordre=>array('selected'=>true))));
					                    }
									?>						<?php echo $form->error($model,'ordre'); ?>
						<div class="clear"></div>
					</div>

					<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	
				CKEDITOR.replace( 'editorshartDescription_text', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			
				CKEDITOR.replace( 'editortext_1', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			
				CKEDITOR.replace( 'editortext_2', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			
				CKEDITOR.replace( 'editorlink_text', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			    
</script>
