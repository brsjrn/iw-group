<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'News'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create News', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#news-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>News</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('News/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		array(
            'header'=>'Slider',
            'value' => 'CHtml::link("Manage", Yii::app()->createUrl("Sliderimage/adminById",array("refModelId"=>$data->uniqId)))',
            'type'  => 'raw',
        ),
		/*'date',
		'shartDescription_text',
		'refCategory',
		'refAutor',
		'dispatchCover_image',
		'dispatchCoverMobile_mage',
		'insideCover_image',
		'insideCoverMobile_image',
		'text_1',
		'text_2',
		'link_url',
		'link_text',
		'ordre',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>