<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'News'=>array('admin'),
	$model->title,
);
?>

<div id="top_admin_model">
	<h1>News #<?php echo $model->id; ?><span class="back_admin"><?php echo CHtml::link('back', array('News/admin')); ?></span></h1>
	
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('News/update', 'id'=>$model->id), array('class'=>'btn_model glyphicon glyphicon-edit')); ?>		<?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>		<?php echo CHtml::link('', array('News/create'), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="ressources">
    <ul>
        <li><?php echo CHtml::link('Slider images ('. $nbChildren .')', array('Sliderimage/adminById', 'refModelId'=>$model->id)); ?></li>
        <div class="clear"></div>
    </ul>
</div>

<div id="content_admin_model">
<div class="view_attribute"><div class="view_attribute_name">id</div><div class="view_attribute_value"><?php echo isset($model->id) ? $model->id : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">title</div><div class="view_attribute_value"><?php echo isset($model->title) ? $model->title : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">date</div><div class="view_attribute_value"><?php echo isset($model->date) ? $model->date : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">shartDescription_text</div><div class="view_attribute_value view_attribute_text"><?php echo isset($model->shartDescription_text) ? $model->shartDescription_text : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">refCategory</div><div class="view_attribute_value"><?php echo isset($model->refCategory) ? $model->refCategory : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">refAutor</div><div class="view_attribute_value"><?php echo isset($model->refAutor) ? $model->refAutor : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">dispatchCover_image</div><div class="view_attribute_value view_attribute_image"><?php echo isset($model->dispatchCover_image) ? CHtml::image("../../../images/News/". $model->dispatchCover_image,"image",array("class"=>"image_preview")):"-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">dispatchCoverMobile_mage</div><div class="view_attribute_value"><?php echo isset($model->dispatchCoverMobile_mage) ? $model->dispatchCoverMobile_mage : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">insideCover_image</div><div class="view_attribute_value view_attribute_image"><?php echo isset($model->insideCover_image) ? CHtml::image("../../../images/News/". $model->insideCover_image,"image",array("class"=>"image_preview")):"-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">insideCoverMobile_image</div><div class="view_attribute_value view_attribute_image"><?php echo isset($model->insideCoverMobile_image) ? CHtml::image("../../../images/News/". $model->insideCoverMobile_image,"image",array("class"=>"image_preview")):"-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">text_1</div><div class="view_attribute_value view_attribute_text"><?php echo isset($model->text_1) ? $model->text_1 : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">text_2</div><div class="view_attribute_value view_attribute_text"><?php echo isset($model->text_2) ? $model->text_2 : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">link_url</div><div class="view_attribute_value"><?php echo isset($model->link_url) ? $model->link_url : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">link_text</div><div class="view_attribute_value view_attribute_text"><?php echo isset($model->link_text) ? $model->link_text : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">ordre</div><div class="view_attribute_value"><?php echo isset($model->ordre) ? $model->ordre : "-"; ?></div><div class="clear"></div></div></div>