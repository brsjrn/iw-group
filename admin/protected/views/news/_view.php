<?php
/* @var $this NewsController */
/* @var $data News */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shartDescription_text')); ?>:</b>
	<?php echo CHtml::encode($data->shartDescription_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('refCategory')); ?>:</b>
	<?php echo CHtml::encode($data->refCategory); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('refAutor')); ?>:</b>
	<?php echo CHtml::encode($data->refAutor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dispatchCover_image')); ?>:</b>
	<?php echo CHtml::encode($data->dispatchCover_image); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('dispatchCoverMobile_mage')); ?>:</b>
	<?php echo CHtml::encode($data->dispatchCoverMobile_mage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insideCover_image')); ?>:</b>
	<?php echo CHtml::encode($data->insideCover_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insideCoverMobile_image')); ?>:</b>
	<?php echo CHtml::encode($data->insideCoverMobile_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_1')); ?>:</b>
	<?php echo CHtml::encode($data->text_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text_2')); ?>:</b>
	<?php echo CHtml::encode($data->text_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('link_url')); ?>:</b>
	<?php echo CHtml::encode($data->link_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('link_text')); ?>:</b>
	<?php echo CHtml::encode($data->link_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordre')); ?>:</b>
	<?php echo CHtml::encode($data->ordre); ?>
	<br />

	*/ ?>

</div>