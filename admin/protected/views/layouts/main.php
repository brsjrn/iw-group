<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

    <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lobster+Two:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,700' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/reset.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/fonts.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap_reset.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
        <?php 
            /* Load jQuery & JS files */
            Yii::app()->clientScript->registerCoreScript('jquery'); 
            
            $baseUrl = Yii::app()->baseUrl; 
            $cs = Yii::app()->getClientScript();
            $cs->registerScriptFile($baseUrl.'/js/global_script.js');
        ?>

	<div id="header">
                <div id="login">
                    <?php 
                    if(Yii::app()->user->isGuest) {
                        echo CHtml::link('<span class="glyphicon glyphicon-log-in"></span>', CController::createUrl('/site/login'));
                    } else {
                        echo CHtml::link('<span class="glyphicon glyphicon-log-out"></span>', CController::createUrl('/site/logout'));
                    }
                    ?>
                </div>
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->
        
        <div id="sub_header">
        <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
        </div>
        
        <div id="middle">
            <?php if (!Yii::app()->user->isGuest) { ?>
            <div id="menu_left">
                <ul id="nav_site">
                    <?php 
                        /* echo CHtml::link('<img src="'. $baseUrl .'/images/theme/home.png">', CController::createUrl('/site/index'), array('id'=>'home_link')); */ 
                        echo CHtml::link('<span class="glyphicon glyphicon-home"></span>', CController::createUrl('/site/index'), array('id'=>'home_link'));
                    ?>
                </ul>

                <ul class="nav_model">
                    <?php $this->widget('zii.widgets.CMenu',array(
                        'items'=>array(
                            array(
                                'label'=>'Blog',
                                'url'=> $baseUrl .'/../blog/wp-admin',
                                'visible'=>!Yii::app()->user->isGuest,
                                'linkOptions' => array('target'=>'_blank')
                            ),

                        ),
                    )); ?>
                </ul>

                <ul class="nav_model">
                    <?php $this->widget('zii.widgets.CMenu',array(
                        'items'=>array(
                            array(
                                'label'=>'Works',
                                'url'=>array('/Work/admin'),
                                'visible'=>!Yii::app()->user->isGuest
                            ),

                        ),
                    )); ?>
                </ul>

                <!--
                <ul class="nav_model">
                    <?php $this->widget('zii.widgets.CMenu',array(
                        'items'=>array(
                            array(
                                'label'=>'News',
                                'url'=>array('/News/admin'),
                                'visible'=>!Yii::app()->user->isGuest
                            ),
                            array(
                                'label'=>'News categories',
                                'url'=>array('/Newscategory/admin'),
                                'visible'=>!Yii::app()->user->isGuest
                            ),
                            array(
                                'label'=>'Authors',
                                'url'=>array('/Author/admin'),
                                'visible'=>!Yii::app()->user->isGuest
                            ),
                        ),
                    )); ?>
                </ul>
                -->

                <ul class="nav_model">
                    <?php $this->widget('zii.widgets.CMenu',array(
                        'items'=>array(
                            array(
                                'label'=>'About logo',
                                'url'=>array('/Logo/admin'),
                                'visible'=>!Yii::app()->user->isGuest
                            ),
                            array(
                                'label'=>'About persons',
                                'url'=>array('/Person/admin'),
                                'visible'=>!Yii::app()->user->isGuest
                            ),
                            array(
                                'label'=>'About services',
                                'url'=>array('/Service/admin'),
                                'visible'=>!Yii::app()->user->isGuest
                            ),
                           array(
                               'label'=>'About slider',
                               'url'=>array('/Slideabout/admin'),
                               'visible'=>!Yii::app()->user->isGuest
                           ),
                        ),
                    )); ?>
                </ul>

                <ul class="nav_model">
                    <?php $this->widget('zii.widgets.CMenu',array(
                        'items'=>array(
                            array(
                                'label'=>'Contact',
                                'url'=>array('/Contact/admin'),
                                'visible'=>!Yii::app()->user->isGuest
                            ),
                        ),
                    )); ?>
                </ul>

            </div>
            <?php } ?>
            
            <div id="content_right">
                <?php echo $content; ?>
            
                <div class="clear"></div>
            </div>
            
            
        </div>

	

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

    <!-- Scripts -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

</body>
</html>
