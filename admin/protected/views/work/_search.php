<?php
/* @var $this WorkController */
/* @var $model Work */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'title'); ?>
		<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>63)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->textField($model,'type',array('size'=>60,'maxlength'=>127)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'isStared'); ?>
		<?php echo $form->textField($model,'isStared'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'staredSubtitle'); ?>
		<?php echo $form->textField($model,'staredSubtitle',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'staredDescription_text'); ?>
		<?php echo $form->textArea($model,'staredDescription_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'staredOrder'); ?>
		<?php echo $form->textField($model,'staredOrder'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cover1_image'); ?>
		<?php echo $form->textField($model,'cover1_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cover1Mobile_image'); ?>
		<?php echo $form->textField($model,'cover1Mobile_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cover2_image'); ?>
		<?php echo $form->textField($model,'cover2_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'cover2Mobile_image'); ?>
		<?php echo $form->textField($model,'cover2Mobile_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content1Title'); ?>
		<?php echo $form->textField($model,'content1Title',array('size'=>60,'maxlength'=>63)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content1_text'); ?>
		<?php echo $form->textArea($model,'content1_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parallax_image'); ?>
		<?php echo $form->textField($model,'parallax_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'parallaxMobile_image'); ?>
		<?php echo $form->textField($model,'parallaxMobile_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content2Title'); ?>
		<?php echo $form->textField($model,'content2Title',array('size'=>60,'maxlength'=>63)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content2_text'); ?>
		<?php echo $form->textArea($model,'content2_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'video_image'); ?>
		<?php echo $form->textField($model,'video_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'videoMobile_image'); ?>
		<?php echo $form->textField($model,'videoMobile_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'video_url'); ?>
		<?php echo $form->textField($model,'video_url',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content3Title'); ?>
		<?php echo $form->textField($model,'content3Title',array('size'=>60,'maxlength'=>63)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content3_text'); ?>
		<?php echo $form->textArea($model,'content3_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contentBig_image'); ?>
		<?php echo $form->textField($model,'contentBig_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content4Title'); ?>
		<?php echo $form->textField($model,'content4Title',array('size'=>60,'maxlength'=>63)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'content4_text'); ?>
		<?php echo $form->textArea($model,'content4_text',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bottom_image'); ?>
		<?php echo $form->textField($model,'bottom_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'bottomMobile_image'); ?>
		<?php echo $form->textField($model,'bottomMobile_image',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ordre'); ?>
		<?php echo $form->textField($model,'ordre'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->