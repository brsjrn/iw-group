<?php
/* @var $this WorkController */
/* @var $model Work */

$this->breadcrumbs=array(
	'Works'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Work', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#work-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Works</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Work/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'work-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'type',
		'isStared',
		array(
            'header'=>'Slider',
            'value' => 'CHtml::link("Manage", Yii::app()->createUrl("Sliderimage/adminById",array("refModelId"=>$data->uniqId)))',
            'type'  => 'raw',
        ),
		/*'staredSubtitle',
		'staredDescription_text',
		'staredOrder',
		'cover1_image',
		'cover1Mobile_image',
		'cover2_image',
		'cover2Mobile_image',
		'content1Title',
		'content1_text',
		'parallax_image',
		'parallaxMobile_image',
		'content2Title',
		'content2_text',
		'video_image',
		'videoMobile_image',
		'video_url',
		'content3Title',
		'content3_text',
		'contentBig_image',
		'content4Title',
		'content4_text',
		'bottom_image',
		'bottomMobile_image',
		*/
		'ordre',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>