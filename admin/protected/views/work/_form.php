<?php
/* @var $this WorkController */
/* @var $model Work */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'work-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

					<div class="row">
						<?php echo $form->labelEx($model,'title'); ?>
						<?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>63)); ?>
						<?php echo $form->error($model,'title'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'type'); ?>
						<?php echo $form->textField($model,'type',array('size'=>60,'maxlength'=>127)); ?>
						<?php echo $form->error($model,'type'); ?>
						<div class="clear"></div>
					</div>

					<!-- IS STARED -->
					<div class="row">
						<?php echo $form->labelEx($model,'isStared'); ?>
						<?php echo $form->checkBox($model,'isStared', array('class' => 'checkHasTarget')); ?>
						<?php echo $form->error($model,'isStared'); ?>
						<div class="clear"></div>
					</div>
					
					<div class="hidden-area" id="Work_isStared_target">
						<div class="row">
							<?php echo $form->labelEx($model,'staredSubtitle'); ?>
							<?php echo $form->textArea($model,'staredSubtitle',array('id'=>'staredSubtitle', 'class' => 'hidden-elt')); ?>
							<?php echo $form->error($model,'staredSubtitle'); ?>
							<div class="clear"></div>
						</div>
						
						<div class="row">
							<?php echo $form->labelEx($model,'staredDescription_text'); ?>
							<?php echo $form->textArea($model,'staredDescription_text',array('id'=>'editorstaredDescription_text', 'class' => 'hidden-elt')); ?>
							<?php echo $form->error($model,'staredDescription_text'); ?>
							<div class="clear"></div>
						</div>

						<div class="row">
							<?php echo $form->labelEx($model,'staredOrder'); ?>
							<?php echo $form->textField($model,'staredOrder', array('class' => 'hidden-elt')); ?>
							<?php echo $form->error($model,'staredOrder'); ?>
							<div class="clear"></div>
						</div>
					</div>
				<!-- END STARED -->

					<div class="row">
						<?php echo $form->labelEx($model,'cover1_image'); ?>
						<?php echo $form->fileField($model,'cover1_image',array('size'=>60,'maxlength'=>255)); ?>
						<?php echo $form->error($model,'cover1_image'); ?>
					<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->cover1_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Work/' . $model->cover1_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>

    			<!--
				<div class="row">
					<?php echo $form->labelEx($model,'cover1Mobile_image'); ?>
					<?php echo $form->fileField($model,'cover1Mobile_image',array('size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'cover1Mobile_image'); ?>
					<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->cover1Mobile_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Work/' . $model->cover1Mobile_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			    </div>
			    -->

					<div class="row">
				<?php echo $form->labelEx($model,'cover2_image'); ?>
				<?php echo $form->fileField($model,'cover2_image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'cover2_image'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->cover2_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Work/' . $model->cover2_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>

				<div class="row">
					<?php echo $form->labelEx($model,'cover2Mobile_image'); ?>
					<?php echo $form->fileField($model,'cover2Mobile_image',array('size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'cover2Mobile_image'); ?>
					<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->cover2Mobile_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Work/' . $model->cover2Mobile_image,'image',array('class'=>'image_preview')); ?>
		        </div>
				        <?php } ?>
			        			</div>
							<div class="row">
						<?php echo $form->labelEx($model,'content1Title'); ?>
						<?php echo $form->textField($model,'content1Title',array('size'=>60,'maxlength'=>63)); ?>
						<?php echo $form->error($model,'content1Title'); ?>
						<div class="clear"></div>
					</div>
									<div class="row">
					<?php echo $form->labelEx($model,'content1_text'); ?>
					<?php echo $form->textArea($model,'content1_text',array('id'=>'editorcontent1_text')); ?>
					<?php echo $form->error($model,'content1_text'); ?>
					<div class="clear"></div>
				</div>
						<div class="row">
				<?php echo $form->labelEx($model,'parallax_image'); ?>
				<?php echo $form->fileField($model,'parallax_image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'parallax_image'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->parallax_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Work/' . $model->parallax_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>

    			<!--
				<div class="row">
					<?php echo $form->labelEx($model,'parallaxMobile_image'); ?>
					<?php echo $form->fileField($model,'parallaxMobile_image',array('size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'parallaxMobile_image'); ?>
					<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->parallaxMobile_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Work/' . $model->parallaxMobile_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
		    	</div>
		    	-->
							<div class="row">
						<?php echo $form->labelEx($model,'content2Title'); ?>
						<?php echo $form->textField($model,'content2Title',array('size'=>60,'maxlength'=>63)); ?>
						<?php echo $form->error($model,'content2Title'); ?>
						<div class="clear"></div>
					</div>
									<div class="row">
					<?php echo $form->labelEx($model,'content2_text'); ?>
					<?php echo $form->textArea($model,'content2_text',array('id'=>'editorcontent2_text')); ?>
					<?php echo $form->error($model,'content2_text'); ?>
					<div class="clear"></div>
				</div>

				<!-- VIDEO -->
				<div class="row">
					<label>Has video</label>
					<input type="checkbox" id="checkVideo" class="checkHasTarget">
				</div>

				<div class="hidden-area" id="checkVideo_target">

						<div class="row">
							<?php echo $form->labelEx($model,'video_image'); ?>
							<?php echo $form->fileField($model,'video_image',array('size'=>60,'maxlength'=>255)); ?>
							<?php echo $form->error($model,'video_image'); ?>
							<div class="clear"></div>
						
								<?php if(!$model->isNewRecord && $model->video_image != ''){ ?>
							        <div class='image_already_exist hidden-image-exist'>
							             <?php echo CHtml::image('../../../../images/Work/' . $model->video_image,'image',array('class'=>'image_preview')); ?>
							        </div>
						        <?php } ?>
				        </div>

				        <!--
						<div class="row">
							<?php echo $form->labelEx($model,'videoMobile_image'); ?>
							<?php echo $form->fileField($model,'videoMobile_image',array('size'=>60,'maxlength'=>255)); ?>
							<?php echo $form->error($model,'videoMobile_image'); ?>
							<div class="clear"></div>
						
							<?php if(!$model->isNewRecord && $model->videoMobile_image != ''){ ?>
						        <div class='image_already_exist hidden-image-exist'>
						             <?php echo CHtml::image('../../../../images/Work/' . $model->videoMobile_image,'image',array('class'=>'image_preview')); ?>
						        </div>
					        <?php } ?>
	        			</div>
	        			-->

						<div class="row">
							<?php echo $form->labelEx($model,'video_url'); ?>
							<?php echo $form->textField($model,'video_url',array('size'=>60,'maxlength'=>255, 'class' => 'hidden-elt')); ?>
							<?php echo $form->error($model,'video_url'); ?>
							<div class="clear"></div>
						</div>

					</div>
					<!-- END VIDEO -->

					<!-- TEXT 3 -->
					<div class="row">
						<label>Has text bloc 3</label>
						<input type="checkbox" id="checkText3" class="checkHasTarget">
					</div>

					<div class="hidden-area" id="checkText3_target">
						<div class="row">
							<?php echo $form->labelEx($model,'content3Title'); ?>
							<?php echo $form->textField($model,'content3Title',array('size'=>60,'maxlength'=>63, 'class' => 'hidden-elt')); ?>
							<?php echo $form->error($model,'content3Title'); ?>
							<div class="clear"></div>
						</div>
						
						<div class="row">
							<?php echo $form->labelEx($model,'content3_text'); ?>
							<?php echo $form->textArea($model,'content3_text',array('id'=>'editorcontent3_text', 'class' => 'hidden-elt')); ?>
							<?php echo $form->error($model,'content3_text'); ?>
							<div class="clear"></div>
						</div>
					</div>

					<!-- TEXT 4 -->
					<div class="row">
						<label>Has big image</label>
						<input type="checkbox" id="checkBigImage" class="checkHasTarget">
					</div>

					<div class="hidden-area" id="checkBigImage_target">
						<div class="row">
							<?php echo $form->labelEx($model,'contentBig_image'); ?>
							<?php echo $form->fileField($model,'contentBig_image',array('size'=>60,'maxlength'=>255)); ?>
							<?php echo $form->error($model,'contentBig_image'); ?>
							<div class="clear"></div>
				
							<?php if(!$model->isNewRecord && $model->contentBig_image != ''){ ?>
					        <div class='image_already_exist hidden-image-exist'>
					             <?php echo CHtml::image('../../../../images/Work/' . $model->contentBig_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        	<?php } ?>

	        			</div>
        			</div>
					
					<!-- TEXT 4 -->
					<div class="row">
						<label>Has text bloc 4</label>
						<input type="checkbox" id="checkText4" class="checkHasTarget">
					</div>

					<div class="hidden-area" id="checkText4_target">
						<div class="row">
							<?php echo $form->labelEx($model,'content4Title'); ?>
							<?php echo $form->textField($model,'content4Title',array('size'=>60,'maxlength'=>63, 'class' => 'hidden-elt')); ?>
							<?php echo $form->error($model,'content4Title'); ?>
							<div class="clear"></div>
						</div>

						<div class="row">
							<?php echo $form->labelEx($model,'content4_text'); ?>
							<?php echo $form->textArea($model,'content4_text',array('id'=>'editorcontent4_text', 'class' => 'hidden-elt')); ?>
							<?php echo $form->error($model,'content4_text'); ?>
							<div class="clear"></div>
						</div>
					</div>

					<div class="row">
						<?php echo $form->labelEx($model,'bottom_image'); ?>
						<?php echo $form->fileField($model,'bottom_image',array('size'=>60,'maxlength'=>255)); ?>
						<?php echo $form->error($model,'bottom_image'); ?>
						<div class="clear"></div>
						
						<?php if(!$model->isNewRecord && $model->bottom_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Work/' . $model->bottom_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
					</div>

					<!--
					<div class="row">
						<?php echo $form->labelEx($model,'bottomMobile_image'); ?>
						<?php echo $form->fileField($model,'bottomMobile_image',array('size'=>60,'maxlength'=>255)); ?>
						<?php echo $form->error($model,'bottomMobile_image'); ?>
						<div class="clear"></div>
						
						<?php if(!$model->isNewRecord && $model->bottomMobile_image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Work/' . $model->bottomMobile_image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        </div>
			        -->
					
					<div class="row">
						<?php echo $form->labelEx($model,'ordre'); ?>
						<?php 
										if($model->isNewRecord) {
					                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array(count($listOrders)=>array('selected'=>true))));
					                    } else {
					                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array($model->ordre=>array('selected'=>true))));
					                    }
									?>						<?php echo $form->error($model,'ordre'); ?>
						<div class="clear"></div>
					</div>

					<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

				CKEDITOR.replace( 'staredSubtitle', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
	
				CKEDITOR.replace( 'editorstaredDescription_text', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			
				CKEDITOR.replace( 'editorcontent1_text', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			
				CKEDITOR.replace( 'editorcontent2_text', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			
				CKEDITOR.replace( 'editorcontent3_text', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			
				CKEDITOR.replace( 'editorcontent4_text', {
			         filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
			         filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
			         filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
			         filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
			         filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
			         filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
			    });
			    
</script>
