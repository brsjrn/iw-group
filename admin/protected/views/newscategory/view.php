<?php
/* @var $this NewscategoryController */
/* @var $model Newscategory */

$this->breadcrumbs=array(
	'Newscategories'=>array('admin'),
	$model->title,
);
?>

<div id="top_admin_model">
	<h1>Newscategory #<?php echo $model->id; ?><span class="back_admin"><?php echo CHtml::link('back', array('Newscategory/admin')); ?></span></h1>
	
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Newscategory/update', 'id'=>$model->id), array('class'=>'btn_model glyphicon glyphicon-edit')); ?>		<?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>		<?php echo CHtml::link('', array('Newscategory/create'), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
<div class="view_attribute"><div class="view_attribute_name">id</div><div class="view_attribute_value"><?php echo isset($model->id) ? $model->id : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">title</div><div class="view_attribute_value"><?php echo isset($model->title) ? $model->title : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">ordre</div><div class="view_attribute_value"><?php echo isset($model->ordre) ? $model->ordre : "-"; ?></div><div class="clear"></div></div></div>