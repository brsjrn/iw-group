<?php
/* @var $this NewscategoryController */
/* @var $model Newscategory */

$this->breadcrumbs=array(
	'Newscategories'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Newscategory', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#newscategory-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Newscategories</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Newscategory/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'newscategory-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'ordre',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>