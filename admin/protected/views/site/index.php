<?php
/* @var $this SiteController */

$baseUrl = Yii::app()->baseUrl; 
$this->pageTitle=Yii::app()->name;

if(Yii::app()->user->isGuest) {
    //$this->redirect(Yii::app()->getModule('user')->loginUrl);
    $this->redirect(array('site/login'));
}
?>

<div id="home_page">
    <div class="admin_info">Welcome on your administration page</div>

    <div class="admin_info">You can access to your specific Wordpress blog administration tools <a href="<?php echo $baseUrl .'/../blog/wp-admin' ?>" target="_blank">here</a></div>
</div>