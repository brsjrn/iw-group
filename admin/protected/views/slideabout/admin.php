<?php
/* @var $this SlideaboutController */
/* @var $model Slideabout */

$this->breadcrumbs=array(
	'Slideabouts'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Slideabout', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#slideabout-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>Slideabouts</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Slideabout/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'slideabout-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
            'name'=>'image',
            'type'=>'html',
            'header' => 'Image',
            'value'=>'CHtml::image(Yii::app()->baseUrl . "/../images/Slideabout/" . $data->image,"",array("style"=>"width:auto;height:200px;"))'
        ),
		//'image',
		//'image_mobile',
		'ordre',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>