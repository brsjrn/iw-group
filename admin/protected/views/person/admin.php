<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'People'=>array('admin'),
	'Manage',
);

$this->menu=array(
	array('label'=>'Create Person', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#person-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="top_admin_model">
	<h1>People</h1>
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Person/create'), array('class'=>'btn_model btn_add glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'person-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
            'name'=>'image',
            'type'=>'html',
            'header' => 'Image',
            'value'=>'CHtml::image(Yii::app()->baseUrl . "/../images/Person/" . $data->image,"",array("style"=>"width:auto;height:200px;"))'
        ),
		//'image',
		//'image_mobile',
		'name',
		'ordre',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
</div>