<?php
/* @var $this PersonController */
/* @var $model Person */

$this->breadcrumbs=array(
	'People'=>array('admin'),
	$model->name,
);
?>

<div id="top_admin_model">
	<h1>Person #<?php echo $model->id; ?><span class="back_admin"><?php echo CHtml::link('back', array('Person/admin')); ?></span></h1>
	
	<div id="btn_model_area">
		<?php echo CHtml::link('', array('Person/update', 'id'=>$model->id), array('class'=>'btn_model glyphicon glyphicon-edit')); ?>		<?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>		<?php echo CHtml::link('', array('Person/create'), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
<div class="view_attribute"><div class="view_attribute_name">id</div><div class="view_attribute_value"><?php echo isset($model->id) ? $model->id : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">image</div><div class="view_attribute_value view_attribute_image"><?php echo isset($model->image) ? CHtml::image("../../../images/Person/". $model->image,"image",array("class"=>"image_preview")):"-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">image_mobile</div><div class="view_attribute_value view_attribute_image"><?php echo isset($model->image_mobile) ? CHtml::image("../../../images/Person/". $model->image_mobile,"image",array("class"=>"image_preview")):"-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">name</div><div class="view_attribute_value"><?php echo isset($model->name) ? $model->name : "-"; ?></div><div class="clear"></div></div><div class="view_attribute"><div class="view_attribute_name">ordre</div><div class="view_attribute_value"><?php echo isset($model->ordre) ? $model->ordre : "-"; ?></div><div class="clear"></div></div></div>