<?php
/* @var $this PersonController */
/* @var $model Person */
/* @var $form CActiveForm */

	
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/form_script.js');
	?>

<script src="<?php echo Yii::app()->baseUrl.'/ckeditor/ckeditor.js'; ?>"></script>
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'person-form',
	'enableAjaxValidation'=>false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

			<div class="row">
				<?php echo $form->labelEx($model,'image'); ?>
				<?php echo $form->fileField($model,'image',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'image'); ?>
				<div class="clear"></div>
				
						<?php if(!$model->isNewRecord && $model->image != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Person/' . $model->image,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
			        			</div>

    			<!--
					<div class="row">
						<?php echo $form->labelEx($model,'image_mobile'); ?>
						<?php echo $form->fileField($model,'image_mobile',array('size'=>60,'maxlength'=>255)); ?>
						<?php echo $form->error($model,'image_mobile'); ?>
						<div class="clear"></div>
						
						<?php if(!$model->isNewRecord && $model->image_mobile != ''){ ?>
					        <div class='image_already_exist'>
					             <?php echo CHtml::image('../../../../images/Person/' . $model->image_mobile,'image',array('class'=>'image_preview')); ?>
					        </div>
				        <?php } ?>
    				</div>
				-->
							<div class="row">
						<?php echo $form->labelEx($model,'name'); ?>
						<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>127)); ?>
						<?php echo $form->error($model,'name'); ?>
						<div class="clear"></div>
					</div>
										<div class="row">
						<?php echo $form->labelEx($model,'ordre'); ?>
						<?php 
										if($model->isNewRecord) {
					                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array(count($listOrders)=>array('selected'=>true))));
					                    } else {
					                        echo $form->dropDownList($model, 'ordre', $listOrders, array('options'=>array($model->ordre=>array('selected'=>true))));
					                    }
									?>						<?php echo $form->error($model,'ordre'); ?>
						<div class="clear"></div>
					</div>
					<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('id'=>'submit_btn')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->


<script type='text/javascript'>

	    
</script>
