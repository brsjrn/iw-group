<?php
/* @var $this ServiceController */
/* @var $model Service */

$this->breadcrumbs=array(
	'Services'=>array('admin'),
	'Create',
);

$this->menu=array(
	array('label'=>'Manage Service', 'url'=>array('admin')),
);
?>

<div id="top_admin_model">
	<h1>Create Service<span class="back_admin"><?php echo CHtml::link('back', array('Service/admin')); ?></span></h1>

	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">create</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">

<?php echo $this->renderPartial('_form', array('model'=>$model, 'listOrders' => $listOrders)); ?></div>