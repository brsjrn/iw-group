<?php
/* @var $this ServiceController */
/* @var $data Service */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bakground_image')); ?>:</b>
	<?php echo CHtml::encode($data->bakground_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordre')); ?>:</b>
	<?php echo CHtml::encode($data->ordre); ?>
	<br />


</div>