<?php

class PersonController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Person;

		
						$listOrders = $this->getListOrderCreate();
					
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Person']))
		{
			$model->attributes=$_POST['Person'];

			// Manage order
			
							if($model->ordre <= $this->countModels()) {
				                // On récupère toutes les rubriques dont l'ordre est >=
				                $listModels = $this->getModelsSupOrdre($model->ordre);
				                
				                foreach ($listModels as $oldModel) {
				                    $oldModel->ordre += 1;
				                    $oldModel->save();
				                }
				            }
						
			// Manage links
						

			// Manage images upload
			
							$tmp_image = CUploadedFile::getInstance($model, 'image');

							if(isset($tmp_image)) {
	                            $model->image = 'Person'.'_image'. uniqid() .'.'. $tmp_image->extensionName;
	                        }
						
							$tmp_image_mobile = CUploadedFile::getInstance($model, 'image_mobile');

							if(isset($tmp_image_mobile)) {
	                            $model->image_mobile = 'Person'.'_image_mobile'. uniqid() .'.'. $tmp_image_mobile->extensionName;
	                        }
						            
			if($model->save()) {
				
								if(isset($tmp_image)) {
									if(!$tmp_image->saveAs('../images/Person/'. $model->image)) {
                                         throw new ExceptionClass('Problem Person image upload');
                                     }
								}
							
								if(isset($tmp_image_mobile)) {
									if(!$tmp_image_mobile->saveAs('../images/Person/'. $model->image_mobile)) {
                                         throw new ExceptionClass('Problem Person image upload');
                                     }
								}
											$this->redirect(array('admin'));
			}
		}

		
					$this->render('create',array(
						'model' => $model,
						'listOrders' => $listOrders
					));
					}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
						$listOrders = $this->getListOrderUpdate();
						$oldOrdre = $model->ordre;
					
		
		// Current images
		
						$old_image = $model->image;
					
						$old_image_mobile = $model->image_mobile;
					
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Person']))
		{
			$model->attributes=$_POST['Person'];

			// Manage order
			
							// Si le nouveau rank est supérieur
	                        if($model->ordre > $oldOrdre) {
	                            // On récupère toutes les rubriquesdont l'ordre est compris entre oldOrdre et model->ordre
	                            $listModels = $this->getModelsBetweenSupOrdres($oldOrdre, $model->ordre);
	                            
	                            for($i=0; $i<$model->ordre-$oldOrdre; $i++) {
	                                $oldModel = $listModels[$i];
	                                $oldModel->ordre -= 1;
	                                $oldModel->save();
	                            }
	                        }
	                        // Si le nouveau rank est inférieur
	                        if($model->ordre < $oldOrdre) {
	                            // On récupère toutes les rubiques dont l'ordre est compris entre model->ordre et oldOrdre
	                            $listModels = $this->getModelsBetweenInfOrdres($model->ordre, $oldOrdre);
	                            
	                            for($i=0; $i<$oldOrdre-$model->ordre; $i++) {
	                                $oldModel = $listModels[$i];
	                                $oldModel->ordre += 1;
	                                $oldModel->save();
	                            }
	                        }
						
			// Manage links
			
			// Manage images upload
			
							$tmp_image = CUploadedFile::getInstance($model, 'image');

							if(isset($tmp_image)) {
	                            $model->image = 'Person'.'_image'. uniqid() .'.'. $tmp_image->extensionName;
	                        }
	                        if(!isset($tmp_image) && isset($old_image)) {
	                            $model->image = $old_image;
	                        }
						
							$tmp_image_mobile = CUploadedFile::getInstance($model, 'image_mobile');

							if(isset($tmp_image_mobile)) {
	                            $model->image_mobile = 'Person'.'_image_mobile'. uniqid() .'.'. $tmp_image_mobile->extensionName;
	                        }
	                        if(!isset($tmp_image_mobile) && isset($old_image_mobile)) {
	                            $model->image_mobile = $old_image_mobile;
	                        }
						
			if($model->save()) {
				
								if(isset($tmp_image)) {
									if(!$tmp_image->saveAs('../images/Person/'. $model->image)) {
                                         throw new ExceptionClass('Problem Person image upload');
                                     }
                                     if(isset($old_image) && strlen($old_image)>0) {
	                                    unlink('../images/Person/' . $old_image);
	                                }
								}
							
								if(isset($tmp_image_mobile)) {
									if(!$tmp_image_mobile->saveAs('../images/Person/'. $model->image_mobile)) {
                                         throw new ExceptionClass('Problem Person image upload');
                                     }
                                     if(isset($old_image_mobile) && strlen($old_image_mobile)>0) {
	                                    unlink('../images/Person/' . $old_image_mobile);
	                                }
								}
											$this->redirect(array('admin'));
			}
		}

		
					$this->render('update',array(
						'model' => $model,
						'listOrders' => $listOrders
					));
					}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->delete();

		 
						$nbModels = $this->countModels()+1;

						// On récupère toutes les rubriques dont l'ordre est compris entre oldOrdre et model->ordre
				        $listModels = $this->getModelsBetweenSupOrdres($model->ordre, $nbModels);

				        for($i=0; $i<$nbModels-$model->ordre; $i++) {
				            $oldModel = $listModels[$i];
				            $oldModel->ordre -= 1;
				            $oldModel->save();
				        }
					
		
                
        
						if(isset($model->image) && $model->image != '') {
				            unlink('../images/Person/'. $model->image);
				        }
					
						if(isset($model->image_mobile) && $model->image_mobile != '') {
				            unlink('../images/Person/'. $model->image_mobile);
				        }
					        

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Person');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Person('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Person']))
			$model->attributes=$_GET['Person'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Person the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Person::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Person $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='person-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    /**
     * Get nb model instances
     */
    protected function countModels() {
        $listModels = Person::model()->findAll();
        
        return count($listModels);
    }

    /**
     * Get model with order superiori to order param
     */
    protected function getModelsSupOrdre($ordre) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordre');
        $criteria->params = array(
                ':ordre'=>$ordre
        );
        $listModels = Person::model()->findAll($criteria);
        
        return $listModels;
    }

     /**
     * Get order list to create model
     */
    protected function getListOrderCreate() {
        $listModels = Person::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels)+1; $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Donne la liste des ordres à afficher dans le formulaire d'update
     */
    protected function getListOrderUpdate() {
        $listModels = Person::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels); $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant remonté)
     */
    protected function getModelsBetweenSupOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>:ordreDeb');
        $criteria->addCondition('ordre<=:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Person::model()->findAll($criteria);
        
        return $listModels;
    }

    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant redescendu)
     */
    protected function getModelsBetweenInfOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordreDeb');
        $criteria->addCondition('ordre<:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Person::model()->findAll($criteria);
        
        return $listModels;
    }
}
