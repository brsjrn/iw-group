<?php

class NewsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
	    $model = $this->loadModel($id);
	    $nbChildren = $this->countChildren($model->id);
		$this->render('view',array(
			'model'=>$model,
	        'nbChildren'=>$nbChildren
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new News;

		// Generate uniqId
		$model->uniqId = rand(1,1000000);
		
						$listOrders = $this->getListOrderCreate();
					
		
					// KCFinder config
	                $_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
	                $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl.'/images/uploads/'; // URL for the uploads folder
	                $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath.'/../images/uploads/'; // path to the uploads folder

				
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
			$model->attributes=$_POST['News'];

			// Manage order
			
							if($model->ordre <= $this->countModels()) {
				                // On récupère toutes les rubriques dont l'ordre est >=
				                $listModels = $this->getModelsSupOrdre($model->ordre);
				                
				                foreach ($listModels as $oldModel) {
				                    $oldModel->ordre += 1;
				                    $oldModel->save();
				                }
				            }
						
			// Manage links
			
							if(strpos($model->link_url, 'http') === FALSE && $model->link_url != '') {
				                $model->link_url = 'http://'. $model->link_url;
				            }
									

			// Manage images upload
			
							$tmp_dispatchCover_image = CUploadedFile::getInstance($model, 'dispatchCover_image');

							if(isset($tmp_dispatchCover_image)) {
	                            $model->dispatchCover_image = 'News'.'_dispatchCover_image'. uniqid() .'.'. $tmp_dispatchCover_image->extensionName;
	                        }
						
							$tmp_insideCover_image = CUploadedFile::getInstance($model, 'insideCover_image');

							if(isset($tmp_insideCover_image)) {
	                            $model->insideCover_image = 'News'.'_insideCover_image'. uniqid() .'.'. $tmp_insideCover_image->extensionName;
	                        }
						
							$tmp_insideCoverMobile_image = CUploadedFile::getInstance($model, 'insideCoverMobile_image');

							if(isset($tmp_insideCoverMobile_image)) {
	                            $model->insideCoverMobile_image = 'News'.'_insideCoverMobile_image'. uniqid() .'.'. $tmp_insideCoverMobile_image->extensionName;
	                        }
						            
			if($model->save()) {
				
								if(isset($tmp_dispatchCover_image)) {
									if(!$tmp_dispatchCover_image->saveAs('../images/News/'. $model->dispatchCover_image)) {
                                         throw new ExceptionClass('Problem News image upload');
                                     }
								}
							
								if(isset($tmp_insideCover_image)) {
									if(!$tmp_insideCover_image->saveAs('../images/News/'. $model->insideCover_image)) {
                                         throw new ExceptionClass('Problem News image upload');
                                     }
								}
							
								if(isset($tmp_insideCoverMobile_image)) {
									if(!$tmp_insideCoverMobile_image->saveAs('../images/News/'. $model->insideCoverMobile_image)) {
                                         throw new ExceptionClass('Problem News image upload');
                                     }
								}
											$this->redirect(array('admin'));
			}
		}

		
					$this->render('create',array(
						'model' => $model,
						'listOrders' => $listOrders,
						'listCategories' => $this->getListCategories(),
						'listAuthors' => $this->getListAuthors()
					));
					}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
						$listOrders = $this->getListOrderUpdate();
						$oldOrdre = $model->ordre;
					
		
					// KCFinder config
	                $_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
	                $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl.'/images/uploads/'; // URL for the uploads folder
	                $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath.'/../images/uploads/'; // path to the uploads folder

				
		// Current images
		
						$old_dispatchCover_image = $model->dispatchCover_image;
					
						$old_insideCover_image = $model->insideCover_image;
					
						$old_insideCoverMobile_image = $model->insideCoverMobile_image;
					
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
			$model->attributes=$_POST['News'];

			// Manage order
			
							// Si le nouveau rank est supérieur
	                        if($model->ordre > $oldOrdre) {
	                            // On récupère toutes les rubriquesdont l'ordre est compris entre oldOrdre et model->ordre
	                            $listModels = $this->getModelsBetweenSupOrdres($oldOrdre, $model->ordre);
	                            
	                            for($i=0; $i<$model->ordre-$oldOrdre; $i++) {
	                                $oldModel = $listModels[$i];
	                                $oldModel->ordre -= 1;
	                                $oldModel->save();
	                            }
	                        }
	                        // Si le nouveau rank est inférieur
	                        if($model->ordre < $oldOrdre) {
	                            // On récupère toutes les rubiques dont l'ordre est compris entre model->ordre et oldOrdre
	                            $listModels = $this->getModelsBetweenInfOrdres($model->ordre, $oldOrdre);
	                            
	                            for($i=0; $i<$oldOrdre-$model->ordre; $i++) {
	                                $oldModel = $listModels[$i];
	                                $oldModel->ordre += 1;
	                                $oldModel->save();
	                            }
	                        }
						
			// Manage links
			
							if(strpos($model->link_url, 'http') === FALSE && $model->link_url != '') {
				                $model->link_url = 'http://'. $model->link_url;
				            }
						
			// Manage images upload
			
							$tmp_dispatchCover_image = CUploadedFile::getInstance($model, 'dispatchCover_image');

							if(isset($tmp_dispatchCover_image)) {
	                            $model->dispatchCover_image = 'News'.'_dispatchCover_image'. uniqid() .'.'. $tmp_dispatchCover_image->extensionName;
	                        }
	                        if(!isset($tmp_dispatchCover_image) && isset($old_dispatchCover_image)) {
	                            $model->dispatchCover_image = $old_dispatchCover_image;
	                        }
						
							$tmp_insideCover_image = CUploadedFile::getInstance($model, 'insideCover_image');

							if(isset($tmp_insideCover_image)) {
	                            $model->insideCover_image = 'News'.'_insideCover_image'. uniqid() .'.'. $tmp_insideCover_image->extensionName;
	                        }
	                        if(!isset($tmp_insideCover_image) && isset($old_insideCover_image)) {
	                            $model->insideCover_image = $old_insideCover_image;
	                        }
						
							$tmp_insideCoverMobile_image = CUploadedFile::getInstance($model, 'insideCoverMobile_image');

							if(isset($tmp_insideCoverMobile_image)) {
	                            $model->insideCoverMobile_image = 'News'.'_insideCoverMobile_image'. uniqid() .'.'. $tmp_insideCoverMobile_image->extensionName;
	                        }
	                        if(!isset($tmp_insideCoverMobile_image) && isset($old_insideCoverMobile_image)) {
	                            $model->insideCoverMobile_image = $old_insideCoverMobile_image;
	                        }
						
			if($model->save()) {
				
								if(isset($tmp_dispatchCover_image)) {
									if(!$tmp_dispatchCover_image->saveAs('../images/News/'. $model->dispatchCover_image)) {
                                         throw new ExceptionClass('Problem News image upload');
                                     }
                                     if(isset($old_dispatchCover_image) && strlen($old_dispatchCover_image)>0) {
	                                    unlink('../images/News/' . $old_dispatchCover_image);
	                                }
								}
							
								if(isset($tmp_insideCover_image)) {
									if(!$tmp_insideCover_image->saveAs('../images/News/'. $model->insideCover_image)) {
                                         throw new ExceptionClass('Problem News image upload');
                                     }
                                     if(isset($old_insideCover_image) && strlen($old_insideCover_image)>0) {
	                                    unlink('../images/News/' . $old_insideCover_image);
	                                }
								}
							
								if(isset($tmp_insideCoverMobile_image)) {
									if(!$tmp_insideCoverMobile_image->saveAs('../images/News/'. $model->insideCoverMobile_image)) {
                                         throw new ExceptionClass('Problem News image upload');
                                     }
                                     if(isset($old_insideCoverMobile_image) && strlen($old_insideCoverMobile_image)>0) {
	                                    unlink('../images/News/' . $old_insideCoverMobile_image);
	                                }
								}
											$this->redirect(array('admin'));
			}
		}

		
					$this->render('update',array(
						'model' => $model,
						'listOrders' => $listOrders,
						'listCategories' => $this->getListCategories(),
						'listAuthors' => $this->getListAuthors()
					));
					}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->delete();

		 
						$nbModels = $this->countModels()+1;

						// On récupère toutes les rubriques dont l'ordre est compris entre oldOrdre et model->ordre
				        $listModels = $this->getModelsBetweenSupOrdres($model->ordre, $nbModels);

				        for($i=0; $i<$nbModels-$model->ordre; $i++) {
				            $oldModel = $listModels[$i];
				            $oldModel->ordre -= 1;
				            $oldModel->save();
				        }
					
		
                
        
						if(isset($model->dispatchCover_image) && $model->dispatchCover_image != '') {
				            unlink('../images/News/'. $model->dispatchCover_image);
				        }
					
						if(isset($model->insideCover_image) && $model->insideCover_image != '') {
				            unlink('../images/News/'. $model->insideCover_image);
				        }
					
						if(isset($model->insideCoverMobile_image) && $model->insideCoverMobile_image != '') {
				            unlink('../images/News/'. $model->insideCoverMobile_image);
				        }
					        

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('News');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new News('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['News']))
			$model->attributes=$_GET['News'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return News the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=News::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param News $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    /**
     * Get nb model instances
     */
    protected function countModels() {
        $listModels = News::model()->findAll();
        
        return count($listModels);
    }

    /**
     * Get model with order superiori to order param
     */
    protected function getModelsSupOrdre($ordre) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordre');
        $criteria->params = array(
                ':ordre'=>$ordre
        );
        $listModels = News::model()->findAll($criteria);
        
        return $listModels;
    }

     /**
     * Get order list to create model
     */
    protected function getListOrderCreate() {
        $listModels = News::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels)+1; $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Donne la liste des ordres à afficher dans le formulaire d'update
     */
    protected function getListOrderUpdate() {
        $listModels = News::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels); $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant remonté)
     */
    protected function getModelsBetweenSupOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>:ordreDeb');
        $criteria->addCondition('ordre<=:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = News::model()->findAll($criteria);
        
        return $listModels;
    }

    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant redescendu)
     */
    protected function getModelsBetweenInfOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordreDeb');
        $criteria->addCondition('ordre<:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = News::model()->findAll($criteria);
        
        return $listModels;
    }

     /**
     * Get all categories
     */
    protected function getListCategories() {
        $criteria = new CDbCriteria();
        $listElt = Newscategory::model()->findAll();
        $listEltsFinal = array();
        foreach ($listElt as $elt) {
            $listEltsFinal[$elt->id] = $elt->title;
        }
        
        return $listEltsFinal;
    }

     /**
     * Get all authors
     */
    protected function getListAuthors() {
        $criteria = new CDbCriteria();
        $listElt = Author::model()->findAll();
        $listEltsFinal = array();
        foreach ($listElt as $elt) {
            $listEltsFinal[$elt->id] = $elt->name;
        }
        
        return $listEltsFinal;
    }

    protected function countChildren($parentId) {
         $criteria = new CDbCriteria();
         $criteria->addCondition('ref_uniqId=:ref_uniqId');
         $criteria->params = array(
             ':ref_uniqId'=>$parentId
         );
         
         $listChild = Sliderimage::model()->findAll($criteria);
         return count($listChild);
    }
}
