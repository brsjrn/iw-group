<?php

class WorkController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
	    $model = $this->loadModel($id);
	    $nbChildren = $this->countChildren($model->id);
		$this->render('view',array(
			'model'=>$model,
	        'nbChildren'=>$nbChildren
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Work;

		// Generate uniqId
		$model->uniqId = rand(1,1000000);

		
		$listOrders = $this->getListOrderCreate();
		

		// KCFinder config
        $_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
        $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl.'/images/uploads/'; // URL for the uploads folder
        $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath.'/../images/uploads/'; // path to the uploads folder

				
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Work']))
		{
			$model->attributes=$_POST['Work'];

			// Manage order
			
							if($model->ordre <= $this->countModels()) {
				                // On récupère toutes les rubriques dont l'ordre est >=
				                $listModels = $this->getModelsSupOrdre($model->ordre);
				                
				                foreach ($listModels as $oldModel) {
				                    $oldModel->ordre += 1;
				                    $oldModel->save();
				                }
				            }
						
			// Manage links
						

			// Manage images upload
			
							$tmp_cover1_image = CUploadedFile::getInstance($model, 'cover1_image');

							if(isset($tmp_cover1_image)) {
	                            $model->cover1_image = 'Work'.'_cover1_image'. uniqid() .'.'. $tmp_cover1_image->extensionName;
	                        }
						
							$tmp_cover1Mobile_image = CUploadedFile::getInstance($model, 'cover1Mobile_image');

							if(isset($tmp_cover1Mobile_image)) {
	                            $model->cover1Mobile_image = 'Work'.'_cover1Mobile_image'. uniqid() .'.'. $tmp_cover1Mobile_image->extensionName;
	                        }
						
							$tmp_cover2_image = CUploadedFile::getInstance($model, 'cover2_image');

							if(isset($tmp_cover2_image)) {
	                            $model->cover2_image = 'Work'.'_cover2_image'. uniqid() .'.'. $tmp_cover2_image->extensionName;
	                        }
						
							$tmp_cover2Mobile_image = CUploadedFile::getInstance($model, 'cover2Mobile_image');

							if(isset($tmp_cover2Mobile_image)) {
	                            $model->cover2Mobile_image = 'Work'.'_cover2Mobile_image'. uniqid() .'.'. $tmp_cover2Mobile_image->extensionName;
	                        }
						
							$tmp_parallax_image = CUploadedFile::getInstance($model, 'parallax_image');

							if(isset($tmp_parallax_image)) {
	                            $model->parallax_image = 'Work'.'_parallax_image'. uniqid() .'.'. $tmp_parallax_image->extensionName;
	                        }
						
							$tmp_parallaxMobile_image = CUploadedFile::getInstance($model, 'parallaxMobile_image');

							if(isset($tmp_parallaxMobile_image)) {
	                            $model->parallaxMobile_image = 'Work'.'_parallaxMobile_image'. uniqid() .'.'. $tmp_parallaxMobile_image->extensionName;
	                        }
						
							$tmp_video_image = CUploadedFile::getInstance($model, 'video_image');

							if(isset($tmp_video_image)) {
	                            $model->video_image = 'Work'.'_video_image'. uniqid() .'.'. $tmp_video_image->extensionName;
	                        }
						
							$tmp_videoMobile_image = CUploadedFile::getInstance($model, 'videoMobile_image');

							if(isset($tmp_videoMobile_image)) {
	                            $model->videoMobile_image = 'Work'.'_videoMobile_image'. uniqid() .'.'. $tmp_videoMobile_image->extensionName;
	                        }
						
							$tmp_contentBig_image = CUploadedFile::getInstance($model, 'contentBig_image');

							if(isset($tmp_contentBig_image)) {
	                            $model->contentBig_image = 'Work'.'_contentBig_image'. uniqid() .'.'. $tmp_contentBig_image->extensionName;
	                        }
						
							$tmp_bottom_image = CUploadedFile::getInstance($model, 'bottom_image');

							if(isset($tmp_bottom_image)) {
	                            $model->bottom_image = 'Work'.'_bottom_image'. uniqid() .'.'. $tmp_bottom_image->extensionName;
	                        }
						
							$tmp_bottomMobile_image = CUploadedFile::getInstance($model, 'bottomMobile_image');

							if(isset($tmp_bottomMobile_image)) {
	                            $model->bottomMobile_image = 'Work'.'_bottomMobile_image'. uniqid() .'.'. $tmp_bottomMobile_image->extensionName;
	                        }
						            
			if($model->save()) {
				
								if(isset($tmp_cover1_image)) {
									if(!$tmp_cover1_image->saveAs('../images/Work/'. $model->cover1_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
							
								if(isset($tmp_cover1Mobile_image)) {
									if(!$tmp_cover1Mobile_image->saveAs('../images/Work/'. $model->cover1Mobile_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
							
								if(isset($tmp_cover2_image)) {
									if(!$tmp_cover2_image->saveAs('../images/Work/'. $model->cover2_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
							
								if(isset($tmp_cover2Mobile_image)) {
									if(!$tmp_cover2Mobile_image->saveAs('../images/Work/'. $model->cover2Mobile_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
							
								if(isset($tmp_parallax_image)) {
									if(!$tmp_parallax_image->saveAs('../images/Work/'. $model->parallax_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
							
								if(isset($tmp_parallaxMobile_image)) {
									if(!$tmp_parallaxMobile_image->saveAs('../images/Work/'. $model->parallaxMobile_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
							
								if(isset($tmp_video_image)) {
									if(!$tmp_video_image->saveAs('../images/Work/'. $model->video_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
							
								if(isset($tmp_videoMobile_image)) {
									if(!$tmp_videoMobile_image->saveAs('../images/Work/'. $model->videoMobile_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
							
								if(isset($tmp_contentBig_image)) {
									if(!$tmp_contentBig_image->saveAs('../images/Work/'. $model->contentBig_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
							
								if(isset($tmp_bottom_image)) {
									if(!$tmp_bottom_image->saveAs('../images/Work/'. $model->bottom_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
							
								if(isset($tmp_bottomMobile_image)) {
									if(!$tmp_bottomMobile_image->saveAs('../images/Work/'. $model->bottomMobile_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
								}
											$this->redirect(array('admin'));
			}
		}

		
					$this->render('create',array(
						'model' => $model,
						'listOrders' => $listOrders
					));
					}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
						$listOrders = $this->getListOrderUpdate();
						$oldOrdre = $model->ordre;
					
		
					// KCFinder config
	                $_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
	                $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl.'/images/uploads/'; // URL for the uploads folder
	                $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath.'/../images/uploads/'; // path to the uploads folder

				
		// Current images
		
						$old_cover1_image = $model->cover1_image;
					
						$old_cover1Mobile_image = $model->cover1Mobile_image;
					
						$old_cover2_image = $model->cover2_image;
					
						$old_cover2Mobile_image = $model->cover2Mobile_image;
					
						$old_parallax_image = $model->parallax_image;
					
						$old_parallaxMobile_image = $model->parallaxMobile_image;
					
						$old_video_image = $model->video_image;
					
						$old_videoMobile_image = $model->videoMobile_image;
					
						$old_contentBig_image = $model->contentBig_image;
					
						$old_bottom_image = $model->bottom_image;
					
						$old_bottomMobile_image = $model->bottomMobile_image;
					
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Work']))
		{
			$model->attributes=$_POST['Work'];

			// Manage order
			
							// Si le nouveau rank est supérieur
	                        if($model->ordre > $oldOrdre) {
	                            // On récupère toutes les rubriquesdont l'ordre est compris entre oldOrdre et model->ordre
	                            $listModels = $this->getModelsBetweenSupOrdres($oldOrdre, $model->ordre);
	                            
	                            for($i=0; $i<$model->ordre-$oldOrdre; $i++) {
	                                $oldModel = $listModels[$i];
	                                $oldModel->ordre -= 1;
	                                $oldModel->save();
	                            }
	                        }
	                        // Si le nouveau rank est inférieur
	                        if($model->ordre < $oldOrdre) {
	                            // On récupère toutes les rubiques dont l'ordre est compris entre model->ordre et oldOrdre
	                            $listModels = $this->getModelsBetweenInfOrdres($model->ordre, $oldOrdre);
	                            
	                            for($i=0; $i<$oldOrdre-$model->ordre; $i++) {
	                                $oldModel = $listModels[$i];
	                                $oldModel->ordre += 1;
	                                $oldModel->save();
	                            }
	                        }
						
			// Manage links
			
			// Manage images upload
			
							$tmp_cover1_image = CUploadedFile::getInstance($model, 'cover1_image');

							if(isset($tmp_cover1_image)) {
	                            $model->cover1_image = 'Work'.'_cover1_image'. uniqid() .'.'. $tmp_cover1_image->extensionName;
	                        }
	                        if(!isset($tmp_cover1_image) && isset($old_cover1_image)) {
	                            $model->cover1_image = $old_cover1_image;
	                        }
						
							$tmp_cover1Mobile_image = CUploadedFile::getInstance($model, 'cover1Mobile_image');

							if(isset($tmp_cover1Mobile_image)) {
	                            $model->cover1Mobile_image = 'Work'.'_cover1Mobile_image'. uniqid() .'.'. $tmp_cover1Mobile_image->extensionName;
	                        }
	                        if(!isset($tmp_cover1Mobile_image) && isset($old_cover1Mobile_image)) {
	                            $model->cover1Mobile_image = $old_cover1Mobile_image;
	                        }
						
							$tmp_cover2_image = CUploadedFile::getInstance($model, 'cover2_image');

							if(isset($tmp_cover2_image)) {
	                            $model->cover2_image = 'Work'.'_cover2_image'. uniqid() .'.'. $tmp_cover2_image->extensionName;
	                        }
	                        if(!isset($tmp_cover2_image) && isset($old_cover2_image)) {
	                            $model->cover2_image = $old_cover2_image;
	                        }
						
							$tmp_cover2Mobile_image = CUploadedFile::getInstance($model, 'cover2Mobile_image');

							if(isset($tmp_cover2Mobile_image)) {
	                            $model->cover2Mobile_image = 'Work'.'_cover2Mobile_image'. uniqid() .'.'. $tmp_cover2Mobile_image->extensionName;
	                        }
	                        if(!isset($tmp_cover2Mobile_image) && isset($old_cover2Mobile_image)) {
	                            $model->cover2Mobile_image = $old_cover2Mobile_image;
	                        }
						
							$tmp_parallax_image = CUploadedFile::getInstance($model, 'parallax_image');

							if(isset($tmp_parallax_image)) {
	                            $model->parallax_image = 'Work'.'_parallax_image'. uniqid() .'.'. $tmp_parallax_image->extensionName;
	                        }
	                        if(!isset($tmp_parallax_image) && isset($old_parallax_image)) {
	                            $model->parallax_image = $old_parallax_image;
	                        }
						
							$tmp_parallaxMobile_image = CUploadedFile::getInstance($model, 'parallaxMobile_image');

							if(isset($tmp_parallaxMobile_image)) {
	                            $model->parallaxMobile_image = 'Work'.'_parallaxMobile_image'. uniqid() .'.'. $tmp_parallaxMobile_image->extensionName;
	                        }
	                        if(!isset($tmp_parallaxMobile_image) && isset($old_parallaxMobile_image)) {
	                            $model->parallaxMobile_image = $old_parallaxMobile_image;
	                        }
						
							$tmp_video_image = CUploadedFile::getInstance($model, 'video_image');

							if(isset($tmp_video_image)) {
	                            $model->video_image = 'Work'.'_video_image'. uniqid() .'.'. $tmp_video_image->extensionName;
	                        }
	                        if(!isset($tmp_video_image) && isset($old_video_image)) {
	                            $model->video_image = $old_video_image;
	                        }
						
							$tmp_videoMobile_image = CUploadedFile::getInstance($model, 'videoMobile_image');

							if(isset($tmp_videoMobile_image)) {
	                            $model->videoMobile_image = 'Work'.'_videoMobile_image'. uniqid() .'.'. $tmp_videoMobile_image->extensionName;
	                        }
	                        if(!isset($tmp_videoMobile_image) && isset($old_videoMobile_image)) {
	                            $model->videoMobile_image = $old_videoMobile_image;
	                        }
						
							$tmp_contentBig_image = CUploadedFile::getInstance($model, 'contentBig_image');

							if(isset($tmp_contentBig_image)) {
	                            $model->contentBig_image = 'Work'.'_contentBig_image'. uniqid() .'.'. $tmp_contentBig_image->extensionName;
	                        }
	                        if(!isset($tmp_contentBig_image) && isset($old_contentBig_image)) {
	                            $model->contentBig_image = $old_contentBig_image;
	                        }
						
							$tmp_bottom_image = CUploadedFile::getInstance($model, 'bottom_image');

							if(isset($tmp_bottom_image)) {
	                            $model->bottom_image = 'Work'.'_bottom_image'. uniqid() .'.'. $tmp_bottom_image->extensionName;
	                        }
	                        if(!isset($tmp_bottom_image) && isset($old_bottom_image)) {
	                            $model->bottom_image = $old_bottom_image;
	                        }
						
							$tmp_bottomMobile_image = CUploadedFile::getInstance($model, 'bottomMobile_image');

							if(isset($tmp_bottomMobile_image)) {
	                            $model->bottomMobile_image = 'Work'.'_bottomMobile_image'. uniqid() .'.'. $tmp_bottomMobile_image->extensionName;
	                        }
	                        if(!isset($tmp_bottomMobile_image) && isset($old_bottomMobile_image)) {
	                            $model->bottomMobile_image = $old_bottomMobile_image;
	                        }
						
			if($model->save()) {
				
								if(isset($tmp_cover1_image)) {
									if(!$tmp_cover1_image->saveAs('../images/Work/'. $model->cover1_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_cover1_image) && strlen($old_cover1_image)>0) {
	                                    unlink('../images/Work/' . $old_cover1_image);
	                                }
								}
							
								if(isset($tmp_cover1Mobile_image)) {
									if(!$tmp_cover1Mobile_image->saveAs('../images/Work/'. $model->cover1Mobile_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_cover1Mobile_image) && strlen($old_cover1Mobile_image)>0) {
	                                    unlink('../images/Work/' . $old_cover1Mobile_image);
	                                }
								}
							
								if(isset($tmp_cover2_image)) {
									if(!$tmp_cover2_image->saveAs('../images/Work/'. $model->cover2_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_cover2_image) && strlen($old_cover2_image)>0) {
	                                    unlink('../images/Work/' . $old_cover2_image);
	                                }
								}
							
								if(isset($tmp_cover2Mobile_image)) {
									if(!$tmp_cover2Mobile_image->saveAs('../images/Work/'. $model->cover2Mobile_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_cover2Mobile_image) && strlen($old_cover2Mobile_image)>0) {
	                                    unlink('../images/Work/' . $old_cover2Mobile_image);
	                                }
								}
							
								if(isset($tmp_parallax_image)) {
									if(!$tmp_parallax_image->saveAs('../images/Work/'. $model->parallax_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_parallax_image) && strlen($old_parallax_image)>0) {
	                                    unlink('../images/Work/' . $old_parallax_image);
	                                }
								}
							
								if(isset($tmp_parallaxMobile_image)) {
									if(!$tmp_parallaxMobile_image->saveAs('../images/Work/'. $model->parallaxMobile_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_parallaxMobile_image) && strlen($old_parallaxMobile_image)>0) {
	                                    unlink('../images/Work/' . $old_parallaxMobile_image);
	                                }
								}
							
								if(isset($tmp_video_image)) {
									if(!$tmp_video_image->saveAs('../images/Work/'. $model->video_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_video_image) && strlen($old_video_image)>0) {
	                                    unlink('../images/Work/' . $old_video_image);
	                                }
								}
							
								if(isset($tmp_videoMobile_image)) {
									if(!$tmp_videoMobile_image->saveAs('../images/Work/'. $model->videoMobile_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_videoMobile_image) && strlen($old_videoMobile_image)>0) {
	                                    unlink('../images/Work/' . $old_videoMobile_image);
	                                }
								}
							
								if(isset($tmp_contentBig_image)) {
									if(!$tmp_contentBig_image->saveAs('../images/Work/'. $model->contentBig_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_contentBig_image) && strlen($old_contentBig_image)>0) {
	                                    unlink('../images/Work/' . $old_contentBig_image);
	                                }
								}
							
								if(isset($tmp_bottom_image)) {
									if(!$tmp_bottom_image->saveAs('../images/Work/'. $model->bottom_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_bottom_image) && strlen($old_bottom_image)>0) {
	                                    unlink('../images/Work/' . $old_bottom_image);
	                                }
								}
							
								if(isset($tmp_bottomMobile_image)) {
									if(!$tmp_bottomMobile_image->saveAs('../images/Work/'. $model->bottomMobile_image)) {
                                         throw new ExceptionClass('Problem Work image upload');
                                     }
                                     if(isset($old_bottomMobile_image) && strlen($old_bottomMobile_image)>0) {
	                                    unlink('../images/Work/' . $old_bottomMobile_image);
	                                }
								}
								
								$this->redirect(array('admin'));
			}
		}

		
					$this->render('update',array(
						'model' => $model,
						'listOrders' => $listOrders
					));
					}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->delete();

		 
						$nbModels = $this->countModels()+1;

						// On récupère toutes les rubriques dont l'ordre est compris entre oldOrdre et model->ordre
				        $listModels = $this->getModelsBetweenSupOrdres($model->ordre, $nbModels);

				        for($i=0; $i<$nbModels-$model->ordre; $i++) {
				            $oldModel = $listModels[$i];
				            $oldModel->ordre -= 1;
				            $oldModel->save();
				        }
					
		
                
        
						if(isset($model->cover1_image) && $model->cover1_image != '') {
				            unlink('../images/Work/'. $model->cover1_image);
				        }
					
						if(isset($model->cover1Mobile_image) && $model->cover1Mobile_image != '') {
				            unlink('../images/Work/'. $model->cover1Mobile_image);
				        }
					
						if(isset($model->cover2_image) && $model->cover2_image != '') {
				            unlink('../images/Work/'. $model->cover2_image);
				        }
					
						if(isset($model->cover2Mobile_image) && $model->cover2Mobile_image != '') {
				            unlink('../images/Work/'. $model->cover2Mobile_image);
				        }
					
						if(isset($model->parallax_image) && $model->parallax_image != '') {
				            unlink('../images/Work/'. $model->parallax_image);
				        }
					
						if(isset($model->parallaxMobile_image) && $model->parallaxMobile_image != '') {
				            unlink('../images/Work/'. $model->parallaxMobile_image);
				        }
					
						if(isset($model->video_image) && $model->video_image != '') {
				            unlink('../images/Work/'. $model->video_image);
				        }
					
						if(isset($model->videoMobile_image) && $model->videoMobile_image != '') {
				            unlink('../images/Work/'. $model->videoMobile_image);
				        }
					
						if(isset($model->contentBig_image) && $model->contentBig_image != '') {
				            unlink('../images/Work/'. $model->contentBig_image);
				        }
					
						if(isset($model->bottom_image) && $model->bottom_image != '') {
				            unlink('../images/Work/'. $model->bottom_image);
				        }
					
						if(isset($model->bottomMobile_image) && $model->bottomMobile_image != '') {
				            unlink('../images/Work/'. $model->bottomMobile_image);
				        }
					        

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Work');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Work('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Work']))
			$model->attributes=$_GET['Work'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Work the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Work::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Work $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='work-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    /**
     * Get nb model instances
     */
    protected function countModels() {
        $listModels = Work::model()->findAll();
        
        return count($listModels);
    }

    /**
     * Get model with order superiori to order param
     */
    protected function getModelsSupOrdre($ordre) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordre');
        $criteria->params = array(
                ':ordre'=>$ordre
        );
        $listModels = Work::model()->findAll($criteria);
        
        return $listModels;
    }

     /**
     * Get order list to create model
     */
    protected function getListOrderCreate() {
        $listModels = Work::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels)+1; $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Donne la liste des ordres à afficher dans le formulaire d'update
     */
    protected function getListOrderUpdate() {
        $listModels = Work::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels); $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant remonté)
     */
    protected function getModelsBetweenSupOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>:ordreDeb');
        $criteria->addCondition('ordre<=:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Work::model()->findAll($criteria);
        
        return $listModels;
    }

    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant redescendu)
     */
    protected function getModelsBetweenInfOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordreDeb');
        $criteria->addCondition('ordre<:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Work::model()->findAll($criteria);
        
        return $listModels;
    }

    protected function countChildren($parentId) {
         $criteria = new CDbCriteria();
         $criteria->addCondition('ref_uniqId=:ref_uniqId');
         $criteria->params = array(
             ':ref_uniqId'=>$parentId
         );
         
         $listChild = Sliderimage::model()->findAll($criteria);
         return count($listChild);
    }
}
