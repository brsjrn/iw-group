<?php

class SliderimageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','adminById','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
	    $model = $this->loadModel($id);

	    // Checl work then news
	    $parent = Work::model()->findByPk($model->ref_uniqId);
	    if(count($parent) == 9) {
	    	$parent = News::model()->findByPk($model->ref_uniqId);
	    }

	    $this->render('view', array(
	        'model' => $model,
	        'parent' => $parent
	    ));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($refModelId)
	{
		$model=new Sliderimage;

		// Find parent
		$criteria = new CDbCriteria();
		$criteria->addCondition('uniqId=:uniqId');
		$criteria->params = array(
		        ':uniqId'=>$refModelId
		);

		$parent = Work::model()->find($criteria);
		$parentType = 'work';
		if(count($parent) == 0) {
			$parentType = 'news';
			$parent = News::model()->find($criteria);
		}
		
		$model->ref_uniqId = $parent->uniqId;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Sliderimage']))
		{
			$model->attributes=$_POST['Sliderimage'];


			// Manage order
			
			// Manage links
						

			// Manage images upload
			
							$tmp_image = CUploadedFile::getInstance($model, 'image');

							if(isset($tmp_image)) {
	                            $model->image = 'Sliderimage'.'_image'. uniqid() .'.'. $tmp_image->extensionName;
	                        }
						
							$tmp_image_mobile = CUploadedFile::getInstance($model, 'image_mobile');

							if(isset($tmp_image_mobile)) {
	                            $model->image_mobile = 'Sliderimage'.'_image_mobile'. uniqid() .'.'. $tmp_image_mobile->extensionName;
	                        }
						            
			if($model->save()) {
				
								if(isset($tmp_image)) {
									if(!$tmp_image->saveAs('../images/Sliderimage/'. $model->image)) {
                                         throw new ExceptionClass('Problem Sliderimage image upload');
                                     }
								}
							
								if(isset($tmp_image_mobile)) {
									if(!$tmp_image_mobile->saveAs('../images/Sliderimage/'. $model->image_mobile)) {
                                         throw new ExceptionClass('Problem Sliderimage image upload');
                                     }
								}
											$this->redirect(array('adminById', 'refModelId' => $parent->uniqId));
			}
		}

		
					$this->render('create',array(
						'model' => $model,
						'parent' => $parent,
						'parentType' => $parentType
					));
					}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		// Find parent
		$criteria = new CDbCriteria();
		$criteria->addCondition('uniqId=:uniqId');
		$criteria->params = array(
		        ':uniqId'=>$model->ref_uniqId
		);

		$parent = Work::model()->find($criteria);
		$parentType = 'work';
		if(count($parent) == 0) {
			$parent = News::model()->find($criteria);
			$parentType = 'news';
		}
		
		// Current images
		$old_image = $model->image;
	
		$old_image_mobile = $model->image_mobile;
					
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Sliderimage']))
		{
			$model->attributes=$_POST['Sliderimage'];

			// Manage order
			
			// Manage links
			
			// Manage images upload
			
							$tmp_image = CUploadedFile::getInstance($model, 'image');

							if(isset($tmp_image)) {
	                            $model->image = 'Sliderimage'.'_image'. uniqid() .'.'. $tmp_image->extensionName;
	                        }
	                        if(!isset($tmp_image) && isset($old_image)) {
	                            $model->image = $old_image;
	                        }
						
							$tmp_image_mobile = CUploadedFile::getInstance($model, 'image_mobile');

							if(isset($tmp_image_mobile)) {
	                            $model->image_mobile = 'Sliderimage'.'_image_mobile'. uniqid() .'.'. $tmp_image_mobile->extensionName;
	                        }
	                        if(!isset($tmp_image_mobile) && isset($old_image_mobile)) {
	                            $model->image_mobile = $old_image_mobile;
	                        }
						
			if($model->save()) {
				
								if(isset($tmp_image)) {
									if(!$tmp_image->saveAs('../images/Sliderimage/'. $model->image)) {
                                         throw new ExceptionClass('Problem Sliderimage image upload');
                                     }
                                     if(isset($old_image) && strlen($old_image)>0) {
	                                    unlink('../images/Sliderimage/' . $old_image);
	                                }
								}
							
								if(isset($tmp_image_mobile)) {
									if(!$tmp_image_mobile->saveAs('../images/Sliderimage/'. $model->image_mobile)) {
                                         throw new ExceptionClass('Problem Sliderimage image upload');
                                     }
                                     if(isset($old_image_mobile) && strlen($old_image_mobile)>0) {
	                                    unlink('../images/Sliderimage/' . $old_image_mobile);
	                                }
								}
											$this->redirect(array('adminById', 'refModelId' => $parent->uniqId));
			}
		}

		
					$this->render('update',array(
						'model' => $model,
						'parent' => $parent,
						'parentType' => $parentType
					));
					}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);

		$refModelId = $model->ref_uniqId;

		$model->delete();

		
		if(isset($model->image) && $model->image != '') {
            unlink('../images/Sliderimage/'. $model->image);
        }
	
		if(isset($model->image_mobile) && $model->image_mobile != '') {
            unlink('../images/Sliderimage/'. $model->image_mobile);
        }
					        

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('adminById', 'refModelId' => $refModelId));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Sliderimage');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Sliderimage('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Sliderimage']))
			$model->attributes=$_GET['Sliderimage'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionAdminById($refModelId) {
	    $model = new Sliderimage('search');
	    $model->unsetAttributes();  // clear any default values
	    if (isset($_GET['Sliderimage']))
	        $model->attributes = $_GET['Sliderimage'];


	    // Find parent
	    $criteria = new CDbCriteria();
	    $criteria->addCondition('uniqId=:uniqId');
	    $criteria->params = array(
	            ':uniqId'=>$refModelId
	    );

	    $parent = Work::model()->find($criteria);
	    $parentType = 'work';

	    if(count($parent) == 0) {
	    	$parent = News::model()->find($criteria);
	    	$parentType = 'news';
	    }

	    $this->render('admin', array(
	        'model' => $model,
	        'parent' => $parent,
	        'parentType' => $parentType
	    ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Sliderimage the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Sliderimage::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Sliderimage $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='sliderimage-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    /**
     * Get nb model instances
     */
    protected function countModels() {
        $listModels = Sliderimage::model()->findAll();
        
        return count($listModels);
    }

    /**
     * Get model with order superiori to order param
     */
    protected function getModelsSupOrdre($ordre) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordre');
        $criteria->params = array(
                ':ordre'=>$ordre
        );
        $listModels = Sliderimage::model()->findAll($criteria);
        
        return $listModels;
    }

     /**
     * Get order list to create model
     */
    protected function getListOrderCreate() {
        $listModels = Sliderimage::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels)+1; $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Donne la liste des ordres à afficher dans le formulaire d'update
     */
    protected function getListOrderUpdate() {
        $listModels = Sliderimage::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels); $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant remonté)
     */
    protected function getModelsBetweenSupOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>:ordreDeb');
        $criteria->addCondition('ordre<=:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Sliderimage::model()->findAll($criteria);
        
        return $listModels;
    }

    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant redescendu)
     */
    protected function getModelsBetweenInfOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordreDeb');
        $criteria->addCondition('ordre<:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = Sliderimage::model()->findAll($criteria);
        
        return $listModels;
    }
}
