<?php

class NewsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','updateNewsSearch'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		// Get request
		//$fromNewsletter = Yii::app()->request->getQuery('newsletter', 0);
		$fromNewsletter = Yii::app()->request->getQuery('newsletter', 1);

		$model = $this->loadModel($id);
		$this->currentActiveMenuElt = 'news-view';

		// Get slider if exist
		$criteria = new CDbCriteria();
		$criteria->addCondition('ref_uniqId=:ref_uniqId');
		$criteria->params = array(
			':ref_uniqId' =>$model->uniqId
		);
		$criteria->order = 'order_slide';

		$listSlides = Sliderimage::model()->findAll($criteria);

		// Get next & prev
		$nextNewsId = $this->getNextOrPrevPhotoId($id, 'next');
		$prevNewsId = $this->getNextOrPrevPhotoId($id, 'prev');

		$nextNews = News::model()->findByPk($nextNewsId);
		$prevNews = News::model()->findByPk($prevNewsId);

		$this->render('view',array(
			'model' => $model,
			'listSlides' => $listSlides,
			'nextNews' => $nextNews,
			'prevNews' => $prevNews,
			'fromNewsletter' => $fromNewsletter
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new News;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
			$model->attributes=$_POST['News'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['News']))
		{
			$model->attributes=$_POST['News'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex($page = 1, $category = 0)
	{
		$nbNewsPerPage = 2;
		$this->currentActiveMenuElt = 'news-index';

		// Get categories
		$listCategories = Newscategory::model()->findAll(array('order' => 'ordre'));

		// Get category actived
		$activedCategoryId = $category;

		// Get articles from page X & from category actived

		$criteria = new CDbCriteria();
		$criteria->order = 'date DESC';
		$criteria->limit = $nbNewsPerPage;
		$criteria->offset = ($page - 1) * $nbNewsPerPage;

		if($category != 0) {
			$criteria->addCondition('refCategory=:refCategory');
			$criteria->params = array(
			        ':refCategory' => $category,
			);
		}

		$listNews = News::model()->findAll($criteria);


		$criteria2 = new CDbCriteria();
		$criteria2->addCondition('refCategory=:refCategory');
		$criteria2->params = array(
		        ':refCategory' => $category,
		);

		if($category != 0) {
			$nbPage = ceil(News::model()->count($criteria2) / $nbNewsPerPage);
		} else{
			$nbPage = ceil(News::model()->count() / $nbNewsPerPage);
		}		

		$this->render('index',array(
			'listNews' => $listNews,
			'listCategories' => $listCategories,
			'activedCategoryId' => $activedCategoryId,
			'page' => $page,
			'nbPage' => $nbPage
		));
	}

	public function actionUpdateNewsSearch()
	{	
		$model = new NewsForm;

		if(isset($_POST['NewsForm']))
		{
			$model->attributes=$_POST['NewsForm'];
			if($model->validate())
			{
				//$result = 'Keyworkds : '. $model->keyword;

				$criteria = new CDbCriteria();
				$criteria->order = 'date DESC';
				$criteria->addSearchCondition('title', $model->keyword);

				/*
				$criteria->addCondition('refCategory=:refCategory');
				$criteria->params = array(
				        ':refCategory' => $category,
				);
				*/

				$resultSearch = News::model()->findAll($criteria);

                $status = "ok";
			} else {
            	$status = "fail";
            }
		} else {
            $status = "missing";
        }
        
        $data = array(
            'status' => $status,
            'resultSearch' => $resultSearch
        );

        //echo json_encode($data);
        //echo json_encode($data);
		
		$this->renderPartial('_ajaxNewsResult', $data, false, true);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new News('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['News']))
			$model->attributes=$_GET['News'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return News the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=News::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param News $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='news-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	private function getNextOrPrevPhotoId($currentId, $nextOrPrev)
	{

	    $records=NULL;
	    if($nextOrPrev == "prev") {
	    	$order="ordre DESC";
	    }
	    if($nextOrPrev == "next") {
	    		$order="ordre ASC";	       
	    }

	   	$criteria = new CDbCriteria();
	   	$criteria->order = $order;
	   	$criteria->select = 'id';
	   	

	    $records=News::model()->findAll($criteria);

	    foreach($records as $i=>$r)
	       if($r->id == $currentId)
	          return isset($records[$i+1]->id) ? $records[$i+1]->id : $records[0]->id;

	    return NULL;
	}
}
