<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// Create cookie on first home visit
		$isFirstVisit = false;
		if(!isset(Yii::app()->request->cookies['nb_visit_home'])) {
			$cookie = new CHttpCookie('nb_visit_home', 1);
			$cookie->expire = time()+60*30; 
			Yii::app()->request->cookies['nb_visit_home'] = $cookie;
			$isFirstVisit = true;
		}
		

		// Set layout
		$this->layout='normal'; // menu with liseret

		// Set active menu
		$this->currentActiveMenuElt = 'home';

		// Get stared works
		$criteria = new CDbCriteria();
		$criteria->order = 'staredOrder ASC';
		$criteria->addCondition('isStared=:isStared');
		$criteria->limit = 3;
		$criteria->params = array(
			':isStared' => 1
		);

		$listStaredWorks = Work::model()->findAll($criteria);

		$this->render('index', array(
			'listStaredWorks' => $listStaredWorks,
			'isFirstVisit' => $isFirstVisit
		));
	}

	/*
	* Page Work index
	*/
	public function actionWorkIndex()
	{
		$this->currentActiveMenuElt = 'work-index';

		$this->render('work-index');
	}

	/*
	* Page Work view
	*/
	public function actionWorkView()
	{
		$this->layout = "normal-liseret-menu";
		$this->currentActiveMenuElt = 'work-view';

		$this->render('work-view');
	}

	/*
	* Page About
	*/
	public function actionAbout()
	{
		$this->layout = "normal-liseret-menu";
		$this->currentActiveMenuElt = 'about';

		// Get logos
		$listLogos = Logo::model()->findAll(array('order' => 'ordre'));

		// Get persons
		$listPersons = Person::model()->findAll(array('order' => 'ordre'));

		// Get services
		$listServices = Service::model()->findAll(array('order' => 'ordre'));

		// Get slider
		$listSlides = Slideabout::model()->findAll(array('order' => 'ordre'));
		
		$this->render('about', array(
			'listLogos' => $listLogos,
			'listPersons' => $listPersons,
			'listServices' => $listServices,
			'listSlides' => $listSlides
		));
	}

	/*
	* Page News Index
	*/
	public function actionNewsIndex()
	{
		$this->layout = "normal-liseret-menu";
		$this->currentActiveMenuElt = 'news-index';
		
		$this->render('news-index');
	}

	/*
	* Page News Vie
	*/
	public function actionNewsView()
	{
		$this->layout = "normal-liseret-menu";
		$this->currentActiveMenuElt = 'news-view';
		
		$this->render('news-view');
	}

	/*
	* Page News Vie
	*/
	public function actionUpdateNewsSearch()
	{	
		$model=new NewsForm;

		if(isset($_POST['NewsForm']))
		{
			$model->attributes=$_POST['NewsForm'];
			if($model->validate())
			{
				$result = 'Keyworkds : '. $model->keyword;
                $status = "ok";
			} else {
            	$status = "fail";
            }
		} else {
            $status = "missing";
        }
        
        $data = array(
            'status' => $status,
            'result' => $result
        );

        //echo json_encode($data);
        //echo json_encode($data);
		
		$this->renderPartial('_ajaxNewsResult', $data, false, true);
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{	
		$this->currentActiveMenuElt = 'contact';

		$listContacts = Contact::model()->findAll(array('order' => 'ordre'));

		$this->render('contact', array(
			'listContacts' => $listContacts
		));
	}

    /**
	* Search ajax news
	*/
	public function actionSearchNews()
	{
		$model=new NewsForm;

		if(isset($_POST['NewsForm']))
		{
			$model->attributes=$_POST['NewsForm'];
			if($model->validate())
			{
			
                $status = "ok";
			} else {
            	$status = "fail";
            }
		} else {
            $status = "missing";
        }
        
        $data = array(
            'status' => $status,
        );

        echo json_encode($data);
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}