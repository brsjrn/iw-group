<?php
	if (!isset($resultSearch) || count($resultSearch) == 0) {
		echo '<div class="news-message">0 news found</div>';
	}
?>


<?php

	if(isset($resultSearch) && count($resultSearch) > 0) {
		foreach ($resultSearch as $key => $news) {

			$date = $news->date;
			$tab_date = explode('-', $date);
			$day = $tab_date[2];
			$month = $tab_date[1];
			$year = $tab_date[0];
			$finalDate = $day .'.'. $month .'.'. $year;

			echo '
				<li class="news no-mobile">
					<div class="news-wrap">
						<div class="news-right">
							<div class="news-cover" style="background:url(../../images/News/'. $news->dispatchCover_image .') no-repeat center center;">
							</div>
						</div>

						<div class="news-left">
							<div class="news-toptitle">
								<span class="category">'. $news->refCategory0->title .'</span> / <span class="date">'. $finalDate .'</span>
							</div>
							<div class="news-title">'. $news->title .'</div>
							<div class="news-text">'. $news->shartDescription_text .'</div>
							<a href="'. CController::createUrl('News/view', array('id' => $news->id)) .'" class="news-btn-read">
								Read More
								<div class="liseret liseret-1"></div>
								<div class="liseret liseret-2"></div>
							</a>
						</div>

						<div class="clear"></div>
					</div>
				</li>
			';

			echo '
				<a href="'. CController::createUrl('News/view', array('id' => $news->id)) .'" class="news mobile">
					<div class="news-wrap">
						<div class="news-right">
							<div class="news-cover" style="background:url(../../images/News/'. $news->dispatchCover_image .') no-repeat center center;">
							</div>
						</div>

						<div class="mask mobile"></div>

						<div class="news-left">
							<div class="news-toptitle">
								<span class="category">'. $news->refCategory0->title .'</span> / <span class="date">'. $finalDate .'</span>
							</div>
							<div class="news-title">'. $news->title .'</div>
							<div class="news-text">'. $news->shartDescription_text .'</div>
							<div class="news-btn-read">
								Read More
								<div class="liseret liseret-1"></div>
								<div class="liseret liseret-2"></div>
							</div>
						</div>

						<div class="clear"></div>
					</div>
				</a>
			';
		}
	} else {
		if(isset($listNews) && count($listNews) == 0) {
			echo '<div class="news-message">Sorry, there is no article in this cateogy.</div>';
		}
	}
?>