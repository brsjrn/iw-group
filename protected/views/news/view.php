<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'/images/';

	$date = $model->date;
	$tab_date = explode('-', $date);
	$day = $tab_date[2];
	$month = $tab_date[1];
	$year = $tab_date[0];
	$finalDate = $day .'.'. $month .'.'. $year;

	// SHARE
	// Get current URL
	function curPageURL() {
	     $pageURL = 'http://';
	     if ($_SERVER["SERVER_PORT"] != "80") {
	      	$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	     } else {
	      	$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	     }
	     return $pageURL;
	}

	// Init share facebook
	$title=urlencode($model->title);
	$url=urlencode(curPageURL());
	$summary=urlencode(substr($model->shartDescription_text, 30));
	$imageFb=urlencode($baseUrl .'/images/News/'. $model->dispatchCover_image);
?>

<?php if($fromNewsletter == 1) { ?>
<div class="popup-newsletter">
	<div class="popup-close-btn icon-popup-close"></div>

	<div class="popup-content">
		<div class="popup-text">Get the lastest content first!</div>
		
		<div class="popup-form">
			<form>
				<input type="text" name="name" placeholder="Your name" />
				<input type="text" name="email" placeholder="Your email" />
				<input type="text" name="subscribe" value="Subscribe" class="btn-subscribe" />
			</form>
		</div>
	</div>

	<div class="popup-response">Thank you ! See you soon on your mailbox !</div>
</div>
<?php } ?>


<div class="page" id="news-view">

	<!--
	| HEADER
	-->
	<div id="header">
		<div id="background" class="full-image-parent">
			<img src="<?php echo $baseUrl .'/images/News/'. $model->insideCover_image ?>" alt="" width="2721" height="1130" class="full-image">
		</div>

		<div id="header-content">
			<div id="header-toptitle">
				<div class="anime-header-text">
					<div class="anime-wrap">
						<div class="anime-wrap-text">
							<span id="category"><?php echo $model->refCategory0->title ?></span> / <span id="date"><?php echo $finalDate ?></span>
						</div>
					</div>
				</div>
			</div>
			<div id="header-title">
				<div class="anime-header-text">
					<div class="anime-wrap">
						<div class="anime-wrap-text">
							<?php echo $model->title ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="news-content">

		<div class="small-width">
			<div id="news-topinfos">
				<span id="author">by <?php echo isset($model->refAutor0->name) ? $model->refAutor0->name : '-' ?></span> / <span id="role"><?php echo isset($model->refAutor0->job) ? $model->refAutor0->job : '-' ?></span>
			</div>

			<div id="news-text-1" class="news-text">
				<?php echo isset($model->text_1) ? $model->text_1 : '-' ?>
			</div>
		</div>

		<div class="shares">
			<div class="shares-wrap">
			    <a href="http://www.facebook.com/share.php?u=<?php echo $url ?>&title=<?php echo $title; ?>" target="_blank" class="share-facebook">
		    		<span class="icon-share icon-share-facebook"></span> <span class="text-share">Share</span>
			    </a>
			    <a href="http://twitter.com/home?status=<?php echo $title; ?> : <?php echo $url ?>" target="_blank" class="share-twitter">
			    	<span class="icon-share icon-share-twitter"></span> <span class="text-share">Tweet</span>
			    </a>
			    <a href="https://plus.google.com/share?url=<?php echo $url ?>" target="_blank" class="share-googleplus">
			    	<span class="icon-share icon-share-google"></span> <span class="text-share">Share</span>
			    </a>
			    <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url ?>&title=<?php echo $title ?>" target="_blank" class="share-linkedin">
			    	<span class="icon-share icon-share-linkedin"></span> <span class="text-share">Share</span>
			    </a>
			    <div class="clear"></div>
		    </div>
		</div>

		<!--
		| SLIDER
		-->
		<?php if(isset($listSlides) && count($listSlides) > 0) { ?>
		<div id="slider">
			<div class="slider">
				<div class="arrow arrow-left icon-slider-arrow-left no-mobile"></div>
				<div class="arrow arrow-left icon-slider-arrow-left-mobile mobile"></div>
				<div class="arrow arrow-right icon-slider-arrow-right no-mobile"></div>
				<div class="arrow arrow-right icon-slider-arrow-right-mobile mobile"></div>

				<ul class="slides"">

					<?php
						foreach ($listSlides as $key => $slide) {
							echo '
								<li style="background:url(../../images/Sliderimage/'. $slide->image .') no-repeat center center;">
								</li>
							';
						}
					?>
				</ul>

				<ul class="bullets">
					<?php
						foreach ($listSlides as $key => $slide) {
							echo '<li></li>';
						}
					?>
				</ul>
			</div>
		</div>
		<?php } ?>

		<?php if(isset($model->text_2) && $model->text_2 != '') { ?>
		<div class="small-width">
			<div id="news-text-2" class="news-text">
				<?php echo isset($model->text_2) ? $model->text_2 : '-' ?>

				<?php if(isset($model->link_url) && $model->link_url != '') { ?>
				<div class="inside-link">
					<div class="inside-link-left">
						<?php echo isset($model->link_text) ? $model->link_text : '-' ?>
					</div>
					<div class="inside-link-right">
						<div class="vertical_align">
							<div class="vertical_align_inner">
								<a href="<?php echo isset($model->link_url) ? $model->link_url : '#' ?>" class="inside-link-btn" target="_blank">Click Here</a>
							</div>
						</div>
					</div>
				</div>
				<? } ?>
			</div>
		</div>
		<?php } ?>
			
		<?php if((isset($listSlides) && count($listSlides) > 0) || (isset($model->text_2) && $model->text_2 != '')) { ?>
		<div class="shares">
			<div class="shares-wrap">
			    <a href="http://www.facebook.com/share.php?u=<?php echo $url ?>&title=<?php echo $title; ?>" target="_blank" class="share-facebook">
		    		<span class="icon-share icon-share-facebook"></span> <span class="text-share">Share</span>
			    </a>
			    <a href="http://twitter.com/home?status=<?php echo $title; ?> : <?php echo $url ?>" target="_blank" class="share-twitter">
			    	<span class="icon-share icon-share-twitter"></span> <span class="text-share">Tweet</span>
			    </a>
			    <a href="https://plus.google.com/share?url=<?php echo $url ?>" target="_blank" class="share-googleplus">
			    	<span class="icon-share icon-share-google"></span> <span class="text-share">Share</span>
			    </a>
			    <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php echo $url ?>&title=<?php echo $title ?>" target="_blank" class="share-linkedin">
			    	<span class="icon-share icon-share-linkedin"></span> <span class="text-share">Share</span>
			    </a>
			    <div class="clear"></div>
		    </div>
		</div>
		<?php } ?>
	</div>

	<div id="comments">
		<div id="disqus_thread"></div>
		<script>

		/**
		*  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
		*  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
		/*
		var disqus_config = function () {
		this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
		this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
		};
		*/
		(function() { // DON'T EDIT BELOW THIS LINE
		var d = document, s = d.createElement('script');
		s.src = '//iw-group.disqus.com/embed.js';
		s.setAttribute('data-timestamp', +new Date());
		(d.head || d.body).appendChild(s);
		})();
		</script>
		<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
		                                
	</div>

	<!--
	| PREV & NEXT NEWS
	-->
	<div id="other-news">

		<?php
			if(isset($prevNews) && count($prevNews) > 0) {

				$prevdate = $prevNews->date;
				$prevtab_date = explode('-', $prevdate);
				$prevday = $prevtab_date[2];
				$prevmonth = $prevtab_date[1];
				$prevyear = $prevtab_date[0];
				$prevfinalDate = $prevday .'.'. $prevmonth .'.'. $prevyear;

				echo '
					<a href="'. CController::createUrl('News/view', array('id' => $prevNews->id)) .'" id="prev-news" class="small-news" style="background:url(../../images/News/'. $prevNews->dispatchCover_image .') no-repeat center center;">

						<div class="mask"></div>

						<div class="small-news-text">
							<div class="infos">
								<span class="category">'. $prevNews->refCategory0->title .'</span> / <span class="date">'. $prevfinalDate .'</span>
							</div>
							<div class="title">'. $prevNews->title .'</div>
							<div class="description mobile">'. $prevNews->	shartDescription_text .'</div>
							<div class="news-btn-read mobile">
								Read More
								<div class="liseret liseret-1"></div>
								<div class="liseret liseret-2"></div>
							</div>
						</div>
					</a>
				';
			}
		?>

		<?php
			if(isset($nextNews) && count($nextNews) > 0) {

				$nextdate = $nextNews->date;
				$nexttab_date = explode('-', $nextdate);
				$nextday = $nexttab_date[2];
				$nextmonth = $nexttab_date[1];
				$nextyear = $nexttab_date[0];
				$nextfinalDate = $nextday .'.'. $nextmonth .'.'. $nextyear;

				echo '
					<a href="'. CController::createUrl('News/view', array('id' => $nextNews->id)) .'" id="prev-news" class="small-news" style="background:url(../../images/News/'. $nextNews->dispatchCover_image .') no-repeat center center;">

						<div class="mask"></div>

						<div class="small-news-text">
							<div class="infos">
								<span class="category">'. $nextNews->refCategory0->title .'</span> / <span class="date">'. $nextfinalDate .'</span>
							</div>
							<div class="title">'. $nextNews->title .'</div>
							<div class="description mobile">'. $nextNews->	shartDescription_text .'</div>
							<div class="news-btn-read mobile">
								Read More
								<div class="liseret liseret-1"></div>
								<div class="liseret liseret-2"></div>
							</div>
						</div>
					</a>
				';
			}
		?>
	</div>

	<!--
	| GET IN TOUCH
	-->
	<div id="join-team" class="next-page next-page-vertical-center anime-appear anime-appear-opacity">
		<div class="background">
			<div class="mask"></div>
			<div class="image-wrapper" style="background:url(../../dist/assets/images/News/join-team.jpg) no-repeat center center;">
			</div>
		</div>

		<div class="text">
			<div class="subtitle">
				<div class="subtitle-text">
					<div class="subtitle-text-inside">Care for a coffee ?</div>
				</div>
			</div>
			<div class="title">
				<div class="title-text">
					<div class="title-text-inside">
						<a href="<?php echo CController::createUrl('site/contact') ?>">Get in touch</a>
						<div class="arrow-title arrow-1 icon-next-page-arrow"></div>
						<div class="arrow-title arrow-2 icon-next-page-arrow"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--
	| FOOTER
	-->
	<?php $this->renderPartial('/site/_footer', array('mobile'=>'')); ?>

</div>