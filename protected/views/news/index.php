<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'/images/';
?>

<div class="page" id="news-index">

	<!--
	| SEARCH OVERLAY
	-->
	<div id="btn-close-search" class="inactive">
		<div class="trait" id="trait-1"></div>
		<div class="trait" id="trait-2"></div>
	</div>

	<div id="search" class="inactive">
		<div id="search-top">
			<div id="text-explain">Type something & press enter</div>

			<!-- AJAX SEARCH FORM -->
			<?php 
				$modelForm=new NewsForm;
				$searchForm=$this->beginWidget('CActiveForm', array(
				        'id'=>'search-form',
				        'enableClientValidation'=>true,
				        'clientOptions'=>array(
				                'validateOnSubmit'=>true,
				        ),
				));

				echo $searchForm->textField($modelForm,'keyword', array('id' => 'search-input', 'placeholder' => 'keyword'));

				 echo CHtml::ajaxSubmitButton('OK', CHtml::normalizeUrl(array('News/updateNewsSearch')), 
				     array(
				         'data'     => 'js:jQuery(this).parents("form").serialize()+"&isAjaxRequest=1"',
				         'update'	=> '#result-news',
				         'success'	=> 'function(data){
				         	$("#search").addClass("searched");
				         	$("#result-news").html(data);
				         	animeEltsDelay($("#result-news .news"), 80, true, "active", null);
				         	console.log("OK");
				         }'
			         ), 
				     array(
				        'class' => 'hiddenSubmit',
				        'id'=>'ajaxSubmit', 
				        'name'=>'ajaxSubmit'
				     ));

				 $this->endWidget();
			?>

			<!-- <input id="search-input" placeholder="keyword" /> -->

			<!-- END AJAX -->

			<div id="liseret"></div>
		</div>

		<div id="result-news"></div>
	</div>
	
	<!--
	| NAV & SEARCH
	-->
	<div id="news-nav" class="no-mobile">

		<div id="news-nav-liseret" class="no-mobile"></div>

		<ul id="categories">

			<?php
				$activeAll = '';

				if(!isset($activedCategoryId) || $activedCategoryId == 0) {
					$activeAll = 'main-active';
				}
			?>

		    <li id="category-all" class="<?php echo $activeAll ?> nav-anime">
		    	<a href="<?php echo  CController::createUrl("News/index", array('page' => 1, 'category' => 0)) ?>">All</a>
		    	<div class="liseret"></div>
	    	</li>

			<?php
				if(isset($listCategories) && count($listCategories) > 0) {
					foreach ($listCategories as $key => $category) {

						$activeCategory = '';

						if(isset($activedCategoryId) && $activedCategoryId == $category->id) {
							$activeCategory = 'main-active';
						}

						echo '
						    <li id="category-all" class="'. $activeCategory .' nav-anime">
						    	<a href="'. CController::createUrl("News/index", array('page' => 1, 'category' => $category->id)) .'">'. $category->title .'</a>
						    	<div class="liseret"></div>
					    	</li>
						';
					}
				}
			?>
		</ul>

		<div id="btn-search" class="icon-search nav-anime"></div>
	</div>

	<!--
	| LOGO & SCROLL DOWN
	-->
	<div id="news-logo" class="icon-small-logo"></div>

	<div id="news-scroll-down" class="icon-news-scroll-down no-mobile"></div>


	<!--
	| LSIT NEWS
	-->
	<ul id="list-news">

		<?php

			if(isset($listNews) && count($listNews) > 0) {
				foreach ($listNews as $key => $news) {

					$date = $news->date;
					$tab_date = explode('-', $date);
					$day = $tab_date[2];
					$month = $tab_date[1];
					$year = $tab_date[0];
					$finalDate = $day .'.'. $month .'.'. $year;

					echo '
						<li class="news no-mobile">
							<div class="news-wrap">
								<div class="news-right">
									<div class="news-cover" style="background:url(../../images/News/'. $news->dispatchCover_image .') no-repeat center center;">
									</div>
								</div>

								<div class="news-left">
									<div class="news-toptitle">
										<span class="category">'. $news->refCategory0->title .'</span> / <span class="date">'. $finalDate .'</span>
									</div>
									<div class="news-title">'. $news->title .'</div>
									<div class="news-text">'. $news->shartDescription_text .'</div>
									<a href="'. CController::createUrl('News/view', array('id' => $news->id)) .'" class="news-btn-read">
										Read More
										<div class="liseret liseret-1"></div>
										<div class="liseret liseret-2"></div>
									</a>
								</div>

								<div class="clear"></div>
							</div>
						</li>
					';

					echo '
						<a href="'. CController::createUrl('News/view', array('id' => $news->id)) .'" class="news mobile">
							<div class="news-wrap">
								<div class="news-right">
									<div class="news-cover" style="background:url(../../images/News/'. $news->dispatchCover_image .') no-repeat center center;">
									</div>
								</div>

								<div class="mask mobile"></div>

								<div class="news-left">
									<div class="news-toptitle">
										<span class="category">'. $news->refCategory0->title .'</span> / <span class="date">'. $finalDate .'</span>
									</div>
									<div class="news-title">'. $news->title .'</div>
									<div class="news-text">'. $news->shartDescription_text .'</div>
									<div class="news-btn-read">
										Read More
										<div class="liseret liseret-1"></div>
										<div class="liseret liseret-2"></div>
									</div>
								</div>

								<div class="clear"></div>
							</div>
						</a>
					';
				}
			} else {
				if(isset($listNews) && count($listNews) == 0) {
					echo '<div class="news-message">Sorry, there is no article in this cateogy.</div>';
				}
			}
		?>
	</ul>

	<div id="pagination">
		<?php
			if($page > 1) {
				echo '
					<a href="'. CController::createUrl("News/index", array('page' => $page - 1, 'category' => $activedCategoryId)) .'" class="prev-next-link" id="prev-link">
						<span class="icon-page-prev"></span> Previous page
					</a>
				';
			}

			if($page < $nbPage) {
				echo '
					<a href="'. CController::createUrl("News/index", array('page' => $page + 1, 'category' => $activedCategoryId)) .'" class="prev-next-link" id="next-link">
						Next page <span class="icon-page-next"></span>
					</a>
				';
			}
		?>

		<div id="pages" class="no-mobile">
			<ul>
				<?php
					$activePage = '';

					for ($i=1; $i <= $nbPage ; $i++) { 
						if($i == $page) {
							$activePage = 'active';
						} else {
							$activePage = '';
						}

						echo '<li class="'. $activePage .'"><a href="'. CController::createUrl("News/index", array('page' => $i, 'category' => $activedCategoryId)) .'">'. $i .'</a></li>';
					}
				?>
			</ul>
		</div>
	</div>

	<!--
	| FOOTER
	-->
	<?php $this->renderPartial('/site/_footer', array('mobile'=>'')); ?>

</div>