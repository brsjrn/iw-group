<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'images/';
?>

<?php if($isFirstVisit) { ?>
<div id="intro">
	<img src="<?php echo $imagesUrl .'/intro-logo-iwgroup_1loop.gif' ?>" id="intro-logo" alt="" width="76.5" height="149.5" loop=none>
</div>
<?php } ?>

<div id="home-wrapper">

	<div id="intro-transition">
			<img src="<?php echo $imagesUrl .'/home/pre-home-transition4.png' ?>" alt="" width="1800" height="2400">
	</div>


	<div class="page page-type-1" id="home">

		<div id="works-urls">
			<?php
				foreach ($listStaredWorks as $key => $work) {
					echo '<div class="work-url">'. CController::createUrl('Work/view', array('id' => $work->id)) .'</div>';
				}
			?>
		</div>

		<div id="home-video">
			<div id="home-video-close">close</div>

			<iframe src="https://player.vimeo.com/video/198625704" width="100%" height="100%" id="home-video-player" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

		</div>

		<div id="home-content" class="no-mobile"> <!-- Normal || exergue -->
			<div id="home-content-left" class="home-content-side">
				<div id="media-area">

					<div class="media active">
						<div class="media-inner" style="background:url('<?php echo $imagesUrl ."home/video-bg.jpg" ?>') no-repeat center center;">

							<div class="video-play-btn">
								<div class="vertical_align">
									<div class="vertical_align_inner">
										<div class="icon-play"></div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php
						if(isset($listStaredWorks) && count($listStaredWorks) > 0) {
							foreach ($listStaredWorks as $key => $work) {
								echo '
									<a href="'. CController::createUrl('Work/view', array('id' => $work->id)) .'" class="media zoomable">'
									;?>

										<div class="media-inner" style="background:url('<?php echo $baseUrl ."/images/Work/". $work->cover2_image ?>') no-repeat center center;"></div>

									<?php echo '</a>
								';
							}
						}
					?>

					<div id="media-transition">
						<img src="<?php echo $imagesUrl .'/home/media-transition2.png' ?>" alt="" width="1000" height="1000">
					</div>

					<div id="view-project-link">
						<a href="#">
							View Project
							<div id="liseret"></div>
						</a>
					</div>
				</div>
			</div>

			<div id="home-content-right" class="home-content-side">
				<div id="top">
					<div id="logo" class="icon-small-logo"></div>
					<a href="mailto:hello@iwgroup.com" id="mailto">
						hello@iwgroup.com
						<div id="liseret"></div>
					</a>
					<div class="clear"></div>
				</div>

				<div class="vertical_align">
					<div class="vertical_align_inner">
						<div id="texts">

							<div id="toptitle">
								<ul>
								    <li>IW Group</li>
								</ul>
							</div>

							<div id="title">
								<ul>
								    <li>
								    	<p>Crafting Content</p>
								    	<p>for Diverse</p>
								    	<p>Consumers</p>
							    	</li>

							    	<?php
							    		if(isset($listStaredWorks) && count($listStaredWorks) > 0) {
							    			foreach ($listStaredWorks as $key => $work) {
							    				echo '
					    						    <li>
					    						    	<p class="brand">'. $work->title .'</p>
					    						    	'. $work->staredSubtitle .'
					    					    	</li>
							    				';
							    			}
							    		}
							    	?>
								    <!--
							    	<li>
								    	<p class="brand">Coca Cola.</p>
								    	<p>Dolor Sit</p>
								    	<p>Amet Lorem</p>
								    	<p>3</p>
							    	</li>
							    	<li>
								    	<p class="brand">Pepsi.</p>
								    	<p>Dolor Sit</p>
								    	<p>Amet Lorem</p>
								    	<p>4</p>
							    	</li>
							    	-->
								</ul>
							</div>


							<div id="description">
								<ul>
								    <li>
								    	To cut through a saturated market, we use a machete called relevancy.<br />
								    	We're a full-service advertising and PR agency that engages audiences by leveraging relevant and actionable insights into authentic relationships
							    	</li>

							    	<?php
							    		if(isset($listStaredWorks) && count($listStaredWorks) > 0) {
							    			foreach ($listStaredWorks as $key => $work) {
							    				echo '
					    						    <li>'. $work->staredDescription_text .'</li>
							    				';
							    			}
							    		}
							    	?>
								    <!--
							    	<li>
								    	3 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							    	</li>
							    	<li>
								    	4 Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
							    	</li>
							    	-->
								</ul>
							</div>

						</div>
					</div>
				</div>

				<ul id="socials">
				    <li><a href="https://www.instagram.com/iwgroup" target="_blank" class="icon-social-instagram"></a></li>
				    <li><a href="https://twitter.com/iwgroup" target="_blank" class="icon-social-twitter"></a></li>
				    <li><a href="https://www.facebook.com/IWGroup/
" target="_blank" class="icon-social-facebook"></a></li>
				</ul>

				<ul id="work-num">
				    <li><span>01</span></li>
				    <li><span>02</span></li>
				    <li><span>03</span></li>
				</ul>

				<div id="home-nav">

					<div class="vertical_align">
						<div class="vertical_align_inner">
							<ul class="bullets-nav">
							    <li class="active"></li>
							    <li></li>
							    <li></li>
							    <li></li>
							</ul>
						</div>
					</div>

					<div id="arrows">
						<div id="arrow-top" class="nav-arrow">
							<div class="nav-arrow-child icon-nav-arrow-top" id="arrow-top-1"></div>
							<div class="nav-arrow-child icon-nav-arrow-top" id="arrow-top-2"></div>
						</div>
						<div id="liseret"></div>
						<div id="arrow-bottom" class="nav-arrow">
							<div class="nav-arrow-child icon-nav-arrow-bottom" id="arrow-bottom-1"></div>
							<div class="nav-arrow-child icon-nav-arrow-bottom" id="arrow-bottom-2"></div>
						</div>
					</div>
				</div>
			</div>

			<div class="clear"></div>
		</div>

		<!--
		| RESPONSIVE MOBILE
		-->
		<div id="home-content-mobile" class="mobile">
			<!-- Home video -->
			<div class="home-mobile-elt active">
				<div class="home-mobile-elt-background full-image-parent">
					<img src="<?php echo $imagesUrl .'/Work/work-0.jpg' ?>" alt="" width="1132" height="1395" class="full-image">
				</div>

				<div class="home-mobile-elt-content">
					<div class="video-play-btn-mobile">
						Play Video
						<div class="liseret"></div>
					</div>
					<div class="toptitle">IW Group</div>
					<div class="title">Crafting Content for Diverse Consumers</div>
					<div class="text">To cut through a saturated market, we use a machete called relevancy.<br />
					We're a full-service advertising and PR agency that engages audiences by leveraging relevant and actionable insights into authentic relationships. </div>
				</div>
			</div>
			
			<!-- Work 1 -->

			<?php
				if(isset($listStaredWorks) && count($listStaredWorks) > 0) {
					foreach ($listStaredWorks as $key => $work) {
						echo '
							<div class="home-mobile-elt">
								<div class="home-mobile-elt-background full-image-parent">
									<img src="'. $baseUrl .'/images/Work/'. $work->cover2_image .'" alt="" width="1132" height="1395" class="full-image">
								</div>

								<div class="home-mobile-elt-content">

									<a href="'. CController::createUrl('Work/view', array('id' => $work->id)) .'" class="link">
										View Project
										<div class="liseret"></div>
									</a>
									<div class="title-band">'. $work->title .'</div>
									<div class="title">'. $work->staredSubtitle .'</div>
									<div class="text">'. $work->staredDescription_text .'</div>
								</div>
							</div>
						';
					}
				}
			?>

			<!-- Bottom home -->
			<div id="home-mobile-bottom">
				<ul id="socials-mobile" class="active">
				    <li><a href="https://www.instagram.com/iwgroup" target="_blank" class="icon-social-instagram"></a></li>
				    <li><a href="https://twitter.com/iwgroup" target="_blank" class="icon-social-twitter"></a></li>
				    <li><a href="https://www.facebook.com/IWGroup/
" target="_blank" class="icon-social-facebook"></a></li>
				</ul>

				<ul id="works-list-mobile">
				    <li>01</li>
				    <li>02</li>
				    <li>03</li>
				</ul>

				<div id="home-mobile-next" class="icon-nav-arrow-bottom"></div>
			</div>

		</div>

	</div>
</div>