<?php
	if (isset($result)) {
		//echo $result;
	} else {
		//echo 'Result still empty';
	}
?>


<li class="news no-mobile">
	<div class="news-wrap">
		<div class="news-right">
			<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
			</div>
		</div>

		<div class="mask mobile"></div>

		<div class="news-left">
			<div class="news-toptitle">
				<span class="category">Insights</span> / <span class="date">10.26.2016</span>
			</div>
			<div class="news-title">Approaching Branded Content</div>
			<div class="news-text">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
			</div>
			<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news-btn-read">
				Read More
				<div class="liseret liseret-1"></div>
				<div class="liseret liseret-2"></div>
			</a>
		</div>

		<div class="clear"></div>
	</div>
</li>

<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news mobile">
			<div class="news-wrap">
				<div class="news-right">
					<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
					</div>
				</div>

				<div class="mask mobile"></div>

				<div class="news-left">
					<div class="news-toptitle">
						<span class="category">Insights</span> / <span class="date">10.26.2016</span>
					</div>
					<div class="news-title">Approaching Branded Content</div>
					<div class="news-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</div>
					<div class="news-btn-read">
						Read More
						<div class="liseret liseret-1"></div>
						<div class="liseret liseret-2"></div>
					</div>
				</div>

				<div class="clear"></div>
			</div>
		</a>

<li class="news no-mobile">
	<div class="news-wrap">
		<div class="news-right">
			<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
			</div>
		</div>

		<div class="mask mobile"></div>

		<div class="news-left">
			<div class="news-toptitle">
				<span class="category">Insights</span> / <span class="date">10.26.2016</span>
			</div>
			<div class="news-title">Approaching Branded Content</div>
			<div class="news-text">
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
			</div>
			<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news-btn-read">
				Read More
				<div class="liseret liseret-1"></div>
				<div class="liseret liseret-2"></div>
			</a>
		</div>

		<div class="clear"></div>
	</div>
</li>

<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news mobile">
			<div class="news-wrap">
				<div class="news-right">
					<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
					</div>
				</div>

				<div class="mask mobile"></div>

				<div class="news-left">
					<div class="news-toptitle">
						<span class="category">Insights</span> / <span class="date">10.26.2016</span>
					</div>
					<div class="news-title">Approaching Branded Content</div>
					<div class="news-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</div>
					<div class="news-btn-read">
						Read More
						<div class="liseret liseret-1"></div>
						<div class="liseret liseret-2"></div>
					</div>
				</div>

				<div class="clear"></div>
			</div>
		</a>