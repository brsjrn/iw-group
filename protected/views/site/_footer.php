<footer class="<?php echo $mobile ?>">
	<div id="footer-content">
		<div id="logo" class="icon-small-logo"></div>

		<ul id="menu-footer">
			<li><a href="<?php echo CController::createUrl('site/index') ?>">Home</a></li>
			<li><a href="<?php echo CController::createUrl('site/about') ?>">About</a></li>
			<li><a href="<?php echo CController::createUrl('Work/index') ?>">Work</a></li>
			<li><a href="<?php echo CController::createUrl('News/index') ?>">Blog</a></li>
			<li><a href="<?php echo CController::createUrl('site/contact') ?>">Contact</a></li>
		</ul>

		<div id="copyright">&copy;<?php echo date("Y"); ?> IW Group. All Rights Reserved.</div>

		<div class="clear"></div>
	</div>
</footer>