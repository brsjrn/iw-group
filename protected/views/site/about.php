<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'/images/';
?>

<div class="page" id="about">

	<!--
	| HEADER
	-->
	<div id="header" class="no-mobile">
		<div id="background" style="background-image: url(../../dist/assets/images/about/header.jpg)"></div>

		<div class="vertical_align">
			<div class="vertical_align_inner">
				<div id="header-align">
					<div id="toptitle" class="anime-header-text">
						<div class="anime-wrap">
							<div class="anime-wrap-text">About us</div>
						</div>
					</div>

					<div id="title">
						<div class="title anime-header-text" id="title-1">
							<div class="anime-wrap">
								<div class="anime-wrap-text">Driving 
									<span class="write-text write-text-1 active-text">Insights,</span>
									<span class="write-text write-text-1">Engagement,</span>
									<span class="write-text write-text-1">Strategy,</span>
									<span class="write-text write-text-1">Behaviors,</span>
									<span class="write-text write-text-1">Innovation,</span>
								</div>
							</div>
						</div>
						<div class="title anime-header-text" id="title-2">
							<div class="anime-wrap">
								<div class="anime-wrap-text">Building 
								<span class="write-text write-text-2 active-text">Excitement</span>
								<span class="write-text write-text-2">Relationships</span>
								<span class="write-text write-text-2">360 Campaigns</span>
								<span class="write-text write-text-2">Awareness</span>
								<span class="write-text write-text-2">Experiences</span>
								</div>
							</div>
						</div>
					</div>

					<div id="subtitle" class="anime-header-text">
						<div class="anime-wrap">
							<div class="anime-wrap-text">with</div>
						</div>
					</div>

					<?php if(isset($listLogos) && count($listLogos) > 0) { ?>
					<ul id="list-logos">

						<?php
							foreach ($listLogos as $key => $logo) {
								echo '
								    <li>
								    	<img src="'. $baseUrl .'/images/Logo/'. $logo->image .'" alt="">
							    	</li>
								';
							}
						?>
					</ul>
					<?php } ?>
				</div>
			</div>
		</div>

		<div id="go-down" class="icon-work-index-next-arrow"></div>

		<div id="logo" class="icon-small-logo"></div>

	</div>

	<div id="header-mobile" class="mobile">
		<div id="background-mobile" style="background-image: url(../../dist/assets/images/about/header.jpg)"></div>

		<div class="header-mobile-content">
			<div class="toptitle-mobile">About us</div>
			<div class="title-mobile">Anticipage <span class="orange">tomorrow,</span></div>
			<div class="title-mobile">experience <span class="orange">today</span></div>
			<div class="subtitle-mobile">with</div>

			<ul id="list-logos-mobile">
			    <li>
			    	<img src="<?php echo $imagesUrl .'/Logo/logo-1.png' ?>" alt="">
		    	</li>
			    <li>
			    	<img src="<?php echo $imagesUrl .'/Logo/logo-2.png' ?>" alt="">
		    	</li>
			    <li>
			    	<img src="<?php echo $imagesUrl .'/Logo/logo-3.png' ?>" alt="">
		    	</li>
			    <li>
			    	<img src="<?php echo $imagesUrl .'/Logo/logo-4.png' ?>" alt="">
		    	</li>
		    	<div class="clear"></div>
			</ul>
		</div>
	</div>

	<div class="page-content">
		<!--
		| TEXT 1
		-->
		<div id="text-1" class="anime-appear-cascade-texts">
			<div class="small-width">
				<div id="text-1-toptitle" class="cascade-text">The Story</div>
				<div id="text-1-title" class="cascade-text">
					IW Group was founded on a simple idea :<br :>
				</div>
				<div id="text-1-text" class="cascade-text">
					<p>In order to cut through a saturated market, one needs a machete called relevancy.</p>

					<p>What is relevancy? Behaviors, interests, insights, and anything that will make our target stop and pay attention. We live in a global world, and more and more consumers have multicultural influences and nuances. With decades of experience, we're poised to take on the next generation of consumers.</p>

					<p>Finding a relevant contact point is only the first step. From there, we craft content that enriches consumers' lives on behalf of our brands. Without this authentic communication, brands run the risk of alienating their target.</p>

					<p>We create relevant and authentic relationships between consumers and brands.</p>
				</div>	
			</div>
		</div>

		<div id="about-liseret-separation-1" class="about-liseret-separation"></div>

		<!--
		| TROMBI
		-->
		<div id="trombi">

			<div id="trombi-toptitle">Leadership</div>

			<div class="normal-width">

			<?php if(isset($listPersons) && count($listPersons) > 0) { ?>
				<div id="list-persons" class="anime-appear-cascade-lines">

					<?php
						$cptPerson = 0;
						$nbPerson = count($listPersons);

						foreach ($listPersons as $key => $person) {
							// Is First
							if($cptPerson == 0) {
								// Open first line
								echo '<div class="trombi-line cascade-line">';
							}

							if($cptPerson%3 == 0 && $cptPerson != 0) {
								// close line
								echo '</div>';

								// Open new line
								echo '<div class="trombi-line cascade-line">';
							}

							// Person
							echo '
								<div class="trombi-perso">
									<div class="trombi-perso-container">
										<img src="'. $baseUrl .'/images/Person/'. $person->image .'" alt="" class="photo" width="753" height="560">

										<img src="'. $imagesUrl .'/Person/mask.png' .'" alt="" class="mask" width="753" height="1120">

										<div class="name">
											<div class="vertical_align">
												<div class="vertical_align_inner">
													<div class="name-text">'. $person->name .'</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							';

							// Is Last
							if($cptPerson == $nbPerson - 1) {
								// close line
								echo '</div>';
							}

							$cptPerson++;
						}
					?>

					<div class="clear"></div>
				</div>
				<?php } ?>

			</div>

		</div>

		<div id="about-liseret-separation-2" class="about-liseret-separation"></div>

		<!--
		| BRAND EXPERIENCE
		-->
		<div id="brand-experience" class="anime-appear-cascade">
			<div class="small-width">
				<div class="title cascade-elt">Brand Experience</div>
				<div class="text cascade-elt">
					IW Group works across industry verticals to drive effective communication between niche consumers and the brands that serve them.
				</div>

				<div id="liseret" class="cascade-elt"></div>

				<ul id="list-brands">
				    <li class="cascade-elt">
				    	<div class="brand-type">Luxury & Premium Brands</div>
				    	<div class="brand-names">Bulgari, Blancpain, Chloé, Guess, Harry Winston, Marc jacobs</div>
				    </li>
				    <li class="cascade-elt">
				    	<div class="brand-type">Luxury & Premium Brands</div>
				    	<div class="brand-names">Bulgari, Blancpain, Chloé, Guess, Harry Winston, Marc jacobs</div>
				    </li>
				    <li class="cascade-elt">
				    	<div class="brand-type">Luxury & Premium Brands</div>
				    	<div class="brand-names">Bulgari, Blancpain, Chloé, Guess, Harry Winston, Marc jacobs</div>
				    </li>
				    <li class="cascade-elt">
				    	<div class="brand-type">Luxury & Premium Brands</div>
				    	<div class="brand-names">Bulgari, Blancpain, Chloé, Guess, Harry Winston, Marc jacobs</div>
			    	</li>
				    <div class="clear"></div>
				</ul>
			</div>
		</div>

		<!--
		| SLIDER
		-->
		<?php if(isset($listSlides) && count($listSlides) > 0) { ?>
		<section id="slider" class="anime-appear anime-appear-opacity">
			<div class="slider">
				<div class="arrow arrow-left icon-slider-arrow-left no-mobile"></div>
				<div class="arrow arrow-left icon-slider-arrow-left-mobile mobile"></div>
				<div class="arrow arrow-right icon-slider-arrow-right no-mobile"></div>
				<div class="arrow arrow-right icon-slider-arrow-right-mobile mobile"></div>

				<ul class="slides"">

					<?php
						foreach ($listSlides as $key => $slide) {
							echo '
								<li style="background:url(../../images/Slideabout/'. $slide->image .') no-repeat center center;">
								</li>
							';
						}
					?>
				</ul>

				<ul class="bullets">
					<?php
						foreach ($listSlides as $key => $slide) {
							echo '<li></li>';
						}
					?>
				</ul>
			</div>
		</section>
		<?php } ?>

		<!--
		| SERVICES
		-->
		<section id="services" class="anime-refresh-resize">
			<div class="small-width">
				<div id="services-toptitle">Our Services</div>

				<?php if(isset($listServices) && count($listServices) > 0) { ?>
					<div id="list-services" class="list-with-background">

						<div class="list-backgrounds no-mobile">
							<?php
								foreach ($listServices as $key => $service) {
									echo '
										<div class="background-mask" style="background:url(../../images/Service/'. $service->bakground_image .') no-repeat center center;">
										</div>
									';
								}
							?>
						</div>

						<ul>
							<?php
								foreach ($listServices as $key => $service) {
									echo '
										<li><span class="background-target">'. $service->title .'</span></li>
									';
								}
							?>
					    </ul>
					</div>
				<?php } ?>

			</div>
		</section>

		<!--
		| GET IN TOUCH
		-->
		<div id="get-in-touch" class="next-page next-page-vertical-center anime-appear anime-appear-opacity no-mobile zoomable">
			<div class="background">
				<div class="mask"></div>
				<div class="image-wrapper" style="background:url(../../dist/assets/images/about/get-in-touch.jpg) no-repeat center center;">
				</div>
			</div>

			<div class="text">
				<div class="subtitle">
					<div class="subtitle-text">
						<div class="subtitle-text-inside">Care for a coffee ?</div>
					</div>
				</div>
				<div class="title">
					<div class="title-text">
						<div class="title-text-inside">
							<a href="<?php echo CController::createUrl('site/contact') ?>">Get in touch</a>
							<div class="arrow-title arrow-1 icon-next-page-arrow"></div>
							<div class="arrow-title arrow-2 icon-next-page-arrow"></div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--
		| FOOTER
		-->
		<?php $this->renderPartial('_footer', array(
			'mobile' => ''
		)); ?>
	</div>
</div>