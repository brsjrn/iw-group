<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'images/';
?>

<div class="page" id="contact">

	<div id="contact-inner" class="no-mobile">

		<!-- Logo -->
		<div id="logo-contact" class="icon-small-logo"></div>

		<!-- Illustration -->
		<div id="contact-illustration" class="ratio-resize-width" data-ratio="0.65">
			<div id="contact-illustration-inner" style="background:url(../../dist/assets/images/contact/illustration3.jpg) no-repeat center center;">
			</div>
		</div>
		

		<!-- Content -->
		<div class="vertical_align">
			<div class="vertical_align_inner">
				<div class="contact-title contact-text">
					<div class="contact-text-line">
						<div class="anime-header-text">
							<div class="anime-wrap">
								<div class="anime-wrap-text">
									Don’t be shy - drop us a line.
								</div>
							</div>
						</div>
					</div>
					<div class="contact-text-line">
						<div class="anime-header-text">
							<div class="anime-wrap">
								<div class="anime-wrap-text">
									We are looking forward to speaking with you!
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="contact-email contact-text">
					<div class="anime-header-text">
						<div class="anime-wrap">
							<div class="anime-wrap-text">
								<p><a href="mailto:hello@iwgroup.com">hello@iwgroup.com</a></p>
							</div>
						</div>
					</div>
				</div>

				<div id="list-spots">
					<ul>
						<?php
							if(isset($listContacts) && count($listContacts) > 0) {
								foreach ($listContacts as $key => $contact) { ?>
								    <li>
								    	<div class="spot-elt">
								    		<?php
								    			if(isset($contact->city) && $contact->city != '') {
								    				echo '<div class="city">'. $contact->city .'</div>';
								    			}
								    		?>
									    	<div class="adress">
								    			<?php
								    				if(isset($contact->adress1) && $contact->adress1 != '') {
								    					echo '<p>'. $contact->adress1 .'</p>';
								    				}
								    				if(isset($contact->adress2) && $contact->adress2 != '') {
								    					echo '<p>'. $contact->adress2 .'</p>';
								    				}
								    			?>
									    	</div>
									    	<?php
									    		if(isset($contact->phone) && $contact->phone != '') {
													echo '
														<div class="phone">
															T <a href="tel:'. $contact->phone .'">'. $contact->phone .'</a>
														</div>
													';
									    		}
									    	?>
								    	</div>
								    </li>
								<?php }
							}
						?>
					</ul>
				</div>

				<div id="socials">
					<ul>
					    <li id="social-instagram">
					    	<canvas id="instagram-canvas" class="contact-canvas" width="60" height="60"></canvas>
					    	<a href="https://www.instagram.com/iwgroup" class="icon-social-instagram"  target="_blank"></a>
				    	</li>
					    <li>
					    	<canvas id="twitter-canvas" class="contact-canvas" width="60" height="60"></canvas>
					    	<a href="https://twitter.com/iwgroup" class="icon-social-twitter"  target="_blank"></a>
				    	</li>
					    <li>
					    	<canvas id="facebook-canvas" class="contact-canvas" width="60" height="60"></canvas>
					    	<a href="https://www.facebook.com/IWGroup/
" class="icon-social-facebook"  target="_blank"></a>
				    	</li>
					</ul>
				</div>
			</div>
		</div>
	</div>

	<!--
	| MOBILE RESPONSIVE
	-->
	<div id="contact-mobile" class="mobile">
		<div class="contact-title contact-text">
			Don’t be shy - drop us a line.
			We are looking forward to speaking with you!
		</div>

		<div class="contact-email contact-text">
			<a href="mailto:hello@iwgroup.com">hello@iwgroup.com</a>
		</div>

		<div id="list-spots">
			<ul>
			    <li>
			    	<div class="spot-elt">
				    	<div class="city">Los Angeles</div>
				    	<div class="adress">
				    		<p>8687 Melrose Ave. Suite G540</p>
				    		<p>West Hollywood, CA 90069</p>
				    	</div>
				    	<div class="phone">
				    		T <a href="tel:310-289-5500">310.289.5500</a>
				    	</div>
			    	</div>
			    </li>
			    <li>
			    	<div class="spot-elt">
				    	<div class="city">New York</div>
				    	<div class="adress">
				    		<p>215 Park Avenue S, 6Th Floor</p>
				    		<p>New York, NY 10003</p>
				    	</div>
				    	<div class="phone">
				    		T <a href="tel:646-979-8959">646.979.8959</a>
				    	</div>
			    	</div>
			    </li>
			    <li>
			    	<div class="spot-elt">
				    	<div class="city">San Francisco</div>
				    	<div class="adress">
				    		<p>33 New Montgomery St Suite 990</p>
				    		<p>San Francisco, CA 94105</p>
				    	</div>
				    	<div class="phone">
				    		T <a href="tel:646-979-8959">646.979.8959</a>
				    	</div>
			    	</div>
			    </li>
			</ul>
		</div>

		<div id="socials">
			<ul>
			    <li id="social-instagram">
			    	<canvas id="instagram-canvas" class="contact-canvas" width="60" height="60"></canvas>
			    	<a href="https://www.instagram.com/iwgroup" target="_blank" class="icon-social-instagram"></a>
		    	</li>
			    <li>
			    	<canvas id="twitter-canvas" class="contact-canvas" width="60" height="60"></canvas>
			    	<a href="https://twitter.com/iwgroup" target="_blank" class="icon-social-twitter"></a>
		    	</li>
			    <li>
			    	<canvas id="facebook-canvas" class="contact-canvas" width="60" height="60"></canvas>
			    	<a href="https://www.facebook.com/IWGroup/
" target="_blank" class="icon-social-facebook"></a>
		    	</li>
			</ul>
		</div>

	</div>

	<!--
	| FOOTER
	-->
	<?php $this->renderPartial('_footer', array('mobile'=>'mobile')); ?>

</div>