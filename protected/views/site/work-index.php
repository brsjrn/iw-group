<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'images/';
?>

<div class="page" id="work-index">

	<div id="work-index-inner" class="no-mobile">
		<div id="work-index-left" class="work-index-side">
			<div class="work-index-side-inner">
				<div id="work-index-title">
					<div class="title-elt">
						<h1>Lax</h1>
						<div class="work-type">Mobile Application</div>
					</div>
					<div class="title-elt">
						<h1>Beaming</h1>
						<div class="work-type">Mobile Application. Design. Photography Direction.</div>
					</div>
				</div>

				<a href="<?php echo CController::createUrl('site/workView') ?>" class="cover">
					<div class="cover-inner full-image-parent">
						<img src="<?php echo $imagesUrl .'/Work/work-index-1.jpg' ?>" alt="" width="1132" height="1395" class="full-image">
					</div>
				</a>
				<a href="<?php echo CController::createUrl('site/workView') ?>" class="cover">
					<div class="cover-inner full-image-parent">
						<img src="<?php echo $imagesUrl .'/Work/work-index-3.jpg' ?>" alt="" width="1132" height="1395" class="full-image">
					</div>
				</a>
			</div>
		</div> 

		<div id="work-index-right" class="work-index-side">
			<div class="work-index-side-inner">
				<a href="<?php echo CController::createUrl('site/workView') ?>" class="cover">
					<div class="cover-inner full-image-parent">
						<img src="<?php echo $imagesUrl .'/Work/work-index-2.jpg' ?>" alt="" width="1132" height="1395" class="full-image">
					</div>
				</a>

				<a href="<?php echo CController::createUrl('site/workView') ?>" class="cover">
					<div class="cover-inner full-image-parent">
						<img src="<?php echo $imagesUrl .'/Work/work-index-4.jpg' ?>" alt="" width="1132" height="1395" class="full-image">
					</div>
				</a>
			</div>

			<div id="work-index-nums">
				<div id="current-num">
					<div class="current-num-elt">01</div>
					<div class="current-num-elt">02</div>
				</div>
				<div id="total-num">02</div>
			</div>
		</div>

		<a href="<?php echo CController::createUrl('site/workView') ?>" id="view-project-link">
			View case study
			<div id="liseret"></div>
		</a>

		<div id="work-index-next" class="icon-work-index-next-arrow"></div>

		<div class="clear"></div>
	</div> 

	<!--
	| MOBILE RESPONSIVE
	-->
	<div id="work-index-mobile" class="mobile">

		<div id="works-list-mobile">

			<a href="<?php echo CController::createUrl('site/workView') ?>" class="work-elt-mobile">
				<div class="background" style="background:url(../../dist/assets/images/Work/mobile-1.jpg) no-repeat center center;"></div>
				<div class="mask"></div>
				<div class="texts-top">
					<div class="title">Lax</div>
					<div class="type">Mobile Application</div>
				</div>

				<div class="view-project">View Project</div>
			</a>

			<a href="<?php echo CController::createUrl('site/workView') ?>" class="work-elt-mobile">
				<div class="background" style="background:url(../../dist/assets/images/Work/mobile-1.jpg) no-repeat center center;"></div>
				<div class="mask"></div>
				<div class="texts-top">
					<div class="title">Lax</div>
					<div class="type">Mobile Application</div>
				</div>

				<div class="view-project">View Project</div>
			</a>

			<a href="<?php echo CController::createUrl('site/workView') ?>" class="work-elt-mobile">
				<div class="background" style="background:url(../../dist/assets/images/Work/mobile-1.jpg) no-repeat center center;"></div>
				<div class="mask"></div>
				<div class="texts-top">
					<div class="title">Lax</div>
					<div class="type">Mobile Application</div>
				</div>

				<div class="view-project">View Project</div>
			</a>

			<a href="<?php echo CController::createUrl('site/workView') ?>" class="work-elt-mobile">
				<div class="background" style="background:url(../../dist/assets/images/Work/mobile-1.jpg) no-repeat center center;"></div>
				<div class="mask"></div>
				<div class="texts-top">
					<div class="title">Lax</div>
					<div class="type">Mobile Application</div>
				</div>

				<div class="view-project">View Project</div>
			</a>

		</div>

	</div>

	<!--
	| FOOTER
	-->
	<?php $this->renderPartial('_footer', array('mobile'=>'')); ?>
</div>