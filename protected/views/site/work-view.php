<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'images/';
?>

<div class="page" id="work-view">

	<section id="header" class="no-mobile">

		<div id="header-left">
			<div class="vertical_align">
				<div class="vertical_align_inner">
					<h1>
						<div class="anime-header-text">
							<div class="anime-wrap">
								<div class="anime-wrap-text">
									Beaming
								</div>
							</div>
						</div>
					</h1>
					<div class="work-type">
						<div class="anime-header-text">
							<div class="anime-wrap">
								<div class="anime-wrap-text">
										Brand Strategy. Design. Photography Direction.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="go-down" class="icon-work-index-next-arrow"></div>
		</div>
		
		<div id="header-right">
			<div id="header-right-inner">
				<div id="cover">
					<div id="cover-inner" style="background:url(../../dist/assets/images/Work/work-index-2.jpg) no-repeat center center;">
					</div>
				</div>
			</div>
		</div>

		<div class="clear"></div>
	</section>

	<section id="header-mobile" class="mobile">
		<div class="background" style="background:url(../../dist/assets/images/Work/mobile-1.jpg) no-repeat center center;"></div>
		<div class="mask"></div>
		<div class="texts">
			<div class="title">Beaming</div>
			<div class="type">Brand Strategy. Design. Photography. Art Direction.</div>
		</div>
	</section>


	<div class="normal-width">

		<!--
		| TEXT 1
		-->
		<section id="text-1" class="work-view-section text-section anime-appear anime-appear-bloc-text">
			<div class="title">
				<span>Starting Point.</span>
			</div>
			<div class="text">
				<div class="text-inner">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</div>
			</div>
			<div class="clear"></div>
		</section>

		<!--
		| PARALLAX
		-->

		<section id="parallax" class="work-view-section anime-appear anime-appear-parallax no-mobile" data-parallax="scroll" data-image-src="<?php echo $imagesUrl .'/Work/image-1.jpg' ?>">
		</section>

		<section id="test" class="mobile" style="background:url(../../dist/assets/images/Work/image-1.jpg) no-repeat center center;"></section>

		<!--
		| TEXT 2
		-->
		<section id="text-2" class="work-view-section anime-appear anime-appear-bloc-text">
			<div id="text-2-inner">
				<div class="title">Taking the brand further</div>
				<div class="text">
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.</p>
				</div>
			</div>
		</section>
	</div>

	<!--
	| VIDEO
	-->
	<section id="video" class="work-view-section" style="background:url(../../dist/assets/images/Work/video.jpg) no-repeat center center;">
	</section>

	<!--
	| TEXT 3
	-->
	<div class="normal-width">
		<section id="text-3" class="work-view-section text-section anime-appear anime-appear-bloc-text">
			<div class="title">
				<span>Web Design.</span>
			</div>
			<div class="text">
				<div class="text-inner">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</div>
			</div>
			<div class="clear"></div>
		</section>
	</div>

	<!--
	| SLIDER
	-->
	<section id="slider" class="work-view-section anime-appear anime-appear-opacity">
		<div class="slider">
			<div class="arrow arrow-left icon-slider-arrow-left no-mobile"></div>
			<div class="arrow arrow-left icon-slider-arrow-left-mobile mobile"></div>
			<div class="arrow arrow-right icon-slider-arrow-right no-mobile"></div>
			<div class="arrow arrow-right icon-slider-arrow-right-mobile mobile"></div>

			<ul class="slides"">
			    <li style="background:url(../../dist/assets/images/Work/slide-test-1.jpg) no-repeat center center;">
			    </li>
			    <li style="background:url(../../dist/assets/images/Work/slide-test-2.jpg) no-repeat center center;">
			    </li>
			    <li style="background:url(../../dist/assets/images/Work/slide-test-3.jpg) no-repeat center center;">
			    </li>
			</ul>

			<ul class="bullets">
			    <li></li>
			    <li></li>
			    <li></li>
			</ul>
		</div>
	</section>
	
	<!--
	| IMAGE 2
	-->
	<section id="image-1" class="work-view-section">
		<img src="<?php echo $imagesUrl .'/Work/patchwork.jpg' ?>" class="anime-appear anime-appear-image" alt="" width="100%" height="auto">
	</section>

	<!--
	| TEXT 4
	-->
	<div class="normal-width">
		<section id="text-4" class="work-view-section text-section anime-appear anime-appear-bloc-text">
			<div class="title">
				<span>The Outcome.</span>
			</div>
			<div class="text">
				<div class="text-inner">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				</div>
			</div>
			<div class="clear"></div>
		</section>
	</div>


	<!--
	| NEXT-WORK
	-->
	<div d="next-work" class="work-view-section next-page next-page-vertical-center anime-appear anime-appear-opacity no-mobile">
		<div class="background">
			<div class="mask"></div>
			<div class="image-wrapper" style="background:url(../../dist/assets/images/Work/next-work.jpg) no-repeat center center;">
			</div>
		</div>

		<div class="text">
			<div class="subtitle">
				<div class="subtitle-text">
					<div class="subtitle-text-inside">Next Project</div>
				</div>
			</div>
			<div class="title">
				<div class="title-text">
					<div class="title-text-inside">
						<a href="#">Lexus</a>

						<div class="arrow-title arrow-1 icon-next-page-arrow"></div>
						<div class="arrow-title arrow-2 icon-next-page-arrow"></div>
					</div>
					<!-- <div class="arrow-1 icon-next-page-arrow"></div> -->
				</div>
			</div>
		</div>
	</div>

	<!--
	| FOOTER
	-->
	<?php $this->renderPartial('_footer', array('mobile'=>'')); ?>

</div>