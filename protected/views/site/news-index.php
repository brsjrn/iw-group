<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'/images/';
?>

<div class="page" id="news-index">

	<!--
	| SEARCH OVERLAY
	-->
	<div id="btn-close-search" class="inactive">
		<div class="trait" id="trait-1"></div>
		<div class="trait" id="trait-2"></div>
	</div>

	<div id="search" class="inactive">
		<div id="search-top">
			<div id="text-explain">Type something & press enter</div>

			<!-- AJAX SEARCH FORM -->
			<?php 
				$modelForm=new NewsForm;
				$searchForm=$this->beginWidget('CActiveForm', array(
				        'id'=>'search-form',
				        'enableClientValidation'=>true,
				        'clientOptions'=>array(
				                'validateOnSubmit'=>true,
				        ),
				));

				echo $searchForm->textField($modelForm,'keyword', array('id' => 'search-input', 'placeholder' => 'keyword'));

				 echo CHtml::ajaxSubmitButton('OK', CHtml::normalizeUrl(array('site/updateNewsSearch')), 
				     array(
				         'data'     => 'js:jQuery(this).parents("form").serialize()+"&isAjaxRequest=1"',
				         'update'	=> '#result-news',
				         'success'	=> 'function(data){
				         	$("#search").addClass("searched");
				         	$("#result-news").html(data);
				         	animeEltsDelay($("#result-news .news"), 80, true, "active", null);
				         	console.log("OK");
				         }'
			         ), 
				     array(
				        'class' => 'hiddenSubmit',
				        'id'=>'ajaxSubmit', 
				        'name'=>'ajaxSubmit'
				     ));

				 $this->endWidget();
			?>

			<!-- <input id="search-input" placeholder="keyword" /> -->

			<!-- END AJAX -->

			<div id="liseret"></div>
		</div>

		<div id="result-news">
			<!--
			<li class="news">
				<div class="news-wrap">
					<div class="news-left">
						<div class="news-toptitle">
							<span class="category">Insights</span> / <span class="date">10.26.2016</span>
						</div>
						<div class="news-title">Approaching Branded Content</div>
						<div class="news-text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						</div>
						<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news-btn-read">
							Read More
							<div class="liseret liseret-1"></div>
							<div class="liseret liseret-2"></div>
						</a>
					</div>

					<div class="news-right">
						<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
						</div>
					</div>

					<div class="clear"></div>
				</div>
			</li>
			<li class="news">
				<div class="news-wrap">
					<div class="news-left">
						<div class="news-toptitle">
							<span class="category">Insights</span> / <span class="date">10.26.2016</span>
						</div>
						<div class="news-title">Approaching Branded Content</div>
						<div class="news-text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						</div>
						<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news-btn-read">
							Read More
							<div class="liseret liseret-1"></div>
							<div class="liseret liseret-2"></div>
						</a>
					</div>

					<div class="news-right">
						<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
						</div>
					</div>

					<div class="clear"></div>
				</div>
			</li>
			<li class="news">
				<div class="news-wrap">
					<div class="news-left">
						<div class="news-toptitle">
							<span class="category">Insights</span> / <span class="date">10.26.2016</span>
						</div>
						<div class="news-title">Approaching Branded Content</div>
						<div class="news-text">
							Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
						</div>
						<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news-btn-read">
							Read More
							<div class="liseret liseret-1"></div>
							<div class="liseret liseret-2"></div>
						</a>
					</div>

					<div class="news-right">
						<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
						</div>
					</div>

					<div class="clear"></div>
				</div>
			</li>
			-->
		</div>
	</div>
	
	<!--
	| NAV & SEARCH
	-->
	<div id="news-nav" class="no-mobile">

		<div id="news-nav-liseret" class="no-mobile"></div>

		<ul id="categories">
		    <li id="category-all" class="main-active nav-anime">
		    	<a href="#">All</a>
		    	<div class="liseret"></div>
	    	</li>
		    <li id="category-insights" class="nav-anime">
		    	<a href="#">Insights</a>
		    	<div class="liseret"></div>
	    	</li>
		    <li id="category-news" class="nav-anime">
		    	<a href="#">News</a>
		    	<div class="liseret"></div>
	    	</li>
		    <li id="category-events" class="nav-anime">
		    	<a href="#">Events</a>
		    	<div class="liseret"></div>
	    	</li>
		    <li id="category-lorem" class="nav-anime">
		    	<a href="#">Lorem</a>
		    	<div class="liseret"></div>
	    	</li>
		    <li id="category-ipsum" class="nav-anime">
		    	<a href="#">Ipsum</a>
		    	<div class="liseret"></div>
	    	</li>
		</ul>

		<div id="btn-search" class="icon-search nav-anime"></div>
	</div>

	<!--
	| LOGO & SCROLL DOWN
	-->
	<div id="news-logo" class="icon-small-logo"></div>

	<div id="news-scroll-down" class="icon-news-scroll-down no-mobile"></div>


	<!--
	| LSIT NEWS
	-->
	<ul id="list-news">

		<li class="news no-mobile">
			<div class="news-wrap">
				<div class="news-right">
					<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
					</div>
				</div>

				<div class="mask mobile"></div>

				<div class="news-left">
					<div class="news-toptitle">
						<span class="category">Insights</span> / <span class="date">10.26.2016</span>
					</div>
					<div class="news-title">Approaching Branded Content</div>
					<div class="news-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</div>
					<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news-btn-read">
						Read More
						<div class="liseret liseret-1"></div>
						<div class="liseret liseret-2"></div>
					</a>
				</div>

				<div class="clear"></div>
			</div>
		</li>

		<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news mobile">
			<div class="news-wrap">
				<div class="news-right">
					<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
					</div>
				</div>

				<div class="mask mobile"></div>

				<div class="news-left">
					<div class="news-toptitle">
						<span class="category">Insights</span> / <span class="date">10.26.2016</span>
					</div>
					<div class="news-title">Approaching Branded Content</div>
					<div class="news-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</div>
					<div class="news-btn-read">
						Read More
						<div class="liseret liseret-1"></div>
						<div class="liseret liseret-2"></div>
					</div>
				</div>

				<div class="clear"></div>
			</div>
		</a>

		<li class="news no-mobile">
			<div class="news-wrap">
				<div class="news-right">
					<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
					</div>
				</div>

				<div class="mask mobile"></div>

				<div class="news-left">
					<div class="news-toptitle">
						<span class="category">Insights</span> / <span class="date">10.26.2016</span>
					</div>
					<div class="news-title">Approaching Branded Content</div>
					<div class="news-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</div>
					<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news-btn-read">
						Read More
						<div class="liseret liseret-1"></div>
						<div class="liseret liseret-2"></div>
					</a>
				</div>

				<div class="clear"></div>
			</div>
		</li>

		<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news mobile">
			<div class="news-wrap">
				<div class="news-right">
					<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
					</div>
				</div>

				<div class="mask mobile"></div>

				<div class="news-left">
					<div class="news-toptitle">
						<span class="category">Insights</span> / <span class="date">10.26.2016</span>
					</div>
					<div class="news-title">Approaching Branded Content</div>
					<div class="news-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</div>
					<div class="news-btn-read">
						Read More
						<div class="liseret liseret-1"></div>
						<div class="liseret liseret-2"></div>
					</div>
				</div>

				<div class="clear"></div>
			</div>
		</a>

		<li class="news no-mobile">
			<div class="news-wrap">
				<div class="news-right">
					<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
					</div>
				</div>

				<div class="mask mobile"></div>

				<div class="news-left">
					<div class="news-toptitle">
						<span class="category">Insights</span> / <span class="date">10.26.2016</span>
					</div>
					<div class="news-title">Approaching Branded Content</div>
					<div class="news-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</div>
					<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news-btn-read">
						Read More
						<div class="liseret liseret-1"></div>
						<div class="liseret liseret-2"></div>
					</a>
				</div>

				<div class="clear"></div>
			</div>
		</li>

		<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news mobile">
			<div class="news-wrap">
				<div class="news-right">
					<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
					</div>
				</div>

				<div class="mask mobile"></div>

				<div class="news-left">
					<div class="news-toptitle">
						<span class="category">Insights</span> / <span class="date">10.26.2016</span>
					</div>
					<div class="news-title">Approaching Branded Content</div>
					<div class="news-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</div>
					<div class="news-btn-read">
						Read More
						<div class="liseret liseret-1"></div>
						<div class="liseret liseret-2"></div>
					</div>
				</div>

				<div class="clear"></div>
			</div>
		</a>

		<li class="news no-mobile">
			<div class="news-wrap">
				<div class="news-right">
					<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
					</div>
				</div>

				<div class="mask mobile"></div>

				<div class="news-left">
					<div class="news-toptitle">
						<span class="category">Insights</span> / <span class="date">10.26.2016</span>
					</div>
					<div class="news-title">Approaching Branded Content</div>
					<div class="news-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</div>
					<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news-btn-read">
						Read More
						<div class="liseret liseret-1"></div>
						<div class="liseret liseret-2"></div>
					</a>
				</div>

				<div class="clear"></div>
			</div>
		</li>

		<a href="<?php echo CController::createUrl('site/newsView') ?>" class="news mobile">
			<div class="news-wrap">
				<div class="news-right">
					<div class="news-cover" style="background:url(../../dist/assets/images/News/news-index-cover.jpg) no-repeat center center;">
					</div>
				</div>

				<div class="mask mobile"></div>

				<div class="news-left">
					<div class="news-toptitle">
						<span class="category">Insights</span> / <span class="date">10.26.2016</span>
					</div>
					<div class="news-title">Approaching Branded Content</div>
					<div class="news-text">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
					</div>
					<div class="news-btn-read">
						Read More
						<div class="liseret liseret-1"></div>
						<div class="liseret liseret-2"></div>
					</div>
				</div>

				<div class="clear"></div>
			</div>
		</a>

	</ul>

	<div id="pagination">
		<a href="#" class="prev-next-link" id="prev-link">
			<span class="icon-page-prev"></span> Previous page
		</a>

		<a href="#" class="prev-next-link" id="next-link">
			Next page <span class="icon-page-next"></span>
		</a>

		<div id="pages" class="no-mobile">
			<ul>
			    <li class="active"><a href="#">1</a></li>
			    <li><a href="#">2</a></li>
			    <li><a href="#">3</a></li>
			    <li><a href="#">4</a></li>
			    <li><a href="#">5</a></li>
			    <li><a href="#">6</a></li>
			</ul>
		</div>
	</div>

	<!--
	| FOOTER
	-->
	<?php $this->renderPartial('_footer', array('mobile'=>'')); ?>

</div>