<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'/images/';
?>

<div class="page" id="news-view">

	<!--
	| HEADER
	-->
	<div id="header">
		<div id="background" class="full-image-parent">
			<img src="<?php echo $imagesUrl .'/News/cover.jpg' ?>" alt="" width="2721" height="1130" class="full-image">
		</div>

		<div id="header-content">
			<div id="header-toptitle">
				<div class="anime-header-text">
					<div class="anime-wrap">
						<div class="anime-wrap-text">
							<span id="category">Inisghts</span> / <span id="date">10.26.2016</span>
						</div>
					</div>
				</div>
			</div>
			<div id="header-title">
				<div class="anime-header-text">
					<div class="anime-wrap">
						<div class="anime-wrap-text">
							Approaching Branded Content.
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div id="news-content">

		<div class="small-width">
			<div id="news-topinfos">
				<span id="author">by Ben Kochavy</span> / <span id="role">Maker Innovator @IWGROUP</span>
			</div>

			<div id="news-text-1" class="news-text">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>
		</div>

		<div class="shares">
			<div class="shares-wrap">
			    <a href="#" class="share-facebook">
		    		<span class="icon-share icon-share-facebook"></span> <span class="text-share">Share</span>

			    </a>
			    <a href="#" class="share-twitter">
			    	<span class="icon-share icon-share-twitter"></span> <span class="text-share">Tweet</span>
			    </a>
			    <a href="#" class="share-googleplus">
			    	<span class="icon-share icon-share-google"></span> <span class="text-share">Share</span>
			    </a>
			    <a href="#" class="share-linkedin">
			    	<span class="icon-share icon-share-linkedin"></span> <span class="text-share">Share</span>
			    </a>
			    <div class="clear"></div>
		    </div>
		</div>

		<!--
		| SLIDER
		-->
		<div id="slider">
			<div class="slider">
				<div class="arrow arrow-left icon-slider-arrow-left no-mobile"></div>
				<div class="arrow arrow-left icon-slider-arrow-left-mobile mobile"></div>
				<div class="arrow arrow-right icon-slider-arrow-right no-mobile"></div>
				<div class="arrow arrow-right icon-slider-arrow-right-mobile mobile"></div>

				<ul class="slides"">
				    <li style="background:url(../../dist/assets/images/Work/slide-test-1.jpg) no-repeat center center;">
				    </li>
				    <li style="background:url(../../dist/assets/images/Work/slide-test-2.jpg) no-repeat center center;">
				    </li>
				    <li style="background:url(../../dist/assets/images/Work/slide-test-3.jpg) no-repeat center center;">
				    </li>
				</ul>

				<ul class="bullets">
				    <li></li>
				    <li></li>
				    <li></li>
				</ul>
			</div>
		</div>

		<div class="small-width">
			<div id="news-text-2" class="news-text">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				<div class="inside-link">
					<div class="inside-link-left">
						Want to see some examples of excellent brand Content? Check out our collection of ones we found articularly 
						swipe worthy!  
					</div>
					<div class="inside-link-right">
						<div class="vertical_align">
							<div class="vertical_align_inner">
								<a href="#" class="inside-link-btn">Click Here</a>
							</div>
						</div>
					</div>
				</div>

				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			</div>

		</div>

		<div class="shares">
			<div class="shares-wrap">
			    <a href="#" class="share-facebook">
		    		<span class="icon-share icon-share-facebook"></span> <span class="text-share">Share</span>

			    </a>
			    <a href="#" class="share-twitter">
			    	<span class="icon-share icon-share-twitter"></span> <span class="text-share">Tweet</span>
			    </a>
			    <a href="#" class="share-googleplus">
			    	<span class="icon-share icon-share-google"></span> <span class="text-share">Share</span>
			    </a>
			    <a href="#" class="share-linkedin">
			    	<span class="icon-share icon-share-linkedin"></span> <span class="text-share">Share</span>
			    </a>
			    <div class="clear"></div>
		    </div>
		</div>
	</div>

	<!--
	| PREV & NEXT NEWS
	-->
	<div id="other-news">
		<a href="<?php echo CController::createUrl('site/newsView') ?>" id="prev-news" class="small-news" style="background:url(../../dist/assets/images/News/thumb-1.jpg) no-repeat center center;">

			<div class="mask"></div>

			<div class="small-news-text">
				<div class="infos">
					<span class="category">Insights</span> / <span class="date">10.26.2016</span>
				</div>
				<div class="title">
					Automotive Industry Condesguir Amet
				</div>
				<div class="description mobile">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
				</div>
				<div class="news-btn-read mobile">
					Read More
					<div class="liseret liseret-1"></div>
					<div class="liseret liseret-2"></div>
				</div>
			</div>
		</a>

		<a href="<?php echo CController::createUrl('site/newsView') ?>" id="prev-news" class="small-news" style="background:url(../../dist/assets/images/News/thumb-2.jpg) no-repeat center center;">

			<div class="mask"></div>

			<div class="small-news-text">
				<div class="infos">
					<span class="category">Insights</span> / <span class="date">10.26.2016</span>
				</div>
				<div class="title">
					Automotive Industry Condesguir Amet
				</div>
				<div class="description mobile">
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
				</div>
				<div class="news-btn-read mobile">
					Read More
					<div class="liseret liseret-1"></div>
					<div class="liseret liseret-2"></div>
				</div>
			</div>
		</a>
	</div>

	<!--
	| GET IN TOUCH
	-->
	<div id="join-team" class="next-page next-page-vertical-center anime-appear anime-appear-opacity">
		<div class="background">
			<div class="mask"></div>
			<div class="image-wrapper" style="background:url(../../dist/assets/images/News/join-team.jpg) no-repeat center center;">
			</div>
		</div>

		<div class="text">
			<div class="subtitle">
				<div class="subtitle-text">
					<div class="subtitle-text-inside">Want to work with us ?</div>
				</div>
			</div>
			<div class="title">
				<div class="title-text">
					<div class="title-text-inside">
						<a href="#">Join our team</a>
						<div class="arrow-title arrow-1 icon-next-page-arrow"></div>
						<div class="arrow-title arrow-2 icon-next-page-arrow"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--
	| FOOTER
	-->
	<?php $this->renderPartial('_footer', array('mobile'=>'')); ?>

</div>