<?php
/* @var $this WorkController */
/* @var $data Work */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isStared')); ?>:</b>
	<?php echo CHtml::encode($data->isStared); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('staredSubtitle')); ?>:</b>
	<?php echo CHtml::encode($data->staredSubtitle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('staredDescription_text')); ?>:</b>
	<?php echo CHtml::encode($data->staredDescription_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('staredOrder')); ?>:</b>
	<?php echo CHtml::encode($data->staredOrder); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('cover1_image')); ?>:</b>
	<?php echo CHtml::encode($data->cover1_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cover1Mobile_image')); ?>:</b>
	<?php echo CHtml::encode($data->cover1Mobile_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cover2_image')); ?>:</b>
	<?php echo CHtml::encode($data->cover2_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cover2Mobile_image')); ?>:</b>
	<?php echo CHtml::encode($data->cover2Mobile_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content1Title')); ?>:</b>
	<?php echo CHtml::encode($data->content1Title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content1_text')); ?>:</b>
	<?php echo CHtml::encode($data->content1_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parallax_image')); ?>:</b>
	<?php echo CHtml::encode($data->parallax_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parallaxMobile_image')); ?>:</b>
	<?php echo CHtml::encode($data->parallaxMobile_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content2Title')); ?>:</b>
	<?php echo CHtml::encode($data->content2Title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content2_text')); ?>:</b>
	<?php echo CHtml::encode($data->content2_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('video_image')); ?>:</b>
	<?php echo CHtml::encode($data->video_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('videoMobile_image')); ?>:</b>
	<?php echo CHtml::encode($data->videoMobile_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('video_url')); ?>:</b>
	<?php echo CHtml::encode($data->video_url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content3Title')); ?>:</b>
	<?php echo CHtml::encode($data->content3Title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content3_text')); ?>:</b>
	<?php echo CHtml::encode($data->content3_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contentBig_image')); ?>:</b>
	<?php echo CHtml::encode($data->contentBig_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content4Title')); ?>:</b>
	<?php echo CHtml::encode($data->content4Title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content4_text')); ?>:</b>
	<?php echo CHtml::encode($data->content4_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bottom_image')); ?>:</b>
	<?php echo CHtml::encode($data->bottom_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bottomMobile_image')); ?>:</b>
	<?php echo CHtml::encode($data->bottomMobile_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ordre')); ?>:</b>
	<?php echo CHtml::encode($data->ordre); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uniqId')); ?>:</b>
	<?php echo CHtml::encode($data->uniqId); ?>
	<br />

	*/ ?>

</div>