<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'images/';
?>

<div class="page page-extend" id="work-view">

	<section id="header" class="no-mobile">

		<div id="header-left">
			<div class="vertical_align">
				<div class="vertical_align_inner">
					<h1>
						<div class="anime-header-text">
							<div class="anime-wrap">
								<div class="anime-wrap-text">
									<?php echo isset($model->title) ? $model->title : '-' ?>
								</div>
							</div>
						</div>
					</h1>
					<div class="work-type">
						<div class="anime-header-text">
							<div class="anime-wrap">
								<div class="anime-wrap-text">
										<?php echo isset($model->type) ? $model->type : '-' ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div id="go-down" class="icon-work-index-next-arrow"></div>
		</div>
		
		<div id="header-right">
			<div id="header-right-inner">
				<div id="cover">
					<div id="cover-inner" style="background:url(../../images/Work/<?php echo $model->cover2_image ?>) no-repeat center center;">
					</div>
				</div>
			</div>
		</div>

		<div class="clear"></div>
	</section>

	<section id="header-mobile" class="mobile">
		<div class="background" style="background:url(../../images/Work/<?php echo $model->cover2Mobile_image ?>) no-repeat center center;"></div>
		<div class="mask"></div>
		<div class="texts">
			<div class="title"><?php echo isset($model->title) ? $model->title : '-' ?></div>
			<div class="type"><?php echo isset($model->type) ? $model->type : '-' ?></div>
		</div>
	</section>


	<div class="page-content">
		<div class="normal-width">

			<!--
			| TEXT 1
			-->
			<?php if(isset($model->content1_text) && $model->content1_text != '') { ?>
			<section id="text-1" class="work-view-section text-section anime-appear anime-appear-bloc-text">
				<div class="title">
					<span><?php echo $model->content1Title ?></span>
				</div>
				<div class="text">
					<div class="text-inner"><?php echo $model->content1_text ?></div>
				</div>
				<div class="clear"></div>
			</section>
			<?php } ?>

			<!--
			| PARALLAX
			-->
			<?php if(isset($model->parallax_image) && $model->parallax_image != '') { ?>
			<section id="parallax" class="work-view-section anime-appear anime-appear-parallax no-mobile" data-parallax="scroll" data-speed="0.6" data-image-src="<?php echo $baseUrl .'/images/Work/'. $model->parallax_image ?>">
			</section>
			<?php } ?>

			<?php if(isset($model->parallax_image) && $model->parallax_image != '') { ?>
			<section id="test" class="mobile" style="background:url(../../images/Work/<?php echo $model->parallax_image ?>) no-repeat center center;"></section>
			<?php } ?>

			<!--
			| TEXT 2
			-->
			<?php if(isset($model->content2_text) && $model->content2_text != '') { ?>
			<section id="text-2" class="work-view-section anime-appear anime-appear-bloc-text">
				<div id="text-2-inner">
					<div class="title"><?php echo $model->content2Title ?></div>
					<div class="text"><?php echo $model->content2_text ?></div>
				</div>
			</section>
			<?php } ?>
		</div>

		<!--
		| VIDEO
		-->
		<?php if(isset($model->video_url) && $model->video_url != '') { ?>
		<section id="video" class="work-view-section full-image-parent">

			<img src="<?php echo $baseUrl .'/images/Work/'. $model->video_image ?>" class="video-cover full-image" width="2719" height="1480">

			<div class="video-play-btn">
				<div class="vertical_align">
					<div class="vertical_align_inner">
						<div class="icon-play"></div>
					</div>
				</div>
			</div>

			<iframe src="https://player.vimeo.com/video/<?php echo $model->video_url ?>?portrait=0&badge=0" width="100%" height="100%" frameborder="0" class="video-iframe" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
		</section>
		<?php } ?>

		<!--
		| TEXT 3
		-->
		<?php if(isset($model->content3_text) && $model->content3_text != '') { ?>
		<div class="normal-width">
			<section id="text-3" class="work-view-section text-section anime-appear anime-appear-bloc-text">
				<div class="title">
					<span><?php echo isset($model->content3Title) ? $model->content3Title : '-' ?></span>
				</div>
				<div class="text">
					<div class="text-inner"><?php echo isset($model->content3_text) ? $model->content3_text : '-' ?></div>
				</div>
				<div class="clear"></div>
			</section>
		</div>
		<?php } ?>

		<!--
		| SLIDER
		-->
		<?php if(isset($listSlides) && count($listSlides) > 0) { ?>
		<section id="slider" class="work-view-section anime-appear anime-appear-opacity">
			<div class="slider">
				<div class="arrow arrow-left icon-slider-arrow-left no-mobile"></div>
				<div class="arrow arrow-left icon-slider-arrow-left-mobile mobile"></div>
				<div class="arrow arrow-right icon-slider-arrow-right no-mobile"></div>
				<div class="arrow arrow-right icon-slider-arrow-right-mobile mobile"></div>

				<ul class="slides"">

					<?php
						foreach ($listSlides as $key => $slide) {
							echo '
								<li style="background:url(../../images/Sliderimage/'. $slide->image .') no-repeat center center;">
								</li>
							';
						}
					?>
				</ul>

				<ul class="bullets">
					<?php
						foreach ($listSlides as $key => $slide) {
							echo '<li></li>';
						}
					?>
				</ul>
			</div>
		</section>
		<?php } ?>
		
		<!--
		| IMAGE 2
		-->
		<?php if(isset($model->contentBig_image) && $model->contentBig_image != '') { ?>
		<section id="image-1" class="work-view-section">
			<img src="<?php echo $baseUrl .'/images/Work/'. $model->contentBig_image  ?>" class="anime-appear anime-appear-image" alt="" width="100%" height="auto">
		</section>
		<?php } ?>

		<!--
		| TEXT 4
		-->
		<?php if(isset($model->content4_text) && $model->content4_text != '') { ?>
		<div class="normal-width">
			<section id="text-4" class="work-view-section text-section anime-appear anime-appear-bloc-text">
				<div class="title">
					<span><?php echo isset($model->content4Title) ? $model->content4Title : '-' ?></span>
				</div>
				<div class="text">
					<div class="text-inner"><?php echo isset($model->content3_text) ? $model->content3_text : '-' ?></div>
				</div>
				<div class="clear"></div>
			</section>
		</div>
		<?php } ?>

		<!--
		| NEXT-WORK
		-->
		<?php if(isset($nextWork) && count($nextWork) > 0) { ?>
		<div id="next-work" class="work-view-section next-page next-page-vertical-center anime-appear anime-appear-opacity no-mobile zoomable">
			<div class="background">
				<div class="mask"></div>
				<div class="image-wrapper" style="background:url(../../images/Work/<?php echo $model->bottom_image ?>) no-repeat center center;">
				</div>
			</div>

			<div class="text">
				<div class="subtitle">
					<div class="subtitle-text">
						<div class="subtitle-text-inside">Next Project</div>
					</div>
				</div>
				<div class="title">
					<div class="title-text">
						<div class="title-text-inside">
							<a href="<?php echo CController::createUrl('Work/view', array('id' => $nextWork->id)) ?>"><?php echo $nextWork->title ?></a>

							<div class="arrow-title arrow-1 icon-next-page-arrow"></div>
							<div class="arrow-title arrow-2 icon-next-page-arrow"></div>
						</div>
						<!-- <div class="arrow-1 icon-next-page-arrow"></div> -->
					</div>
				</div>
			</div>
		</div>
		<?php } ?>

		<!--
		| FOOTER
		-->
		<?php $this->renderPartial('/site/_footer', array('mobile'=>'')); ?>
	</div>

</div>