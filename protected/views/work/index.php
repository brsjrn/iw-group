<?php
	/* @var $this SiteController */
	//$this->pageTitle=Yii::app()->name;

	// Paths
	$baseUrl = Yii::app()->baseUrl;
	$assetsUrl = $baseUrl .'/dist/assets/';
	$imagesUrl = $assetsUrl .'images/';
?>

<div class="page" id="work-index">

	<div id="work-index-list-urls">
		<?php
			foreach ($listWorks as $key => $work) {
				echo '<div class="work-url">'. CController::createUrl('Work/view', array('id' => $work->id)) .'</div>';
			}
		?>
	</div>

	<div id="work-index-inner" class="no-mobile">
		<div id="work-index-left" class="work-index-side">
			<div class="work-index-side-inner">
				<div id="work-index-title">
					<?php
						foreach ($listWorks as $key => $work) {
							echo '
								<div class="title-elt">
									<h1>'. $work->title .'</h1>
									<div class="work-type">'. $work->type .'</div>
								</div>
							';
						}
					?>
				</div>

				<?php
					foreach ($listWorks as $key => $work) {
						echo '
							<a href="'. CController::createUrl('Work/view', array('id' => $work->id)) .'" class="cover zoomable">'; ?>

								<div class="cover-inner full-image-parent" style="background:url('<?php echo $baseUrl ."/images/Work/". $work->cover1_image ?>') no-repeat center center;">
								
								<!--	<img src="'. $baseUrl .'/images/Work/'. $work->cover1_image .'" alt="" width="1132" height="1395" class="full-image"> -->

								</div>

						<?php echo '</a>
						';
					}
				?>
			</div>
		</div> 

		<div id="work-index-right" class="work-index-side">
			<div class="work-index-side-inner">
				<?php
					foreach ($listWorks as $key => $work) {
						echo '
							<a href="'. CController::createUrl('Work/view', array('id' => $work->id)) .'" class="cover zoomable">'; ?>

								<div class="cover-inner full-image-parent" style="background:url('<?php echo $baseUrl ."/images/Work/". $work->cover2_image ?>') no-repeat center center;">

									<!--	<img src="'. $baseUrl .'/images/Work/'. $work->cover2_image .'" alt="" width="1132" height="1395" class="full-image"> -->
								</div>

						<?php echo '</a>
						';
					}
				?>
			</div>

			<div id="work-index-nums">
				<div id="current-num">
					<?php
						for ($i=1; $i <= count($listWorks) ; $i++) { 
							if($i < 10) {
								echo '<div class="current-num-elt">0'. $i .'</div>';
							} else {
								echo '<div class="current-num-elt">'. $i .'</div>';
							}
						}
					?>
				</div>
				<?php
					if(count($listWorks) < 10) {
						echo '<div id="total-num">0'. count($listWorks) .'</div>';
					} else {
						echo '<div id="total-num">'. count($listWorks) .'</div>';
					}
				?>
			</div>
		</div>

		<a href="<?php echo CController::createUrl('site/workView') ?>" id="view-project-link">
			View case study
			<div id="liseret"></div>
		</a>

		<div id="work-index-next" class="icon-work-index-next-arrow"></div>

		<div class="clear"></div>
	</div> 

	<!--
	| MOBILE RESPONSIVE
	-->
	<div id="work-index-mobile" class="mobile">

		<div id="works-list-mobile">

			<?php
				foreach ($listWorks as $key => $work) {
					echo '
						<a href="'. CController::createUrl('Work/view', array('id' => $work->id)) .'" class="work-elt-mobile">
							<div class="background" style="background:url(../../images/Work/'. $work->cover2Mobile_image .') no-repeat center center;"></div>
							<div class="mask"></div>
							<div class="texts-top">
								<div class="title">'. $work->title .'</div>
								<div class="type">'. $work->type .'</div>
							</div>

							<div class="view-project">View Project</div>
						</a>
					';
				}
			?>

		</div>

	</div>

	<!--
	| FOOTER
	-->
	<?php $this->renderPartial('/site/_footer', array('mobile'=>'')); ?>
</div>