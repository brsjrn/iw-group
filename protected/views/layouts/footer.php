<footer>
	<div id="footer-content">
		<div class="normal-width">
			<div id="copyright">&copy;<?php echo date("Y"); ?> IW Group. All Rights Reserved.</div>

			<ul id="menu-footer">
				<li><a href="#">Home</a></li>
				<li><a href="#">About</a></li>
				<li><a href="#">Work</a></li>
				<li><a href="#">Blog</a></li>
				<li><a href="#">Contact</a></li>
			</ul>

			<div class="clear"></div>
			<div id="logo" class="icon-small-logo"></div>
		</div>
	</div>
</footer>