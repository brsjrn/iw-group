<?php 
    /* @var $this Controller */ 
    $baseUrl = Yii::app()->baseUrl;
    $assetsUrl = $baseUrl .'/dist/assets/';
    $imagesUrl = $assetsUrl .'images/';

    // Menu mobile background color
    $mobileMenuBg = "";
    if($this->currentActiveMenuElt == 'home') {
    	$mobileMenuBg = "transparent";
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, minimum-scale=1">
        <meta name="format-detection" content="telephone=no">
            
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<!-- Google fonts -->
	
	<!-- Vendors -->
	<!-- <link rel="stylesheet" href="<?php echo $baseUrl .'/vendor/uikit/css/uikit.min.css' ?>" />-->

	<!-- Css -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/dist/assets/css/style.concat.css" />
	<!-- Prod
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/dist/assets/css/style.min.css" />
	-->

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body scrollbar>

	<div id="menu-btn-open" class="menu-btn-open no-mobile">
		<div class="tiret tiret-1"></div>
		<div class="tiret tiret-2"></div>
		<div class="tiret tiret-3"></div>
	</div>

	<!-- <div id="menu-btn-full-sreen" class="icon-full-screen toggle-full-screen"></div> -->

	<div id="menu-background" class="no-mobile">
		<div id="menu-content">
			<canvas id="canvas-menu-1" width="300" height="300"> 
			  Texte alternatif pour les navigateurs ne supportant pas Canvas.
			</canvas>
			<canvas id="canvas-menu-2" width="300" height="300"> 
			  Texte alternatif pour les navigateurs ne supportant pas Canvas.
			</canvas>

		</div>
		<!-- <div id="menu-border"></div> -->
	</div>

	<?php 
		$activeHome = '';
		$activeWork = '';
		$activeAbout = '';
		$activeNews = '';
		$activeContact = '';

		switch ($this->currentActiveMenuElt) {
			case 'home':
				$activeHome = 'current';
				break;
			case 'work-index':
				$activeWork = 'current';
				break;
			case 'work-view':
				$activeWork = 'current';
				break;
			case 'about':
				$activeAbout = 'current';
				break;
			case 'news-index':
				$activeNews = 'current';
				break;
			case 'news-view':
				$activeNews = 'current';
				break;
			case 'contact':
				$activeContact = 'current';
				break;
			default:
				# code...
				break;
		}
	?>

	<div id="menu" class="no-mobile">
		<div class="vertical_align">
			<div class="vertical_align_inner">

				<div id="list-menu-elts" class="list-with-background">

					<div class="list-backgrounds">
						<div class="background-mask">
							<img src="<?php echo $imagesUrl .'menu/home-bw.jpg' ?>" alt="" width="1144" height="607">
						</div>
						<div class="background-mask">
							<img src="<?php echo $imagesUrl .'menu/work-bw.jpg' ?>" alt="" width="1144" height="607">
						</div>
						<div class="background-mask">
							<img src="<?php echo $imagesUrl .'menu/about-bw.jpg' ?>" alt="" width="1144" height="607">
						</div>
						<div class="background-mask">
							<img src="<?php echo $imagesUrl .'menu/blog-bw.jpg' ?>" alt="" width="1144" height="607">
						</div>
						<div class="background-mask">
							<img src="<?php echo $imagesUrl .'menu/contact-bw.jpg' ?>" alt="" width="1144" height="607">
						</div>
					</div>

					<ul id="menu-elts">
					    <li class="<?php echo $activeHome ?>"><a href="<?php echo CController::createUrl('site/index') ?>" class="menu-link background-target">Home</a></li>

					    <li class="<?php echo $activeWork ?>"><a href="<?php echo CController::createUrl('Work/index') ?>" class="menu-link background-target">Our work</a></li>

					    <li class="<?php echo $activeAbout ?>"><a href="<?php echo CController::createUrl('site/about') ?>" class="menu-link background-target">About us</a></li>

					    <li class="<?php echo $activeNews ?>"><a href="<?php echo CController::createUrl('News/index') ?>" class="menu-link background-target">Blog</a></li>

					    <li class="<?php echo $activeContact ?>"><a href="<?php echo CController::createUrl('site/contact') ?>" class="menu-link background-target">Contact us</a></li>
					</ul>

				</div>
			</div>
		</div>
	</div>

	<!--
	| RESPONSIVE MOBILE
	-->
	<div id="menu-mobile-top" class="mobile <?php echo $mobileMenuBg ?>">
		<a href="<?php echo CController::createUrl('site/index') ?>"><div id="menu-mobile-logo" class="icon-small-logo"></div></a>

		<div id="menu-btn-open-mobile" class="menu-btn-open">
			<div class="tiret tiret-1"></div>
			<div class="tiret tiret-2"></div>
			<div class="tiret tiret-3"></div>
		</div>
	</div>

	<div id="menu-mobile-list" class="mobile">
		<div id="menu-mobile-list-content">
			<ul>
			    <li class="<?php echo $activeHome ?>"><a href="<?php echo CController::createUrl('site/index') ?>">Home</a></li>
			    <li class="<?php echo $activeWork ?>"><a href="<?php echo CController::createUrl('Work/index') ?>">Our work</a></li>
			    <li class="<?php echo $activeAbout ?>"><a href="<?php echo CController::createUrl('site/about') ?>">About us</a></li>
			    <li class="<?php echo $activeNews ?>"><a href="<?php echo CController::createUrl('News/index') ?>">Blog</a></li>
			    <li class="<?php echo $activeContact ?>"><a href="<?php echo CController::createUrl('site/contact') ?>">Contact</a></li>
			</ul>
		</div>
	</div>
    
    <?php echo $content; ?>

	<!-- Javascript -->
    <script type="text/javascript" src="<?php echo $baseUrl .'/dist/assets/js/script.js' ?>"></script>
    <!-- Prod
	<script type="text/javascript" src="<?php echo $baseUrl .'/dist/assets/js/script.min.js' ?>"></script>
	-->	

</body>
</html>
