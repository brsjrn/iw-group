<?php

/**
 * This is the model class for table "work".
 *
 * The followings are the available columns in table 'work':
 * @property integer $id
 * @property string $title
 * @property string $type
 * @property integer $isStared
 * @property string $staredSubtitle
 * @property string $staredDescription_text
 * @property integer $staredOrder
 * @property string $cover1_image
 * @property string $cover1Mobile_image
 * @property string $cover2_image
 * @property string $cover2Mobile_image
 * @property string $content1Title
 * @property string $content1_text
 * @property string $parallax_image
 * @property string $parallaxMobile_image
 * @property string $content2Title
 * @property string $content2_text
 * @property string $video_image
 * @property string $videoMobile_image
 * @property string $video_url
 * @property string $content3Title
 * @property string $content3_text
 * @property string $contentBig_image
 * @property string $content4Title
 * @property string $content4_text
 * @property string $bottom_image
 * @property string $bottomMobile_image
 * @property integer $ordre
 * @property integer $uniqId
 */
class Work extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'work';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, type, cover1_image, cover2_image, content1Title, content1_text, content2Title, content2_text, uniqId', 'required'),
			array('isStared, staredOrder, ordre, uniqId', 'numerical', 'integerOnly'=>true),
			array('title, content1Title, content2Title, content3Title, content4Title', 'length', 'max'=>63),
			array('type', 'length', 'max'=>127),
			array('staredSubtitle, cover1_image, cover1Mobile_image, cover2_image, cover2Mobile_image, parallax_image, parallaxMobile_image, video_image, videoMobile_image, video_url, contentBig_image, bottom_image, bottomMobile_image', 'length', 'max'=>255),
			array('staredDescription_text, content3_text, content4_text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, type, isStared, staredSubtitle, staredDescription_text, staredOrder, cover1_image, cover1Mobile_image, cover2_image, cover2Mobile_image, content1Title, content1_text, parallax_image, parallaxMobile_image, content2Title, content2_text, video_image, videoMobile_image, video_url, content3Title, content3_text, contentBig_image, content4Title, content4_text, bottom_image, bottomMobile_image, ordre, uniqId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'type' => 'Type',
			'isStared' => 'Is Stared',
			'staredSubtitle' => 'Stared Subtitle',
			'staredDescription_text' => 'Stared Description Text',
			'staredOrder' => 'Stared Order',
			'cover1_image' => 'Cover1 Image',
			'cover1Mobile_image' => 'Cover1 Mobile Image',
			'cover2_image' => 'Cover2 Image',
			'cover2Mobile_image' => 'Cover2 Mobile Image',
			'content1Title' => 'Content1 Title',
			'content1_text' => 'Content1 Text',
			'parallax_image' => 'Parallax Image',
			'parallaxMobile_image' => 'Parallax Mobile Image',
			'content2Title' => 'Content2 Title',
			'content2_text' => 'Content2 Text',
			'video_image' => 'Video Image',
			'videoMobile_image' => 'Video Mobile Image',
			'video_url' => 'Video Url',
			'content3Title' => 'Content3 Title',
			'content3_text' => 'Content3 Text',
			'contentBig_image' => 'Content Big Image',
			'content4Title' => 'Content4 Title',
			'content4_text' => 'Content4 Text',
			'bottom_image' => 'Bottom Image',
			'bottomMobile_image' => 'Bottom Mobile Image',
			'ordre' => 'Ordre',
			'uniqId' => 'Uniq',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('isStared',$this->isStared);
		$criteria->compare('staredSubtitle',$this->staredSubtitle,true);
		$criteria->compare('staredDescription_text',$this->staredDescription_text,true);
		$criteria->compare('staredOrder',$this->staredOrder);
		$criteria->compare('cover1_image',$this->cover1_image,true);
		$criteria->compare('cover1Mobile_image',$this->cover1Mobile_image,true);
		$criteria->compare('cover2_image',$this->cover2_image,true);
		$criteria->compare('cover2Mobile_image',$this->cover2Mobile_image,true);
		$criteria->compare('content1Title',$this->content1Title,true);
		$criteria->compare('content1_text',$this->content1_text,true);
		$criteria->compare('parallax_image',$this->parallax_image,true);
		$criteria->compare('parallaxMobile_image',$this->parallaxMobile_image,true);
		$criteria->compare('content2Title',$this->content2Title,true);
		$criteria->compare('content2_text',$this->content2_text,true);
		$criteria->compare('video_image',$this->video_image,true);
		$criteria->compare('videoMobile_image',$this->videoMobile_image,true);
		$criteria->compare('video_url',$this->video_url,true);
		$criteria->compare('content3Title',$this->content3Title,true);
		$criteria->compare('content3_text',$this->content3_text,true);
		$criteria->compare('contentBig_image',$this->contentBig_image,true);
		$criteria->compare('content4Title',$this->content4Title,true);
		$criteria->compare('content4_text',$this->content4_text,true);
		$criteria->compare('bottom_image',$this->bottom_image,true);
		$criteria->compare('bottomMobile_image',$this->bottomMobile_image,true);
		$criteria->compare('ordre',$this->ordre);
		$criteria->compare('uniqId',$this->uniqId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Work the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
