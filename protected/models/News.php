<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $id
 * @property string $title
 * @property string $date
 * @property string $shartDescription_text
 * @property integer $refCategory
 * @property integer $refAutor
 * @property string $dispatchCover_image
 * @property string $dispatchCoverMobile_mage
 * @property string $insideCover_image
 * @property string $insideCoverMobile_image
 * @property string $text_1
 * @property string $text_2
 * @property string $link_url
 * @property string $link_text
 * @property integer $ordre
 * @property integer $uniqId
 *
 * The followings are the available model relations:
 * @property Newscategory $refCategory0
 * @property Author $refAutor0
 */
class News extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, date, shartDescription_text, dispatchCover_image, insideCover_image, text_1, uniqId', 'required'),
			array('refCategory, refAutor, ordre, uniqId', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>127),
			array('dispatchCover_image, dispatchCoverMobile_mage, insideCover_image, insideCoverMobile_image, link_url', 'length', 'max'=>255),
			array('text_2, link_text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, title, date, shartDescription_text, refCategory, refAutor, dispatchCover_image, dispatchCoverMobile_mage, insideCover_image, insideCoverMobile_image, text_1, text_2, link_url, link_text, ordre, uniqId', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'refCategory0' => array(self::BELONGS_TO, 'Newscategory', 'refCategory'),
			'refAutor0' => array(self::BELONGS_TO, 'Author', 'refAutor'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'date' => 'Date',
			'shartDescription_text' => 'Shart Description Text',
			'refCategory' => 'Ref Category',
			'refAutor' => 'Ref Autor',
			'dispatchCover_image' => 'Dispatch Cover Image',
			'dispatchCoverMobile_mage' => 'Dispatch Cover Mobile Mage',
			'insideCover_image' => 'Inside Cover Image',
			'insideCoverMobile_image' => 'Inside Cover Mobile Image',
			'text_1' => 'Text 1',
			'text_2' => 'Text 2',
			'link_url' => 'Link Url',
			'link_text' => 'Link Text',
			'ordre' => 'Ordre',
			'uniqId' => 'Uniq',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('shartDescription_text',$this->shartDescription_text,true);
		$criteria->compare('refCategory',$this->refCategory);
		$criteria->compare('refAutor',$this->refAutor);
		$criteria->compare('dispatchCover_image',$this->dispatchCover_image,true);
		$criteria->compare('dispatchCoverMobile_mage',$this->dispatchCoverMobile_mage,true);
		$criteria->compare('insideCover_image',$this->insideCover_image,true);
		$criteria->compare('insideCoverMobile_image',$this->insideCoverMobile_image,true);
		$criteria->compare('text_1',$this->text_1,true);
		$criteria->compare('text_2',$this->text_2,true);
		$criteria->compare('link_url',$this->link_url,true);
		$criteria->compare('link_text',$this->link_text,true);
		$criteria->compare('ordre',$this->ordre);
		$criteria->compare('uniqId',$this->uniqId);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
