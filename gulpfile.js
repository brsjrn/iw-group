// Requis
var gulp = require('gulp'),
	plugins = require('gulp-load-plugins')()
	// Plugins
	sass = require('gulp-sass'),
	autoprefixer = require('gulp-autoprefixer'),
	cleanCSS = require('gulp-clean-css'),
	rename = require('gulp-rename'),
	imagemin = require('gulp-imagemin'),
	concat = require('gulp-concat'),
	uglify = require('gulp-uglify'),
	spritesmith = require('gulp.spritesmith'),
	buffer = require('vinyl-buffer'),
	merge = require('merge-stream'),
	// Display messages (errors mainly)
	gutil = require('gulp-util'),
	// Manage errors to not kill pipe stream
	plumber = require('gulp-plumber'),
	// Notifications
	notify = require('gulp-notify');

// Variables de chemins
var source = './src'; // dossier de travail
var destination = './dist'; // dossier à livrer

// SASS
//Autoprefxer
gulp.task('sass', function() {
	return gulp.src(source + '/assets/scss/style.scss')
		.pipe(
			plumber({errorHandler: notify.onError({
				message: "<%= error.message %>",
		        title: "Sass Error"

			})})
		)
		.pipe(sass())
		.pipe(autoprefixer())
		.pipe(plumber.stop())
		.pipe(gulp.dest(destination + '/assets/css/'))
});

gulp.task('concat-css', function() {
	return gulp.src([destination + '/assets/css/style.css', destination + '/assets/css/sprite.css'])
		.pipe(concat('style.concat.css'))
		.pipe(
			plumber({errorHandler: notify.onError({
				message: "<%= error.message %>",
		        title: "Concat css Error"

			})})
		)
		//.pipe(autoprefixer())
		.pipe(plumber.stop())
		.pipe(gulp.dest(destination + '/assets/css/'))
});


// Javascript
gulp.task('javascript', function() {
  return gulp.src([source + '/assets/js/lib/*.js', source + '/assets/js/modules/*.js', source + '/assets/js/vendors/*.js', source + '/assets/js/*.js'])
    .pipe(concat('script.js'))
    //.pipe(uglify())
    .pipe(gulp.dest(destination + '/assets/js/'));
});

// Minify CSS
gulp.task('minify-css', function() {
	return gulp.src(destination + '/assets/css/style.css')
		.pipe(cleanCSS())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(destination + '/assets/css/'));
});

// Minify JS
gulp.task('minify-js', function() {
	return gulp.src(destination + '/assets/js/script.js')
		.pipe(uglify())
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(destination + '/assets/js/'));
});


// Images
gulp.task('media', function() {
	return gulp.src([source + '/assets/images/*', source + '/assets/images/**/*'])
		.pipe(imagemin())
		.pipe(gulp.dest(destination + '/assets/images/'));
});

// Sprites
gulp.task('sprite', function () {
  // Generate our spritesheet
  var spriteData = gulp.src(source + '/assets/images/sprites/*.png').pipe(spritesmith({
    retinaSrcFilter: source + '/assets/images/sprites/*@2x.png',
    imgName: 'sprite.png',
    retinaImgName: 'sprite@2x.png',
    cssName: 'sprite.css',
    imgPath:'../../../dist/assets/images/sprite.png',
    retinaImgPath:'../../../dist/assets/images/sprite@2x.png'
    //engine: phantomjssmith
  }));

  // Pipe image stream through image optimizer and onto disk
  var imgStream = spriteData.img
    // DEV: We must buffer our stream into a Buffer for `imagemin`
    .pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest(destination + '/assets/images/'));

  // Pipe CSS stream through CSS optimizer and onto disk
  var cssStream = spriteData.css
    .pipe(gulp.dest(destination + '/assets/css/'));

  // Return a merged stream to handle both `end` events
  return merge(imgStream, cssStream);
});

// Css
gulp.task('css', ['sass', 'concat-css']);

// Build
gulp.task('build', ['css', 'javascript']);

// Prod
gulp.task('prod', ['media', 'css', 'javascript', 'minify-css', 'minify-js']);

// Watch
gulp.task('watch', function () {
	gulp.watch(source + '/assets/scss/**/*.scss', ['css']);
	gulp.watch(source + '/assets/js/**/*.js', ['javascript']);
});

// default
gulp.task('default', ['media', 'build', 'watch']);