-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 24 Décembre 2016 à 16:20
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `iwgroup`
--

-- --------------------------------------------------------

--
-- Structure de la table `author`
--

CREATE TABLE IF NOT EXISTS `author` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) NOT NULL,
  `job` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `author`
--

INSERT INTO `author` (`id`, `name`, `job`) VALUES
(1, 'Ben Kochavy', 'Maker innovator @IWGroup');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(63) NOT NULL,
  `adress1` varchar(255) NOT NULL,
  `adress2` varchar(255) DEFAULT NULL,
  `phone` varchar(31) DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`id`, `city`, `adress1`, `adress2`, `phone`, `ordre`) VALUES
(1, 'Los Angeles', '8687 Melrose Ave. Suite G540', 'West Hollywood, CA 90069', '310.289.5500', 1),
(2, 'New York', '215 Park Avenue S, 6Th Floor', 'New York, NY 10003', '646.979.8959', 2),
(3, 'San Francisco', '33 New Montgomery St Suite 990', 'San Francisco, CA 94105', '646.979.8959', 3);

-- --------------------------------------------------------

--
-- Structure de la table `logo`
--

CREATE TABLE IF NOT EXISTS `logo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `image_mobile` varchar(255) DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `logo`
--

INSERT INTO `logo` (`id`, `image`, `image_mobile`, `ordre`) VALUES
(2, 'Logo_image58593e9868afa.png', '', 1),
(3, 'Logo_image58593e9d0d32a.png', '', 2),
(4, 'Logo_image58593ea0e7c21.png', '', 3),
(5, 'Logo_image58593eb7136bb.png', '', 4);

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(127) NOT NULL,
  `date` date NOT NULL,
  `shartDescription_text` text NOT NULL,
  `refCategory` int(11) DEFAULT NULL,
  `refAutor` int(11) DEFAULT NULL,
  `dispatchCover_image` varchar(255) NOT NULL,
  `dispatchCoverMobile_mage` varchar(255) DEFAULT NULL,
  `insideCover_image` varchar(255) NOT NULL,
  `insideCoverMobile_image` varchar(255) DEFAULT NULL,
  `text_1` text NOT NULL,
  `text_2` text,
  `link_url` varchar(255) DEFAULT NULL,
  `link_text` text,
  `ordre` int(11) DEFAULT NULL,
  `uniqId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqId` (`uniqId`),
  KEY `refCategory` (`refCategory`,`refAutor`),
  KEY `refAutor` (`refAutor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `news`
--

INSERT INTO `news` (`id`, `title`, `date`, `shartDescription_text`, `refCategory`, `refAutor`, `dispatchCover_image`, `dispatchCoverMobile_mage`, `insideCover_image`, `insideCoverMobile_image`, `text_1`, `text_2`, `link_url`, `link_text`, `ordre`, `uniqId`) VALUES
(2, 'News 1', '2016-12-08', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore et dolore magna aliqua.</p>\r\n', 1, 1, 'News_dispatchCover_image5858571c2b9c8.jpg', '', 'News_insideCover_image5858571c2bdb0.jpg', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'http://www.google.com', '<p>Want to see some examples of excellent brand Content?</p>\r\n', 1, 14985),
(3, 'News 2', '2016-12-09', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>\r\n', 2, 1, 'News_dispatchCover_image58585aac4be13.jpg', '', 'News_insideCover_image58585aac4c1fb.jpg', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', 'http://www.google.com', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua.</p>\r\n', 2, 213135),
(4, 'News 3', '2016-12-10', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua.</p>\r\n', 3, 1, 'News_dispatchCover_image58585cfc532b9.jpg', '', 'News_insideCover_image58585cfc536a1.jpg', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo&nbsp;consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse&nbsp;cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non&nbsp;proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo&nbsp;consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse&nbsp;cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'http://www.google.com', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing. Lorem ipsum dolor sit amet.</p>\r\n', 3, 419648),
(5, 'News 4', '2016-12-11', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua.</p>\r\n', 2, 1, 'News_dispatchCover_image58585d1de5643.jpg', '', 'News_insideCover_image58585d1de5a2b.jpg', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', 'http://www.google.com', '<p>Cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non.</p>\r\n', 4, 410553),
(6, 'News 5', '2016-12-12', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore.</p>\r\n', 1, 1, 'News_dispatchCover_image58585d49d1fc3.jpg', '', 'News_insideCover_image58585d49d23ab.jpg', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', 'http://www.google.com', '<p>Lorem ipsum dolor sit amet.</p>\r\n', 5, 924408),
(7, 'News 6', '2016-12-14', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore.</p>\r\n', 1, 1, 'News_dispatchCover_image585864f27a9e2.jpg', '', 'News_insideCover_image585864f27adca.jpg', '', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo&nbsp;consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse&nbsp;cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non&nbsp;proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo&nbsp;consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse&nbsp;cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non&nbsp;proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,&nbsp;quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo&nbsp;consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse&nbsp;cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non&nbsp;proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'http://www.google.com', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod&nbsp;tempor incididunt ut labore et dolore magna aliqua.</p>\r\n', 6, 36835);

-- --------------------------------------------------------

--
-- Structure de la table `newscategory`
--

CREATE TABLE IF NOT EXISTS `newscategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) NOT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `newscategory`
--

INSERT INTO `newscategory` (`id`, `title`, `ordre`) VALUES
(1, 'Insight', 1),
(2, 'News', 2),
(3, 'Event', 3);

-- --------------------------------------------------------

--
-- Structure de la table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `image_mobile` varchar(255) DEFAULT NULL,
  `name` varchar(127) NOT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `person`
--

INSERT INTO `person` (`id`, `image`, `image_mobile`, `name`, `ordre`) VALUES
(1, 'Person_image585941d3382c4.jpg', '', 'Person 1', 1),
(2, 'Person_image585941fe5b330.jpg', '', 'Person 2', 2),
(3, 'Person_image585942324864a.jpg', NULL, 'Person 3', 3),
(4, 'Person_image5859423c03ecd.jpg', NULL, 'Person 4', 4),
(5, 'Person_image585942b5904b7.jpg', NULL, 'Person 5', 5),
(6, 'Person_image585942c15bb99.jpg', NULL, 'Person 6', 6),
(7, 'Person_image585942cb0dbc3.jpg', NULL, 'Person 7', 7),
(8, 'Person_image5859bbe1ab7c1.jpg', NULL, 'Person 8', 8),
(9, 'Person_image5859bbe8c9db8.jpg', NULL, 'Person 9', 9);

-- --------------------------------------------------------

--
-- Structure de la table `service`
--

CREATE TABLE IF NOT EXISTS `service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) NOT NULL,
  `bakground_image` varchar(255) DEFAULT NULL,
  `ordre` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Contenu de la table `service`
--

INSERT INTO `service` (`id`, `title`, `bakground_image`, `ordre`) VALUES
(1, 'Branding & Identity', 'Service_bakground_image58594414e5c47.jpg', 1),
(2, 'Digital Strategy', 'Service_bakground_image5859443f91e3f.jpg', 2),
(3, 'Creative Direction', 'Service_bakground_image5859444fedac0.jpg', 3),
(4, 'Art Direction & Design', 'Service_bakground_image5859446014db1.jpg', 4),
(5, 'Web Development', 'Service_bakground_image585944692b719.jpg', 5),
(6, 'Backend Integration', 'Service_bakground_image5859447c5c8a4.jpg', 6),
(7, 'Ecommerce', 'Service_bakground_image585944863d2f0.jpg', 7);

-- --------------------------------------------------------

--
-- Structure de la table `slideabout`
--

CREATE TABLE IF NOT EXISTS `slideabout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `image_mobile` varchar(255) DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `slideabout`
--

INSERT INTO `slideabout` (`id`, `image`, `image_mobile`, `ordre`) VALUES
(2, 'Slideabout_image585949ea418fd.jpg', NULL, 1),
(3, 'Slideabout_image585949ee61d89.jpg', NULL, 2),
(4, 'Slideabout_image585949f253bdb.jpg', NULL, 3);

-- --------------------------------------------------------

--
-- Structure de la table `sliderimage`
--

CREATE TABLE IF NOT EXISTS `sliderimage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `image_mobile` varchar(255) DEFAULT NULL,
  `ref_uniqId` int(11) NOT NULL,
  `order_slide` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ref_uniqId` (`ref_uniqId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Contenu de la table `sliderimage`
--

INSERT INTO `sliderimage` (`id`, `image`, `image_mobile`, `ref_uniqId`, `order_slide`) VALUES
(13, 'Sliderimage_image 5857fca731964.jpg', '', 791993, 1),
(14, 'Sliderimage_image5858fe4dd6981.jpg', '', 419648, 1),
(15, 'Sliderimage_image5858fe533a2cc.jpg', '', 419648, 2),
(16, 'Sliderimage_image585900d865da2.jpg', '', 419648, 3),
(17, 'Sliderimage_image58591a33e4637.jpg', 'Sliderimage_image_mobile58591a33e4e08.jpg', 884675, NULL),
(18, 'Sliderimage_image58591a45cb7df.jpg', '', 884675, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `work`
--

CREATE TABLE IF NOT EXISTS `work` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(63) NOT NULL,
  `type` varchar(127) NOT NULL,
  `isStared` tinyint(1) DEFAULT NULL,
  `staredSubtitle` varchar(255) DEFAULT NULL,
  `staredDescription_text` text,
  `staredOrder` int(11) DEFAULT NULL,
  `cover1_image` varchar(255) NOT NULL,
  `cover1Mobile_image` varchar(255) DEFAULT NULL,
  `cover2_image` varchar(255) NOT NULL,
  `cover2Mobile_image` varchar(255) DEFAULT NULL,
  `content1Title` varchar(63) NOT NULL,
  `content1_text` text NOT NULL,
  `parallax_image` varchar(255) DEFAULT NULL,
  `parallaxMobile_image` varchar(255) DEFAULT NULL,
  `content2Title` varchar(63) NOT NULL,
  `content2_text` text NOT NULL,
  `video_image` varchar(255) DEFAULT NULL,
  `videoMobile_image` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) DEFAULT NULL,
  `content3Title` varchar(63) DEFAULT NULL,
  `content3_text` text,
  `contentBig_image` varchar(255) DEFAULT NULL,
  `content4Title` varchar(63) DEFAULT NULL,
  `content4_text` text,
  `bottom_image` varchar(255) DEFAULT NULL,
  `bottomMobile_image` varchar(255) DEFAULT NULL,
  `ordre` int(11) DEFAULT NULL,
  `uniqId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqId` (`uniqId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `work`
--

INSERT INTO `work` (`id`, `title`, `type`, `isStared`, `staredSubtitle`, `staredDescription_text`, `staredOrder`, `cover1_image`, `cover1Mobile_image`, `cover2_image`, `cover2Mobile_image`, `content1Title`, `content1_text`, `parallax_image`, `parallaxMobile_image`, `content2Title`, `content2_text`, `video_image`, `videoMobile_image`, `video_url`, `content3Title`, `content3_text`, `contentBig_image`, `content4Title`, `content4_text`, `bottom_image`, `bottomMobile_image`, `ordre`, `uniqId`) VALUES
(2, 'Work 1', 'Mobile Application', 0, '', '', NULL, 'Work_cover1_image58590ba0d1bc2.jpg', '', 'Work_cover2_image58590ba0d1faa.jpg', 'Work_cover2Mobile_image585910ebdc5b2.jpg', 'Starting Point', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Work_parallax_image58590ba0d2392.jpg', '', 'Taking the brand further', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br />\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia.</p>\r\n', 'Work_video_image585969d352f49.jpg', '', '196277011', 'Web Design.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Work_contentBig_image58590ba0d2392.jpg', 'The Outcome.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Work_bottom_image58590ba0d2392.jpg', '', 1, 884675),
(3, 'Work 2', 'Brand Strategy. Design. Photography Direction.', 1, '<p>work 2</p>\r\n\r\n<p>subtitle</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n', 1, 'Work_cover1_image58590c997a248.jpg', '', 'Work_cover2_image58590c997a630.jpg', 'Work_cover2Mobile_image585910112270a.jpg', 'Starting Point.', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Work_parallax_image58590c997a630.jpg', '', 'Taking the brand further', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', '', '', '', '', '', '', '', 'Work_bottom_image58590c997aa18.jpg', '', 2, 238495),
(4, 'Work 3', 'Ecommerce', 1, '<p>Work 3</p>\r\n\r\n<p>subtitle</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n', 3, 'Work_cover1_image58594c005c102.jpg', '', 'Work_cover2_image58594c005c4ea.jpg', '', 'Point 1', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Work_parallax_image58594c005c4ea.jpg', '', 'Point 2', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', '', '', '', '', '', '', '', '', 'Work_bottom_image58594c005c8d2.jpg', '', 3, 577607),
(5, 'Work 4', 'Branding & Identity', 1, '<p>Work 4 sub</p>\r\n', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>\r\n', 2, 'Work_cover1_image58594cc26e276.jpg', NULL, 'Work_cover2_image58594cc26e65e.jpg', 'Work_cover2Mobile_image58594cc26e65e.jpg', 'Point 1', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Work_parallax_image58594cc26e65e.jpg', NULL, 'Point 2', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'Work_video_image5859612be40b7.jpg', NULL, '195592912', '', '', '', '', '', 'Work_bottom_image58594cc26ea46.jpg', NULL, 4, 371247);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`refCategory`) REFERENCES `newscategory` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `news_ibfk_2` FOREIGN KEY (`refAutor`) REFERENCES `author` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
