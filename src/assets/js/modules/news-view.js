(function ($) {
  $.newsView = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.page = $('.page');
      _this.header = $('#news-view #header');
      _this.headerAnimeTexts = _this.header.find('.anime-header-text');
      _this.content = $('#news-view #news-content');
      _this.slider = $('#news-view #slider');
      _this.popup = $('.popup-newsletter');
      _this.popupCloseBtn = $('.popup-newsletter .popup-close-btn')

      // Variables

      // Load image & init page
      _this.loadAndInit();

      console.log("[ Module ] Init News View");

      _this.build();
    };

    _this.build = function() {
      // Close popup newsletter
      _this.popupCloseBtn.on('click', function() {
        _this.popup.removeClass('active');
      });

      // Subscribe
      $('.btn-subscribe').on('click', function() {
        $('.popup-content').fadeOut(function() {
          $('.popup-response').fadeIn(function() {
            setTimeout(function() {
              _this.popup.removeClass('active');
            }, 1500);
          });
        });
      });
    }


    /* ---------------------------------
    |               Header              |
    ---------------------------------- */
    _this.initHeader = function() {

      // Prepare text to appear
      _this.headerAnimeTexts.each(function() {
        $(this).height($(this).find('.anime-wrap-text').height());
      })

      // Display anime texts
      animeEltsDelay(_this.headerAnimeTexts, 300, true, 'active', null);

      // Show background
      setTimeout(function() {
        _this.header.find('#background').addClass('active');
      }, 200);
    }

    /* ---------------------------------
    |              Content              |
    ---------------------------------- */
    _this.initContent = function() {
      _this.content.addClass('active');
    }

    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    _this.loadAndInit = function() {
      // Load images & init page
      _this.page.imagesLoaded()
        .done( function( instance ) {
            console.log('all images successfully loaded');
            _this.initPage();
          })
          .fail( function() {
            console.log('all images loaded, at least one is broken');
            _this.initPage();
          });
    }
    
    _this.initPage = function() {

      // Init header
      _this.initHeader();

      // Init content
      setTimeout(function() {
        _this.initContent();
      }, 400);

      // Init slider
      new $.slider(_this.slider);

      // Init popup newsletter
      if(_this.popup.length > 0) {
        setTimeout(function() {
          _this.popup.addClass('active');
        }, 600);
      }
    }


    _this.init();
  };
})(jQuery);