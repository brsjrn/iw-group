(function ($) {
  $.socials = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {

      _this.idCanvas = options[0];
      _this.color = options[1];

      // Variables
      _this.canvas = document.getElementById(_this.idCanvas);
      _this.context = _this.canvas.getContext('2d');
      _this.context.lineWidth = 2;
      _this.context.strokeStyle =  _this.color;
      _this.x = _this.canvas.width / 2;
      _this.y = _this.canvas.height / 2;
      _this.radius = _this.canvas.width / 2 - 2;
      _this.endPercent = 100;
      _this.curPerc = 0;
      _this.counterClockwise = false;
      _this.circ = Math.PI * 2;
      _this.quart = Math.PI / 2;
      _this.speedInit = 2;
      _this.speed = _this.speedInit;
      _this.acceleration = 1.1,
      _this.sens = true;


      console.log("[ Module ] Init social");

      _this.build();
    };

    _this.build = function() {

    }

    _this.animateSocialCanvas = function(current) {
      _this.context.clearRect(0, 0, _this.canvas.width, _this.canvas.height);
      _this.context.beginPath();
      //_this.context.arc(_this.x, _this.y, _this.radius, -(_this.quart), ((_this.circ) * _this.current) - _this.quart, false);
      _this.context.arc(_this.x, _this.y, _this.radius, -(_this.quart), ((_this.circ) * current) - _this.quart, false);
      _this.context.stroke();

      var stop = false;
      
      if(_this.sens) {
        
        if(_this.curPerc != 100 ) {
          _this.speed = _this.speed * _this.acceleration;
          _this.curPerc = _this.curPerc + _this.speed;

          if (_this.curPerc >= _this. endPercent) {
            _this.curPerc = 100;
          }
        } else {
          stop = true;
        }
      } else {

        if(_this.curPerc != 0) {
          _this.speed = _this.speed / _this.acceleration;
          _this.curPerc = _this.curPerc - _this.speed;

          if (_this.curPerc < 0) {
            _this.curPerc = 0;
          }

        } else {
          _this.speed = _this.speedInit;
          stop = true;
        }
      }

      if(!stop) {
        requestAnimationFrame(function () {
            _this.animateSocialCanvas(_this.curPerc / 100)
        });
      }
    }


    _this.init();
  };
})(jQuery);