(function ($) {
  $.slider = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.slider = $(element);
      _this.slides = _this.slider.find('.slides li');
      _this.arrownLeft = _this.slider.find('.arrow-left');
      _this.arrownRight = _this.slider.find('.arrow-right');
      _this.bullets = _this.slider.find('.bullets li');

      // Var local
      _this.cptPrev = 0;
      _this.cpt = 0;
      _this.isRunning = false;
      _this.total = _this.slides.length;

      // Slide params
      _this.slideSpeed = 1000;
      _this.easing = "easeInOutQuart";

      // Ini elts
      _this.initElts();

      console.log("[ Module ] Init slider");

      _this.build();
    };

    _this.build = function() {

      // Click left arrow
      _this.arrownLeft.on('click', function() {
        if(!_this.isRunning) {
          _this.isRunning = true;
          _this.previous();
        }
      });

      // Click right arrow
      _this.arrownRight.on('click', function() {
        if(!_this.isRunning) {
          _this.isRunning = true;
          _this.next();
        }
      });

      /* MOBILE RESPONSIVE */
      // Swipe elts
      _this.slider.on('swipeleft', function() {
        if(!_this.isRunning) {
          _this.isRunning = true;
          _this.previous();
        }
      });

      _this.slider.on('swiperight', function() {
        if(!_this.isRunning) {
          _this.isRunning = true;
          _this.next();
        }
      });
    }


    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    _this.initElts = function() {
      _this.initBullets();
    }

    /* ---------------------------------
    |               Bullets             |
    ---------------------------------- */
    _this.initBullets = function() {
      _this.bullets.eq(_this.cpt).addClass('active');
    }

    _this.animeBullet = function() {
      _this.bullets.eq(_this.cptPrev).removeClass('active');
      _this.bullets.eq(_this.cpt).addClass('active');
    }

    /* ---------------------------------
    |               Actions             |
    ---------------------------------- */
    _this.next = function() {
      _this.cptPrev = _this.cpt;

      if(_this.cpt < _this.total - 1) {
        _this.cpt++;
      } else {
        _this.cpt = 0;
      }

      _this.animeBullet();
      _this.animeSlider('right');
    }

    _this.previous = function() {
      _this.cptPrev = _this.cpt;

      if(_this.cpt > 0) {
        _this.cpt--;
      } else {
        _this.cpt = _this.total - 1;
      }

      _this.animeBullet();
      _this.animeSlider('left');
    }

    _this.animeSlider = function(direction) {
      var oldSlide = _this.slides.eq(_this.cptPrev);
      var newSlide = _this.slides.eq(_this.cpt);
      var oldEndPoint;

      oldSlide.css('z-index', 3);
      newSlide.css('z-index', 4);

      if(direction == 'left') {
        newSlide.css('left', '100%');
        oldEndPoint = "-50%";
      } else {
        newSlide.css('left', '-100%');
        oldEndPoint = "50%";
      }

      newSlide.animate({
        left: '0%'
      }, {
        duration: _this.slideSpeed,
        specialEasing: {
            left: _this.easing
        },
        complete: function() {
          newSlide.css('z-index', 3);
        }
      });

      oldSlide.animate({
        left: oldEndPoint
      }, {
        duration: _this.slideSpeed,
        specialEasing: {
            left: _this.easing
        },
        complete: function() {
          oldSlide.css('z-index', 2);
          oldSlide.css('left', 0);
          _this.isRunning = false;
        }
      });
    }

    _this.init();
  };
})(jQuery);