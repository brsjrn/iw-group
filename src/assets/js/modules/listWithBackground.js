(function ($) {
  $.listWithBackground = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.list = $(element);
      _this.listElts = _this.list.find('.background-target');
      _this.listBackgrounds = _this.list.find('.list-backgrounds');
      _this.lastOutIndex;
      _this.lastOutTime;

      // Variables
      _this.isListRunning = false;

      console.log("[ Module ] Init list with background");

      _this.build();
    };

    _this.build = function() {

      // Mouse hover elt
      _this.listElts.hover(function() {
        

        if(!_this.isListRunning) {
          _this.isListRunning = true;

          var index = _this.listElts.index($(this));
          var diffTime = Math.floor((new Date()).getTime()) - _this.lastOutTime;

          setTimeout(function() {
            if(index != _this.lastOutIndex || (diffTime > 100 &&  index == _this.lastOutIndex)) {
              _this.listBackgrounds.find('.background-mask').removeClass('active');
              _this.listBackgrounds.find('.background-mask').eq(index).addClass('active');
              _this.listBackgrounds.addClass('active');
            }
          }, 200);

        }
      }, function() {
        var index = _this.listElts.index($(this));

        _this.lastOutTime = Math.floor((new Date()).getTime());

        _this.listBackgrounds.removeClass('active');
        _this.isListRunning = false;
        _this.lastOutIndex = index;
      });
    }

    /* ---------------------------------
                   List             |
    ---------------------------------- */
    _this.resizeList = function() {
      _this.listBackgrounds.find('.background-mask').height(_this.list.height() + 60);
    }

    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    _this.resizePage = function() {
      console.log('resize list');
      _this.resizeList();
    }


    _this.init();
  };
})(jQuery);