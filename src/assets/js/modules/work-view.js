(function ($) {
  $.workView = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.page = $('.page');
      _this.header = $('#work-view #header');
      _this.headerAnimeTexts = $('#work-view #header').find('.anime-header-text');
      _this.cover = $('#work-view #cover');
      _this.goDownbtn = $('#work-view #go-down');
      _this.mainTitle = $('#work-view #header h1');
      _this.workType = $('#work-view #header .work-type');
      _this.slider = $('#work-view .slider');
      _this.video =  $('#work-view #video');
      _this.videobackground = $('#work-view #video .video-cover');
      _this.videoPlayBtn = $('#work-view #video .video-play-btn');


      // Variables

      // Load image & init page
      _this.loadAndInit();

      console.log("[ Module ] Init Work View");

      _this.build();
    };

    _this.build = function() {

      // Scroll down on "go down" btn click
      _this.goDownbtn.on('click', function() {
        scrollTo('#text-1', 100, 800);
      });


      // Play video
      if( _this.video.length > 0) {
        var iframe = document.querySelector('.video-iframe');
        var player = new Vimeo.Player(iframe);

        _this.videoPlayBtn.on('click', function() {
          $(this).fadeOut();
          _this.videobackground.fadeOut();
          player.play();
        });

        player.on('pause', function() {
            _this.videoPlayBtn.fadeIn();
            _this.videobackground.fadeIn();
        });
      }
    }

    /* ---------------------------------
    |               Header              |
    ---------------------------------- */
    _this.initHeader = function() {
      _this.cover.addClass('active');

      /*

      setTimeout(function() {
        _this.mainTitle.addClass('active');
      }, 200);

      setTimeout(function() {
        _this.workType.addClass('active');
      }, 300);

    */

      // Prepare anime texts size
      $('.anime-header-text').each(function() {
        $(this).height($(this).find('.anime-wrap-text').height());
      })

      // Display anime texts
      animeEltsDelay(_this.headerAnimeTexts, 150, true, 'active', null);

      setTimeout(function() {
        _this.goDownbtn.addClass('active');
      }, 400);
    }

    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    _this.loadAndInit = function() {
      // Load images & init page
      _this.page.imagesLoaded()
        .done( function( instance ) {
            console.log('all images successfully loaded');
            _this.initPage();
          })
          .fail( function() {
            console.log('all images loaded, at least one is broken');
            _this.initPage();
          });
    }

    _this.initPage = function() {

      // Init header
      setTimeout(function() {
        _this.initHeader();
      }, 500);

      // Init slider
      new $.slider(_this.slider);
    }

    _this.resizePage = function() {
      _this.header.height(win.height());
    }

    _this.init();
  };
})(jQuery);