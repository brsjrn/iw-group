(function ($) {
  $.menu = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.menu = $('#menu');
      _this.menuBackground = $('#menu-background');
      _this.menuContent = $('#menu-content');
      _this.menuOpenBtn = $('#menu-btn-open');
      _this.menuElts = $('ul#menu-elts li');
      _this.menuLinkgs = $('.menu-link');
      _this.listWithBackground;
      _this.list = $('#menu #list-menu-elts');

      // Mobile responsive
      _this.mobileMenuOpenBtn = $('#menu-btn-open-mobile');
      _this.mobileMenuList = $('#menu-mobile-list');

      // Variables
      // -- Size area
      _this.isMenuOpen = false;
      _this.isMenuMoving;
      _this.isMobileMenuMoving = false;

      _this.initPage();

      console.log("[ Module ] Init Menu");

      _this.build();
    };

    _this.build = function() {

      // active current active menu elt
      _this.activeCurrentElt();

      // Open / Close
      _this.menuOpenBtn.on('click', function() {
        
          if(!_this.isMenuMoving) {

              if(!_this.isMenuOpen) {
                  console.log("[ Action ] Open menu");

                  // Show menu
                  _this.menu.show();

                  // Resize
                  _this.listWithBackground.resizeList();

                  $(this).addClass('active');

                  // Close => Open
                  _this.isMenuMoving = true;

                  _this.menuBackground.animate({
                      height: '100%'
                  }, {
                    duration: 1000,
                    specialEasing: {
                        height: "easeInOutQuint"
                    }
                  });

                  _this.menuContent.animate({
                      height: '100%'
                  }, {
                    duration: 1200,
                    specialEasing: {
                        height: "easeInOutQuint"
                    },
                    complete: function() {
                        _this.isMenuMoving = false;
                        _this.isMenuOpen = true;
                    }
                  });

                  setTimeout(function(){ 
                      _this.displayMenuElt();
                  }, 600);
                  
                  
                  // Draw canvas
                  canvas.initCanvas();
              } else {
                console.log("[ Action ] Close menu");

                // Show menu
                _this.menu.hide();

                // Resize
                _this.listWithBackground.resizeList();

                  $(this).removeClass('active');

                  // Open => Close
                  _this.isMenuMoving = true;

                  setTimeout(function(){ 
                      _this.menuBackground.animate({
                          height: '0'
                      }, {
                          duration: 1200,
                          specialEasing: {
                              height: "easeInOutQuint"
                          }
                      });

                      _this.menuContent.animate({
                          height: '0'
                      }, {
                        duration: 1000,
                        specialEasing: {
                            height: "easeInOutQuint"
                        },
                        complete: function() {
                            _this.isMenuMoving = false;
                            _this.isMenuOpen = false;
                        }
                      });
                  }, 20);

                  _this.hideMenuElt();
              }
          }
      });

      // Elt menu click
      _this.menuLinkgs.on('click', function(e) {
          e.preventDefault();
          var url = $(this).attr('href');
          _this.list.find('.list-backgrounds').addClass('clicked');

          // Anim arc to line

          canvas.initArcsToLine();
          $('#canvas-menu-2').fadeOut();
          _this.menuOpenBtn.removeClass('active');


          // Load page in 0.5s
          setTimeout(function(){ 
              window.location.href = url;
          }, 500);

          // Hide menu
          _this.hideMenuElt();
      });

      _this.menuLinkgs.hover(function() {
        canvas.followMenuDrawArcs($(this));
      }, function() {
        canvas.backMenuDrawArcs($(this));
      });

      /* ------- */
      /*  MOBILE */
      /* ------- */
      _this.mobileMenuOpenBtn.on('click', function() {
        if(!_this.isMobileMenuMoving) {
          _this.isMobileMenuMoving = true;

          if(_this.mobileMenuList.hasClass('active')) {
            // Close mobile menu
            _this.mobileMenuList.removeClass('active');
            _this.mobileMenuOpenBtn.removeClass('active');
          } else {
            // Open mobile menu
            _this.mobileMenuList.addClass('active');
            _this.mobileMenuOpenBtn.addClass('active');
          }
        }


        setTimeout(function() {
          _this.isMobileMenuMoving = false;
        }, 800);
      });

    };

    _this.displayMenuElt = function() {
      
      //Display menu
      _this.menu.css('z-index', 55);

      animeEltsDelay(_this.menuElts, 100, true, 'active', null);

    }

    _this.hideMenuElt = function() {

      animeEltsDelay(_this.menuElts, 50, true, 'inactive-top', null);

      setTimeout(function(){ 
        //Hide menu
        _this.menuElts.removeClass('active inactive-top');
        _this.menu.css('z-index', 0);
      }, 500);

    }

    _this.activeCurrentElt = function() {
      var currentPageId = $('.page').attr('id');
    }

    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    _this.initPage = function() {
      _this.listWithBackground = new $.listWithBackground(_this.list);
    }

    _this.resizePage = function() {
      console.log('resize menu');
      _this.listWithBackground.resizeList();
    }

    _this.init();
  };
})(jQuery);