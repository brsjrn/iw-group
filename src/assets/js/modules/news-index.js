(function ($) {
  $.newsIndex = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.page = $('.page');
      _this.nav = $('#news-index #news-nav');
      _this.scrollDownBtn = $('#news-index #news-scroll-down');
      _this.categories = _this.nav.find('#categories li');
      _this.news = $('#news-index #list-news .news');
      _this.search = $('#news-index #search');
      _this.btnSearchOn = $('#news-index #btn-search');
      _this.btnSearchOff = $('#news-index #btn-close-search');

      // Variables
      
      // Load image & init page
      _this.loadAndInit();

      console.log("[ Module ] Init News Index");

      _this.build();
    };

    _this.build = function() {

      // Btn scroll down
      _this.scrollDownBtn.on('click', function() {
        scrollTo('footer', 300, 1000);
      });

      // Open search
      _this.btnSearchOn.on('click', function() {
        // Display search page
        _this.search.addClass('active');

        // Display btn close
        _this.btnSearchOff.addClass('active');

        // Focus input
        $('#search-input').focus();
        

        // Change overflows
        setTimeout(function() {
          _this.search.css('overflow-y', 'scroll');
          _this.page.css('overflow', 'hidden');
        }, 1000);
      });

      _this.btnSearchOff.on('click', function() {
        // Hide search page
        _this.search.removeClass('active');

        // Hide btn close
        $(this).removeClass('active');

        // Change overflows
        _this.search.css('overflow-y', 'hidden');
        _this.page.css('overflow', 'auto');

        // Remove searched
        setTimeout(function() {
          _this.search.removeClass('searched');
          $("#result-news").html("");
          $('#search-input').val("");
        }, 1000);
      });
    }


    /* ---------------------------------
    |               Nav                 |
    ---------------------------------- */
    _this.initNav = function() {
      animeEltsDelay(_this.nav.find('.nav-anime'), 100, true, 'active', null);
      
      _this.nav.find('#news-nav-liseret').addClass('active');
    }

    /* ---------------------------------
    |             List News             |
    ---------------------------------- */
    _this.initListNews = function() {
      animeEltsDelay(_this.news, 100, true, 'active', null);
    }

    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    this.loadAndInit = function() {
      // Load images & init page
      _this.page.imagesLoaded()
        .done( function( instance ) {
            console.log('all images successfully loaded');
            _this.initPage();
          })
          .fail( function() {
            console.log('all images loaded, at least one is broken');
            _this.initPage();
          });
    }


    _this.initPage = function() {
      // Init nav
      _this.initNav();

      // Init list news
      setTimeout(function() {
        _this.initListNews();
      }, 400);
    }


    _this.init();
  };
})(jQuery);