(function ($) {
  $.animeAppear = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.animeAppearElts = $('.anime-appear');
      _this.animeAppearBlocsTextElts = $('.anime-appear-bloc-text');
      _this.animeAppearImageElts = $('.anime-appear-image');
      _this.animeAppearParallaxElts = $('.anime-appear-parallax');
      _this.animeAppearOpacityElts = $('.anime-appear-opacity');
      _this.animeAppearCascadeTexts = $('.anime-appear-cascade-texts');
      _this.animeAppearCascadeLines = $('.anime-appear-cascade-lines');
      _this.animeAppearCascade = $('.anime-appear-cascade');     
      _this.animeRefreshResize = $('.anime-refresh-resize');

      // Ini elts
      _this.initElts();

      console.log("[ Module ] Init anim");

      _this.build();
    };

    _this.build = function() {

    }



    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    _this.initElts = function() {
      _this.animeAppearBlocsTextElts.each(function() {
        if(!$(this).hasClass('active')) {
          $(this).wrapInner('<div class="anime-appear-wrap"></div>');

          // Redim elts
          _this.redimAppearElts();
        }
      });
    }

    _this.initAgainElts = function() {
      _this.animeAppearBlocsTextElts.each(function() {
        if(!$(this).hasClass('active')) {
          if($(this).find('.anime-appear-wrap').length > 0) {
            $(this).find('.anime-appear-wrap').children().unwrap();
          }

          $(this).wrapInner('<div class="anime-appear-wrap"></div>');

          // Redim elts
          _this.redimAppearElts();
        }
      });
    }

    _this.redimAppearElts = function() {
      _this.animeAppearBlocsTextElts.each(function(index, value) {
        if(!$(this).hasClass('active')) {

          var wrap = $(this).children('.anime-appear-wrap');
          var eltHeight = wrap.height(),
              eltWidth = wrap.width();

          $(this).height(eltHeight);
        }
      });
    }

    _this.scrollTrigger = function() {

      // Display animeAppear
      var eltsModified = [];

      // --- Active visible elts
      _this.animeAppearElts.each(function() {
        if(isScrolledIntoView($(this))  && (!$(this).hasClass('active'))) {
          $(this).addClass('active');

          if($(this).hasClass('anime-appear-bloc-text')) {
            eltsModified.push($(this));
          }
        }
      });


      // --- When display, free elts size to fit if screen resize
      setTimeout(function() {
        for(var i= 0; i < eltsModified.length; i++)
        {
          if(!$(this).hasClass('active')) {
            eltsModified[i].addClass('display');
            eltsModified[i].width('100%');
            eltsModified[i].height('auto');
            eltsModified[i].find('.anime-appear-wrap').children().unwrap();
          }
        }
      }, 700);


      // Display animeAppearCascadeTexts
      // --- Active visible elts
      _this.animeAppearCascadeTexts.each(function() {
        if(isScrolledIntoView($(this))) {
          if(!$(this).hasClass('activated')) { 
            animeEltsDelay($(this).find('.cascade-text'), 100, true, 'active', null);
            $(this).addClass('activated');
          }
        }
      });

      // Display animeAppearCascadeLines
      // --- Active visible elts
      _this.animeAppearCascadeLines.each(function() {
        if(isScrolledIntoView($(this))) {
          if(!$(this).hasClass('activated')) {
            animeEltsDelay($(this).find('.cascade-line'), 100, true, 'active', null);
            $(this).addClass('activated');
          }
        }
      });

      // Display animeAppearCascade
      // --- Active visible elts
      _this.animeAppearCascade.each(function() {
        if(isScrolledIntoView($(this))) {
          if(!$(this).hasClass('activated')) {
            animeEltsDelay($(this).find('.cascade-elt'), 100, true, 'active', null);
            $(this).addClass('activated');
          }
        }
      });

      _this.animeRefreshResize.each(function() {
        if(isScrolledIntoView($(this))) {
          if(!$(this).hasClass('activated')) {
            resizeBrowser();
            $(this).addClass('activated');
          }
        }
      });
    }

    _this.init();
  };
})(jQuery);