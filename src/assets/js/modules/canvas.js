(function ($) {
  $.canvas = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.canvas_1 = $('#canvas-menu-1');
      _this.canvas_2 = $('#canvas-menu-2');

      // Variables
      // -- Size area
      _this.heightCanvas = $(window).height();
      _this.widthCanvas = 300;

      // -- Points
      _this.point0X = 120;
      _this.point1X = 280;
      _this.point2X = 220;
      _this.point3X = 240;

      // -- Canvas infos
      _this.originX = 100;
      _this.originY = 0;
      _this.endX = 100;
      _this.endY = _this.heightCanvas;
      _this.midY = _this.heightCanvas / 2;

      // -- Animation
      _this.phase = 0;
      _this.xMove = _this.point0X;
      _this.move = true;
      _this.moveMask = false;
      _this.speed = 13;

      _this.currentMenuElt = $('.current');
      _this.currentEltPositionTop = _this.currentMenuElt.offset().top - $(document).scrollTop();
      _this.currentEltHeight = _this.currentMenuElt.height();

      _this.currentMenuEltHover;
      _this.maskTargetPositionTop;

      _this.maskPositionTop = _this.currentEltPositionTop;
      _this.speedMask = 1;

      console.log("[ Module ] Init Canvas");

      _this.build();
    };

    _this.build = function() {

    };

    _this.initCanvas = function(){
      console.log("[ Action ] Draw canvas");
      // Init move
      //_this.move = true;
      //_this.xMove = _this.point0X;

      // Re-init animation
      _this.phase = 0,
      _this.xMove = _this.point0X;
      _this.move = true;
      _this.speed = 13;

      // Draw init canvas
      _this.drawArcs();

      setTimeout(function(){ window.requestAnimationFrame(_this.animArcs); }, 400);
    }

    _this.initCanvasMask = function() {
      console.log("[ Action ] Draw mask");

      _this.moveMask = true;

      //_this.maskPositionTop = _this.currentEltPositionTop;
      _this.speedMask = 1.4;

      _this.drawArcs();
      window.requestAnimationFrame(_this.animArcs);
    }

    _this.animArcs = function() {

        if(_this.move) {
            
            //speed = speed * 0.909;

            switch(_this.phase) {
                case 0:
                    if(_this.xMove < _this.point1X) {
                        _this.xMove += _this.speed;
                    } else {
                        _this.xMove -= _this.speed;
                        _this.phase = 1;
                        _this.speed = _this.speed * 0.3;
                    }
                    break;
                case 1:
                    if(_this.xMove > _this.point2X) {
                        _this.xMove -= _this.speed;
                    } else {
                        _this.xMove += _this.speed;
                        _this.phase = 2;
                        _this.speed = _this.speed * 0.2;
                    }
                    break;
                case 2:
                    if(_this.xMove < _this.point3X) {
                        _this.xMove += _this.speed;
                    } else {
                        _this.move = false;
                    }
                default:
                    break;
            }

            _this.drawArcs();

            // TO DO
            /*
            Faire suivre la zone blanche du canvas selon position de la souris
            => à chaque mourvement de souris, redessiner le canvas 2 et faire un masque
            dont l'origine est l'ordonnée de la souris
            */

            window.requestAnimationFrame(_this.animArcs);
        }

        if(_this.moveMask) {

          _this.speedMask = _this.speedMask* 1.08;           
          if((_this.maskPositionTop > _this.maskTargetPositionTop + 20)) {
            _this.maskPositionTop -= _this.speedMask;
          } else if((_this.maskPositionTop < _this.maskTargetPositionTop - 20)) {
            _this.maskPositionTop += _this.speedMask;
          } else {
            _this.maskPositionTop = _this.maskTargetPositionTop;
            //_this.speedMask = 1;
            _this.moveMask = false;
            _this.speedMask = 1.4;
          }

          _this.drawArcs();

          window.requestAnimationFrame(_this.animArcs);
        }
    }

    _this.drawArcs = function() {
        var c1 = document.getElementById('canvas-menu-1'),
            ctx1 = c1.getContext('2d'),
            c2 = document.getElementById('canvas-menu-2'),
            ctx2 = c2.getContext('2d');

        // Resize & refresh canvas
        c1.height = _this.heightCanvas;
        ctx1.clearRect(0,0,_this.widthCanvas,_this.heightCanvas); // effacer le canvas
        c2.height = _this.heightCanvas;
        ctx2.clearRect(0,0,_this.widthCanvas,_this.heightCanvas); // effacer le canvas

        // Arc 1
        ctx1.beginPath();
        ctx1.moveTo(_this.originX,0);
        ctx1.quadraticCurveTo(_this.xMove,_this.midY,_this.endX,_this.endY);
        ctx1.strokeStyle = '#393939';
        ctx1.lineWidth = 2;
        //ctx.strokeStyle = '#fff';
        ctx1.stroke();
        ctx1.closePath();

        // masque menu
        ctx2.beginPath();
        ctx2.fillStyle = "transparent";
        //ctx2.strokeStyle = '#fff';
        ctx2.rect(0,_this.maskPositionTop,300,_this.currentEltHeight);
        ctx2.clip();
        ctx2.fill();
        ctx2.closePath();

        // Arc 2
        ctx2.beginPath();
        ctx2.strokeStyle = '#fff';
        ctx2.lineWidth = 3;
        ctx2.moveTo(_this.originX,0);
        ctx2.quadraticCurveTo(_this.xMove,_this.midY,_this.endX,_this.endY);
        ctx2.stroke();
        ctx2.closePath();
    }

    _this.drawArcsToLine = function() {
      var c1 = document.getElementById('canvas-menu-1'),
          ctx1 = c1.getContext('2d'),
          c2 = document.getElementById('canvas-menu-2'),
          ctx2 = c2.getContext('2d');

      if(_this.xMove > _this.originX) {

        // Resize & refresh canvas
        c1.height = _this.heightCanvas;
        ctx1.clearRect(0,0,_this.widthCanvas,_this.heightCanvas); // effacer le canvas
        c2.height = _this.heightCanvas;
        ctx2.clearRect(0,0,_this.widthCanvas,_this.heightCanvas); // effacer le canvas

        // Change speed & update xMove
        _this.speed = _this.speed * 0.92;
        _this.xMove -= _this.speed;

        // Arc 1
        ctx1.beginPath();
        ctx1.moveTo(_this.originX,0);
        ctx1.quadraticCurveTo(_this.xMove,_this.midY,_this.endX,_this.endY);
        ctx1.strokeStyle = '#393939';
        ctx1.lineWidth = 2;
        //ctx.strokeStyle = '#fff';
        ctx1.stroke();
        ctx1.closePath();

        // masque menu
        ctx2.beginPath();
        ctx2.fillStyle = "transparent";
        ctx2.rect(0,_this.maskPositionTop,300,_this.currentEltHeight);
        ctx2.clip();
        ctx2.fill();
        ctx2.closePath();

        // Arc 2
        ctx2.beginPath();
        ctx2.strokeStyle = '#fff';
        ctx2.lineWidth = 3;
        ctx2.moveTo(_this.originX,0);
        ctx2.quadraticCurveTo(_this.xMove,_this.midY,_this.endX,_this.endY);
        ctx2.stroke();
        ctx2.closePath();

        window.requestAnimationFrame(_this.drawArcsToLine);
      }
    }

    _this.initArcsToLine = function() {
      _this.speed = 15;
      window.requestAnimationFrame(_this.drawArcsToLine);
    }

    _this.followMenuDrawArcs = function(eltHover) {
      _this.currentMenuEltHover = eltHover;
      _this.maskTargetPositionTop = eltHover.offset().top - $(document).scrollTop();
      
      _this.initCanvasMask();
    }

    _this.backMenuDrawArcs = function() {
      _this.maskTargetPositionTop = _this.currentMenuElt.offset().top - $(document).scrollTop();

      _this.initCanvasMask();
    }

    _this.init();
  };
})(jQuery);