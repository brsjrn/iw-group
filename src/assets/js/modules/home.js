(function ($) {
  $.home = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.imagesLoadArea = $('#images-load-area');
      _this.intro = $('#intro');
      _this.introTransition = $('#intro-transition');
      _this.page = $('.page');
      _this.texts = $('#home #texts');
      _this.social = $('#home #socials');
      _this.socials = $('#home #socials li');
      _this.workNum = $('#home #work-num');
      _this.workNums = $('#home #work-num li');
      _this.viewProjectLink = $('#home #view-project-link');
      _this.navArrows = $('#home .nav-arrow');
      _this.navArrowTop = $('#home #arrow-top');
      _this.navArrowTopId = 'arrow-top';
      _this.navArrowBottom = $('#home #arrow-bottom');
      _this.navArrowBottomId = 'arrow-bottom';
      _this.navArrowTop1 = $('#home #arrow-top-1');
      _this.navArrowTop2 = $('#home #arrow-top-2');
      _this.navArrowBottom1 = $('#home #arrow-bottom-1');
      _this.navArrowBottom2 = $('#home #arrow-bottom-2');
      _this.bullets = $('.bullets-nav li');


      //-- Texts
      _this.texts = $('#texts');
      _this.titles = $('#texts #title ul li');
      _this.toptitles = $('#toptitle ul li');
      _this.descriptions =  $('#description ul li');

      // Medias
      _this.medias = $('.media');
      _this.mediasTransition = $('#home #media-transition');

      // -- Top
      _this.mailto = $('#mailto');


      // -- Video
      _this.homeVideo = $('#home #home-video');
      _this.videoBtnPlay = $('#home .video-play-btn');


      // Works URLs
      _this.worksUrls = $('.work-url');

      // Variables
      _this.animationTimeout = 1400;
      _this.arrowSpeed = 500;
      _this.cptScreen = 0;
      _this.cptScreenPrev;
      _this.numTotalScreen = 4;

      /* MOBILE RESPONSIVE */
      // Elts
      _this.mobileNext = $('#home #home-mobile-next');
      _this.mobileHomeElts = $('#home .home-mobile-elt');
      _this.mobileBottomSocials = $('#home #socials-mobile');
      _this.mobileBottomWorksList = $('#home #works-list-mobile');
      _this.videoBtnPlayMobile = $('#home .video-play-btn-mobile');

      // Variables
      _this.mobileAnimationTimeout = 600;
      _this.mobileCptScreen = 0;
      _this.mobileCptScreenPrev;
      _this.mobileIsMove = false;


      console.log("[ Module ] Init Home");

      if(_this.intro.length > 0) {
        _this.initPreHome();
      } else {
        // Load image & init page
        _this.loadAndInit();
      }

      _this.build();
    };

    _this.build = function() {

      // Click arrow
      _this.navArrows.on('click', function() {
        if(!isScrolling) {
          
          isScrolling = true;

          if($(this).attr('id') == _this.navArrowTopId) {
            // Arrow top

            // -- Update cpt
            _this.decreaseCpt();

            // -- Anime arrow top
            _this.animeArrowTop();

          } else {
            // Arrow bottom
            // -- Update cpt
            _this.increaseCpt();

            // -- Anime arrow bottom
            _this.animeArrowBottom();

          }
        }
      });

      // Click bullet
      _this.bullets.on('click', function() {
        if(!isScrolling) {
          isScrolling = true;
          
          var clickedBullet = _this.bullets.index($(this));

          // update cpt
          _this.updateCpt(clickedBullet);
        }
      });

      // Click work num
      _this.workNums.on('click', function() {
        if(!isScrolling) {
          isScrolling = true;

          var clickedWorkNum = _this.workNums.index($(this)) + 1;

          // update cpt
          _this.updateCpt(clickedWorkNum);
        }
      });

      // Mailto hover
      _this.mailto.hover(function() {
        $(this).children('#liseret').stop().animate({
          width: '100%',
          left: '0'
        }, {
          duration: 300,
          specialEasing: {
              width: "easeInQuart",
              left: "easeInQuart"
          }
        });
      }, function() {
        $(this).children('#liseret').stop().animate({
          width: '0',
          left: '100%'
        }, {
          duration: 300,
          specialEasing: {
              width: "easeInQuart",
              left: "easeInQuart"
          },
          complete: function() {
            $(this).css('left', 0);
          }
        });
      });

      // Call scroll manager
      scrollTriggerFunction(_this.decreaseCpt, _this.increaseCpt, _this.scrollingTime);
    
      // Zoom when "View project" hover
      _this.viewProjectLink.children('a').hover(function() {
        $('.zoomable.active').addClass('zoom');
      }, function() {
        $('.zoomable.zoom').removeClass('zoom');
      });

      /* MOBILE RESPONSIVE */
      // Click next arrow
      _this.mobileNext.on('click', function() {
        if(!_this.mobileIsMove) {
          _this.mobileIsMove = true;
          _this.mobileIncreaseCpt();
        }
      });

      // Click works num
      _this.mobileBottomWorksList.find('li').on('click', function() {
        if(!_this.mobileIsMove) {
          _this.mobileIsMove = true;
          var indexWork = _this.mobileBottomWorksList.find('li').index($(this)) + 1;
          _this.mobileUpdateCpt(indexWork);
        }
      });

      // Swipe elts
      _this.mobileHomeElts.on('swipeleft', function() {
        if(!_this.mobileIsMove) {
          _this.mobileIsMove = true;
          _this.mobileIncreaseCpt();
        }
      });

      _this.mobileHomeElts.on('swiperight', function() {
        if(!_this.mobileIsMove) {
          _this.mobileIsMove = true;
          _this.mobileDecreaseCpt();
        }
      });

      // Play / stop video
      // Play video
      if( _this.homeVideo.length > 0) {
        var iframe = document.querySelector('#home-video-player');
        var player = new Vimeo.Player(iframe);

        _this.homeVideo.find('#home-video-close').on('click', function() {
          _this.homeVideo.fadeOut();
          player.pause();
        })

        _this.videoBtnPlay.on('click', function() {
          _this.homeVideo.fadeIn();
          player.play();
        });

        _this.videoBtnPlayMobile.on('click', function() {
          _this.homeVideo.fadeIn();
          player.play();
        });

        player.on('pause', function() {
            _this.videoPlayBtn.fadeIn();
        });
      }

    }

    /* ---------------------------------
    |                Cpt                |
    ---------------------------------- */
    _this.updateCpt = function(newCpt) {
      _this.cptScreenPrev = _this.cptScreen;
      _this.cptScreen = newCpt;

      // Update Bullet
      _this.updateBullets();
      _this.updateWorksNums();

      // Anime Socials
      _this.updateSocials();

      // Anime medias
      _this.updateMedias();

      // Update Texts
      _this.updateTexts();


      // Update Works Urls
      _this.viewProjectLink.children('a').attr('href', _this.worksUrls.eq(_this.cptScreen-1).html());
      
      // End animation
      setTimeout(function() {
        isScrolling =  false;
      }, _this.animationTimeout);
    }

    _this.increaseCpt = function() {
      if(_this.cptScreen < _this.numTotalScreen-1) {
        _this.updateCpt(_this.cptScreen+1);
      } else {
        _this.updateCpt(0);
      }
    }

    _this.decreaseCpt = function() {
      if(_this.cptScreen > 0) {
        _this.updateCpt(_this.cptScreen-1);
      } else {
        _this.updateCpt(_this.numTotalScreen-1);
      }
    }

    /* ---------------------------------
    |              Bullets              |
    ---------------------------------- */
    _this.updateBullets = function() {
      _this.bullets.removeClass('active');
      _this.bullets.eq(_this.cptScreen).addClass('active');
    }

    /* ---------------------------------
    |              Work num             |
    ---------------------------------- */
    _this.updateWorksNums = function() {
      if(_this.cptScreen != 0) {
        _this.workNums.removeClass('select');
        _this.workNums.eq(_this.cptScreen-1).addClass('select');
      }

      _this.animeWorksNums();
    }

    _this.animeWorksNums = function() {
      if(_this.cptScreen == 0) {
        animeEltsDelay(_this.workNums, 100, true, null, 'active');
      } else {
        setTimeout(function() {animeEltsDelay(_this.workNums, 100, true, 'active', null)}, 300);
      }
    }

    /* ---------------------------------
    |              Arrows               |
    ---------------------------------- */
    _this.animeArrowTop = function() {
      // Position arrows
      _this.navArrowTop1.css('top', 0);
      _this.navArrowTop2.css('top', 30);

      // Arow opacity
      _this.navArrowTop2.css('opacity', 0);

      // Anime arrows 1
      _this.navArrowTop1.animate({
        top: -30,
        opacity: 0
      }, {
        duration: _this.arrowSpeed,
        specialEasing: {
            top: "easeInQuart",
            opacity: "easeInQuart"
        },
        complete: function() {
          _this.navArrowTop1.css('top', 0);
          _this.navArrowTop1.css('opacity', 1);
        }
      });

      // Anime arrows 2
      _this.navArrowTop2.animate({
        top: 0,
        opacity: 1
      }, {
        duration: _this.arrowSpeed,
        specialEasing: {
            top: "easeInQuart",
            opacity: "easeInQuart"
        }
      });
    }

    _this.animeArrowBottom = function() {
      // Position arrows
      _this.navArrowBottom1.css('top', 0);
      _this.navArrowBottom2.css('top', -30);

      // Arow opacity
      _this.navArrowBottom2.css('opacity', 0);

      // Anime arrows 1
      _this.navArrowBottom1.animate({
        top: 30,
        opacity: 0
      }, {
        duration: _this.arrowSpeed,
        specialEasing: {
            top: "easeInQuart",
            opacity: "easeInQuart"
        },
        complete: function() {
          _this.navArrowBottom1.css('top', 0);
          _this.navArrowBottom1.css('opacity', 1);
        }
      });

      // Anime arrows 2
      _this.navArrowBottom2.animate({
        top: 0,
        opacity: 1
      }, {
        duration: _this.arrowSpeed,
        specialEasing: {
            top: "easeInQuart",
            opacity: "easeInQuart"
        }
      });
    }

    /* ---------------------------------
    |               Texts               |
    ---------------------------------- */
    _this.initTexts = function() {
      console.log("Init texts");

      // Wrap title content with ".text-anime" div class
      _this.titles.find('p').wrapInner('<div class="text-anime"></div>');

      // Active first screen
      _this.toptitles.eq(_this.cptScreen).show();
      _this.titles.eq(_this.cptScreen).addClass('active');
      _this.descriptions.eq(_this.cptScreen).show();

      // Anime first screen
      setTimeout(function() {
        animeEltsDelay($('#title ul li').eq(_this.cptScreen).find('p'), 150, true, 'active', null);
      }, 100);

      setTimeout(function() {
        _this.toptitles.eq(_this.cptScreen).addClass('active');
      }, 200);

      setTimeout(function() {
        _this.descriptions.eq(_this.cptScreen).addClass('active');
      }, 500);

    }

    _this.updateTexts = function() {
      _this.animeTexts();
    }

    _this.animeTexts = function() {

      // -- OLD SCREEN
      // Title leaves
      animeEltsDelay(_this.titles.eq(_this.cptScreenPrev).find('p'), 80, true, 'inactive-top', null);

      // Top & description leave
      setTimeout(function() {
        _this.toptitles.eq(_this.cptScreenPrev).removeClass('active');
        _this.descriptions.eq(_this.cptScreenPrev).removeClass('active');
      }, 300);

      // Remove title & top & description
      setTimeout(function() {
        _this.titles.eq(_this.cptScreenPrev).removeClass('active');
        _this.titles.eq(_this.cptScreenPrev).find('p').removeClass('active inactive-top');
        _this.toptitles.eq(_this.cptScreenPrev).hide();
        _this.descriptions.eq(_this.cptScreenPrev).hide();
      }, 700);

      // -- NEW SCREEN
      // Active title & top & description
      setTimeout(function() {
        _this.titles.eq(_this.cptScreen).addClass('active');
        _this.toptitles.eq(_this.cptScreen).show();
        _this.descriptions.eq(_this.cptScreen).show();

        // Title appears
        animeEltsDelay(_this.titles.eq(_this.cptScreen).find('p'), 80, true, 'active', null);
      }, 700);

      // Tio & description appear
      setTimeout(function() {
        _this.toptitles.eq(_this.cptScreen).addClass('active');
        _this.descriptions.eq(_this.cptScreen).addClass('active');
      }, 1000);
  
    }

    /* ---------------------------------
    |              Medias               |
    ---------------------------------- */
    _this.initMedias = function() {
      //_this.medias.eq(_this.cptScreen).addClass('active');
    }

    _this.updateMedias = function() {
      if(_this.cptScreen == 0) {
        _this.viewProjectLink.removeClass('active');
      } else {
        _this.viewProjectLink.addClass('active');
      }

      _this.animeMedias();
    }

    _this.animeMedias = function() {
      
      // Media transition
      _this.animeMediasTransition();

      setTimeout(function() {
        // OLD MEDIA
        _this.medias.eq(_this.cptScreenPrev).css('z-index', 1);

        // NEW MEDIA
        _this.medias.eq(_this.cptScreen).css('z-index', 3);
        _this.medias.eq(_this.cptScreen).addClass('active');
      }, 300);

      setTimeout(function() {
        _this.medias.eq(_this.cptScreenPrev).removeClass('active');
      }, 1200);
    }

    _this.animeMediasTransition = function() {
      /*
      _this.mediasTransition.animate({
          top: '-400%',
          height: '400%'
        }, {
          duration: 1600,
          specialEasing: {
            top: "easeInOutCubic",
            height: "easeInOutCubic"
          },
          complete: function() {
            $(this).css('top', '100%');
            $(this).css('height', '60%');
          }
      });
      */
      _this.mediasTransition.addClass('active');

      setTimeout(function() {
        _this.mediasTransition.removeClass('active');
      }, 1300);
    }

    /* ---------------------------------
    |              Socials              |
    ---------------------------------- */
    _this.initSocials = function() {
      setTimeout(function() {animeEltsDelay(_this.socials, 100, false, 'active', 'inactive-top')}, 300);
    }

    _this.updateSocials = function() {
      _this.animeSocials();
    }

    _this.animeSocials = function() {
      if(_this.cptScreen == 0) {
        setTimeout(function() {animeEltsDelay(_this.socials, 100, false, 'active', 'inactive-top')}, 300);
      } else {
        animeEltsDelay(_this.socials, 100, false, 'inactive-top', null);
      }
    }

    /* ---------------------------------
    |              MOBILE               |
    ---------------------------------- */
    _this.mobileUpdateCpt = function(newCpt) {
      _this.mobileCptScreenPrev = _this.mobileCptScreen;
      _this.mobileCptScreen = newCpt;

      if(_this.mobileCptScreenPrev != _this.mobileCptScreen) {
        _this.mobileAnimeHomePage();
        _this.mobileUpdateHomeBottom();
      } else {
        _this.mobileIsMove = false;
      }
    }

    _this.mobileIncreaseCpt = function() {
      if(_this.mobileCptScreen < _this.numTotalScreen-1) {
        _this.mobileUpdateCpt(_this.mobileCptScreen+1);
      } else {
        _this.mobileUpdateCpt(0);
      }
    }

    _this.mobileDecreaseCpt = function() {
      if(_this.mobileCptScreen > 0) {
        _this.mobileUpdateCpt(_this.mobileCptScreen-1);
      } else {
        _this.mobileUpdateCpt(_this.numTotalScreen-1);
      }
    }

    _this.mobileAnimeHomePage = function() {
      _this.mobileHomeElts.eq(_this.mobileCptScreenPrev).css('z-index', 2);
      _this.mobileHomeElts.eq(_this.mobileCptScreen).css('z-index', 3);
      _this.mobileHomeElts.eq(_this.mobileCptScreen).addClass('active');

      setTimeout(function() {
        _this.mobileIsMove = false;

        _this.mobileHomeElts.eq(_this.mobileCptScreenPrev).removeClass('active');
        _this.mobileHomeElts.eq(_this.mobileCptScreenPrev).css('z-index', 1);
      }, _this.mobileAnimationTimeout)
    }

    _this.mobileUpdateHomeBottom = function() {
      if(_this.mobileCptScreen == 0) {
        _this.mobileBottomSocials.addClass('active');
        _this.mobileBottomWorksList.removeClass('active');
      } else {
        _this.mobileBottomSocials.removeClass('active');
        _this.mobileBottomWorksList.addClass('active');

        this.mobileBottomWorksList.find('li').removeClass('active');
        this.mobileBottomWorksList.find('li').eq(_this.mobileCptScreen - 1).addClass('active');
      }
    }

    /* ---------------------------------
                  PreHome               |
    ---------------------------------- */
    _this.initPreHome = function() {
      console.log('[Load pre-home]');

      // Load pre-home
      _this.intro.imagesLoaded()
        .done( function( instance ) {
            console.log('Pre-home loaded');

            setTimeout(function() {
              _this.loadAndInit();
            }, 2000);
          })
          .fail( function() {
            console.log('Pre-home loaded, at least one is broken');
            
            setTimeout(function() {
              _this.loadAndInit();
            }, 2000);
          });
    }

    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    _this.loadAndInit = function() {
      console.log('[Load home]');

      // Load images & init page
      _this.page.imagesLoaded()
        .done( function( instance ) {
            console.log('all images successfully loaded');
            if(_this.intro.length > 0) {
              console.log("intro transition");
              _this.intro.fadeOut(function() {
                _this.initPage();
              });

              /*
              _this.introTransition.animate({
                top: '-400%',
                height: '400%'
              }, 1000);
              */
              _this.introTransition.addClass('active');

            } else {
              /*
              _this.introTransition.animate({
                top: '-400%',
                height: '400%'
              }, 1000);
              */
              _this.introTransition.addClass('active');


              setTimeout(function() {
                _this.initPage();
              }, 500);
            }
          })
          .fail( function() {
            console.log('all images loaded, at least one is broken');
            if(_this.intro.length > 0) {
              _this.intro.fadeOut(function() {
                _this.initPage();
              });
            } else {
              _this.initPage();
            }
          });
    }


    _this.initPage = function() {
      console.log("Init home page");
      _this.initSocials();

      // Init text
      _this.initTexts();

      // Init medias
      _this.initMedias();
    }



    _this.init();
  };
})(jQuery);