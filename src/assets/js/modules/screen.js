(function ($) {
  $.screen = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.toggleFullScreenBtn = $('.toggle-full-screen');
      // Variables

      console.log("[ Module ] Screen");

      _this.build();
    };

    _this.build = function() {

      // Toggle fullscreen on click
      _this.toggleFullScreenBtn.on('click', function() {
          //simulateKeyPress(this, {keyCodeArg: 122});
          _this.toggleFullScreen();
      });
    }    

    /* ---------------------------------
    |             Functions             |
    ---------------------------------- */
    _this.toggleFullScreen = function() {
      if ((document.fullScreenElement && document.fullScreenElement !== null) ||    
       (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if (document.documentElement.requestFullScreen) {  
          document.documentElement.requestFullScreen();  
        } else if (document.documentElement.mozRequestFullScreen) {  
          document.documentElement.mozRequestFullScreen();  
        } else if (document.documentElement.webkitRequestFullScreen) {  
          document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);  
        }  
      } else {  
        if (document.cancelFullScreen) {  
          document.cancelFullScreen();  
        } else if (document.mozCancelFullScreen) {  
          document.mozCancelFullScreen();  
        } else if (document.webkitCancelFullScreen) {  
          document.webkitCancelFullScreen();  
        }  
      }  
    }

    _this.init();
  };
})(jQuery);