(function ($) {
  $.about = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.page = $('.page');
      _this.header = $('#about #header');
      _this.headerBackground = $('#about #header #background');
      _this.headerAnimeTexts = $('#about #header').find('.anime-header-text');
      _this.headerLogos = $('#about #header #list-logos li');
      _this.goDownBtn = $('#about #header #go-down');
      _this.writeTitle = $('#about #header #title');
      _this.writeTexts = $('#about #header .write-text');
      _this.writeTexts1 = $('#about #header .write-text-1');
      _this.writeTexts2 = $('#about #header .write-text-2');
      _this.trombi = $('#about #trombi');
      _this.slider = $('#about .slider');
      _this.list = $('#about #list-services');
      _this.listWithBackground;

      /*
      _this.listServices = $('#about #services #list-services');
      _this.listServicesElts = $('#about #services #list-services ul li span');
      _this.listServicesBackgrounds = $('#about #services #list-services #list-services-backgrounds');
      _this.listServicesBackgroundsmasks = $('#about #services #list-services #list-services-backgrounds .service-background-mask');
      */

      _this.listWidthBackgrounds = $('.list-with-background');

      // Variables
      _this.writeTextsCptsArray = [];
      _this.writeSpeed = 90;
      _this.pauseSpeed = 400;
      _this.isPageInit = false;
      _this.writeTextGlobalCpt = 0;

      // Load image & init page
      _this.loadAndInit();

      console.log("[ Module ] Init About");

      _this.build();
    };

    _this.build = function() {

      // Click on header go down btn
      _this.goDownBtn.on('click', function() {
        scrollTo('#text-1', 100, 800);
      });
    }

    /* ---------------------------------
    |               Header              |
    ---------------------------------- */
    _this.initHeader = function() {
      // Display background
      _this.headerBackground.addClass('active');

      // Display anime texts
      setTimeout(function() {
        animeEltsDelay(_this.headerAnimeTexts, 100, true, 'active', null);
      }, 0);

      // Display anime texts
      setTimeout(function() {
        animeEltsDelay(_this.headerLogos, 100, true, 'active', null);
      }, 400);

      // Anime write texts
      _this.initWriteTexts();

      setTimeout(function() {
        _this.startAnimeWriteTexts();  
      }, 1600);
    }

    // Write texts
    _this.initWriteTexts = function() {

      _this.writeTexts.each(function(index, element) {

        _this.writeTextsCptsArray[index] = [];
        
        // Init cpts
        _this.writeTextsCptsArray[index][0] = 0;
        // Elt
        _this.writeTextsCptsArray[index][1] = $(element);
        // Elt text
        _this.writeTextsCptsArray[index][2] = $(element).text();
        // Current writing text
        _this.writeTextsCptsArray[index][3] = "";
        // Record length
        _this.writeTextsCptsArray[index][4] = $(element).text().length;

        // Hide texts
        $(element).html("");
      });
    }

    _this.startAnimeWriteTexts = function() {
      // Start anim ...
      _this.animeWriteTexts(0);
    }

    _this.animeWriteTexts = function(index) {
      console.log("INDEX : "+ index);

      var cpt = _this.writeTextsCptsArray[index][0];

      console.log("CPT : "+ cpt);

      if(cpt == 0) {
        if(_this.writeTextGlobalCpt%2 == 0) {
          _this.writeTitle.find('.active-text').removeClass('active-text');
        }

        _this.writeTextsCptsArray[index][1].addClass('active-text');
      }

      if(cpt <= _this.writeTextsCptsArray[index][4] - 1) {
        // Concat new letter
        _this.writeTextsCptsArray[index][3] = _this.writeTextsCptsArray[index][3] + _this.writeTextsCptsArray[index][2].charAt(cpt);
        // Display new string
        _this.writeTextsCptsArray[index][1].html(_this.writeTextsCptsArray[index][3]);
        // Iterate cpt
        _this.writeTextsCptsArray[index][0]++;

        // Call again
        setTimeout(function() {
          _this.animeWriteTexts(index);
        }, _this.writeSpeed);

      } else {
        // End of this current text anim (turn orange & next text)


        setTimeout(function() {
          if(_this.writeTextGlobalCpt < _this.writeTexts.length-1) {

            _this.writeTextGlobalCpt++;

            // Text turns white
            _this.writeTextsCptsArray[index][1].addClass('white');

            if(_this.writeTextGlobalCpt%2 == 1) {
              _this.animeWriteTexts((_this.writeTextGlobalCpt-1)/2 + _this.writeTexts.length/2);
            } else {
              _this.animeWriteTexts(_this.writeTextGlobalCpt/2);            
            }
          } else {
            _this.writeTextGlobalCpt = 0;
            _this.reinitAnimeWriteTexts();
          }

        }, _this.pauseSpeed);
      }

    }

    _this.reinitAnimeWriteTexts = function() {

      setTimeout(function() {
        _this.writeTexts.each(function(indexTmp, elementTmp) {
          // Reinit cpt
          _this.writeTextsCptsArray[indexTmp][0] = 0;
          // Hide texts
          $(elementTmp).html("");
          // Reinit string
          _this.writeTextsCptsArray[indexTmp][3] = "";
          _this.writeTextsCptsArray[indexTmp][1].removeClass('white');
          
        });
        // Start again !
        _this.startAnimeWriteTexts();
      },  _this.pauseSpeed);
    }

    /* ---------------------------------
                   Services             |
    ---------------------------------- */
    /*
    _this.animeServices = function() {

    }

    _this.resizeServices = function() {
      _this.listWidthBackgrounds.find('.background-mask').height(_this.listServices.height() + 60);
    }
    */

    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    _this.loadAndInit = function() {
      // Load images & init page
      _this.page.imagesLoaded()
        .done( function( instance ) {
            console.log('all images successfully loaded');
            _this.initPage();
          })
          .fail( function() {
            console.log('all images loaded, at least one is broken');
            _this.initPage();
          });
    }

    _this.initPage = function() {
      _this.isPageInit = true;

      // Init header
      _this.initHeader();

      // Init slider
      new $.slider(_this.slider);

      // Init list with background
      _this.listWithBackground = new $.listWithBackground(_this.list);
    }

    _this.resizePage = function() {
      console.log('resize about');
      if(_this.isPageInit) {
        _this.listWithBackground.resizePage();
      }
    }


    _this.init();
  };
})(jQuery);