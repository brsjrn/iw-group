(function ($) {
  $.workIndex = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.page = $('.page');

      // -- Covers
      _this.covers = $('.cover');
      _this.coverLeft = $('#work-index-left .cover');
      _this.coverRight = $('#work-index-right .cover');
      _this.nextWorkBtn = $('#work-index-next');
      _this.worksUrls = $('#work-index .work-url');

      // -- Title
      _this.workTitles = $('#work-index-title .title-elt');

      // -- Nums
      _this.workNums = $('.current-num-elt');
      _this.totalNum = $('#total-num');

      // -- Other elts 
      _this.viewProjectLink = $('#view-project-link');

      // Variables
      _this.cptWorkPrev = 0;
      _this.cptWork = 0;
      _this.numTotalWork = _this.coverLeft.length;
      _this.isMove = false;
      _this.scrollingTime = 800;

      // Init elts
      // Load image & init page
      _this.loadAndInit();

      console.log("[ Module ] Work Index");

      _this.build();
    };

    _this.build = function() {


      // Click arrow next
      _this.nextWorkBtn.on('click', function() {
        //if(!_this.isMove) {
        if(!isScrolling) {
          //_this.isMove = true;
          isScrolling = true;
          _this.increaseCptWork();
        }
      });

      // Call scroll manager
      scrollTriggerFunction(_this.decreaseCptWork, _this.increaseCptWork, _this.scrollingTime);

    } 

    /* ---------------------------------
    |             Cpt Work              |
    ---------------------------------- */
    _this.updateCptWork = function(newCpt) {
      _this.cptWorkPrev = _this.cptWork;
      _this.cptWork = newCpt;

      // Update link
      _this.viewProjectLink.attr('href', _this.worksUrls.eq(_this.cptWork).html());

      // Remove zoomable
      _this.covers.removeClass('zoomable');

      // Update title
      setTimeout(function() {
        _this.animeTitle();
      }, 200);

      // Update covers
      setTimeout(function() {
        _this.animeCovers();
      }, 500);

      // Update nums
      setTimeout(function() {
        _this.animeNums();
        // Remove zoomable
      _this.covers.addClass('zoomable');
      }, 800);
    }

    _this.increaseCptWork = function() {
      if(_this.cptWork < _this.numTotalWork-1) {
        _this.updateCptWork(_this.cptWork+1);
      } else {
        _this.updateCptWork(0);
      }
    }

    _this.decreaseCptWork = function() {

      if(_this.cptWork > 0) {
        _this.updateCptWork(_this.cptWork-1);
      } else {
        _this.updateCptWork(_this.numTotalWork-1);
      }
    }

    /* ---------------------------------
    |                Covers             |
    ---------------------------------- */
    _this.initCovers = function() {
      _this.animeCovers();
    }

    _this.animeCovers = function() {
      var prevWorkLeft = _this.coverLeft.eq(_this.cptWorkPrev),
          currentWorkLeft = _this.coverLeft.eq(_this.cptWork),
          prevWorkRight = _this.coverRight.eq(_this.cptWorkPrev),
          currentWorkRight = _this.coverRight.eq(_this.cptWork);

      // Show new covers
      currentWorkLeft.css('opacity', 1);
      currentWorkRight.css('opacity', 1);

      setTimeout(function() {
        currentWorkLeft.addClass('active'); // Appear
        currentWorkRight.addClass('active'); // Appear
      }, 200);

      // Hide old cover
      if(_this.cptWork != _this.cptWorkPrev) {
        prevWorkLeft.addClass('inactive-top'); // Disappear
        prevWorkRight.addClass('inactive-top'); // Disappear
      
        setTimeout(function() {
          prevWorkLeft.css('opacity', 0);
          prevWorkRight.css('opacity', 0);

          prevWorkLeft.removeClass('active inactive-top');
          prevWorkRight.removeClass('active inactive-top');
        }, 700);

        
        setTimeout(function() {
          //_this.isMove = false;
          isScrolling = false;
        }, 800);
        
      }
    }

    /* ---------------------------------
    |              Titles               |
    ---------------------------------- */
    _this.initTitle = function() {
      _this.animeTitle();
    }

    _this.animeTitle = function() {
      var prevTitle = _this.workTitles.eq(_this.cptWorkPrev),
          currentTitle = _this.workTitles.eq(_this.cptWork);

        // Show new title
        setTimeout(function() {
          currentTitle.addClass('active'); // Appear
        }, 400);

        // Hide old title
        if(_this.cptWork != _this.cptWorkPrev) {
          prevTitle.addClass('inactive-top'); // Disappear

          setTimeout(function() {
            prevTitle.removeClass('active inactive-top');
          }, 700);
        }
    }

    /* ---------------------------------
    |               Nums                |
    ---------------------------------- */
    _this.initNums = function() {

      setTimeout(function() {
        _this.totalNum.addClass('active');
      }, 300);

      _this.animeNums();
    }

    _this.animeNums = function() {
      var prevNum = _this.workNums.eq(_this.cptWorkPrev),
          currentNum = _this.workNums.eq(_this.cptWork);

        // Show new title
        setTimeout(function() {
          currentNum.addClass('active'); // Appear
        }, 200);

        // Hide old title
        if(_this.cptWork != _this.cptWorkPrev) {
          prevNum.removeClass('active'); // Disappear
        }
    }

    /* ---------------------------------
    |            Other elts             |
    ---------------------------------- */
    _this.initOtherElts = function() {

      // Display view project link
      setTimeout(function() {
        _this.viewProjectLink.addClass('active');
      }, 700);

      // Display arrow
      setTimeout(function() {
        _this.nextWorkBtn.addClass('active');
      }, 800);
    }


    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    _this.loadAndInit = function() {
      // Load images & init page
      _this.page.imagesLoaded()
        .done( function( instance ) {
            console.log('all images successfully loaded');
            _this.initPage();
          })
          .fail( function() {
            console.log('all images loaded, at least one is broken');
            _this.initPage();
          });
    }

    _this.initPage = function() {
      _this.initCovers();
      _this.initTitle();
      _this.initNums();
      _this.initOtherElts();

      // Init Url
      _this.viewProjectLink.attr('href', _this.worksUrls.eq(_this.cptWork).html());
    }

    _this.init();
  };
})(jQuery);