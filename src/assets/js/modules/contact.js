(function ($) {
  $.contact = function(element, options) {
    var _this = this;

    _this.defaults = {
    };

    _this.init = function() {
      // Elts
      _this.page = $('.page');
      _this.texts = $('.contact-text')
      _this.headerAnimeTexts = $('.anime-header-text');
      _this.spots = $('#list-spots ul li .spot-elt');
      _this.socials = $('#socials');
      _this.socialsLinks = $('#socials a');
      _this.illustration = $('#contact-illustration');

      // Load image & init page
      _this.loadAndInit();

      // Init canvas
      _this.socialsCavans = [
        new $.socials(null, ['instagram-canvas', '#8c8c8c']),
        new $.socials(null, ['twitter-canvas', '#8c8c8c']),
        new $.socials(null, ['facebook-canvas', '#8c8c8c'])
        ];

      console.log("[ Module ] Init Contact");

      _this.build();
    };

    _this.build = function() {


      // Hover social
      _this.socialsLinks.hover(function() {
        var socialCanva = _this.socialsCavans[_this.socialsLinks.index($(this))];

        socialCanva.sens = true;
        socialCanva.animateSocialCanvas(0);
      }, function() {
        var socialCanva = _this.socialsCavans[_this.socialsLinks.index($(this))];

        socialCanva.sens = false;
        socialCanva.animateSocialCanvas(0);
      })
    }

    /* ---------------------------------
    |               Texts               |
    ---------------------------------- */
    _this.initTexts = function() {
      // Prepare text to appear
      _this.headerAnimeTexts.each(function() {
        $(this).height($(this).find('.anime-wrap-text').height());
      })

      // Display anime texts
      animeEltsDelay(_this.headerAnimeTexts, 150, true, 'active', null);
    }

    /* ---------------------------------
    |              Spots                |
    ---------------------------------- */
    _this.initSpots = function() {

      // Anime first screen
      animeEltsDelay(_this.spots, 150, true, 'active', null);

    }

    /* ---------------------------------
    |           Illustration            |
    ---------------------------------- */
    _this.initIllustration = function() {
      _this.illustration.addClass('active');
    }

    /* ---------------------------------
    |              Socials              |
    ---------------------------------- */
    _this.initSocials = function() {
      setTimeout(function() {animeEltsDelay(_this.socials, 100, false, 'active', null)}, 300);
    }


    /* ---------------------------------
    |               Page                |
    ---------------------------------- */
    _this.loadAndInit = function() {
      // Load images & init page
      _this.page.imagesLoaded()
        .done( function( instance ) {
            console.log('all images successfully loaded');
            _this.initPage();
          })
          .fail( function() {
            console.log('all images loaded, at least one is broken');
            _this.initPage();
          });
    }


    _this.initPage = function() {

      // Init text
      _this.initTexts();

      // Init illustration
      _this.initIllustration();

      // Init spots
      setTimeout(function() {
        _this.initSpots();
      }, 500);

      //Init socials
      setTimeout(function() {
        _this.initSocials();
      }, 700);
    }

    _this.init();
  };
})(jQuery);