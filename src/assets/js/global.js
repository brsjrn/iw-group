'use strict';

/* ---------------------------------
|             VARIABLES             |
---------------------------------- */

// Var global
var win,
    isScrolling = false;

// Var sizes
var large_width,
    ratio_image;

// Modules
var canvas,
    menu,
    home,
    screen,
    workIndex,
    workView,
    newsIndex,
    newsView,
    contact,
    animeAppear,
    about;

/* ---------------------------------
|               READY               |
---------------------------------- */

$(document).ready(function(){
    // Init
    initVariables();

    // Call modules
    canvas = new $.canvas();
    menu = new $.menu();
    screen = new $.screen();
    animeAppear = new $.animeAppear();

    var pageId = $('.page').attr('id');

    switch(pageId) {
        case 'home':
            home = new $.home();
            break;
        case 'work-index':
            workIndex = new $.workIndex();
            break;
        case 'work-view':
            workView = new $.workView();
            break;
        case 'news-index':
            newsIndex = new $.newsIndex();
            break;
        case 'news-view':
            newsView = new $.newsView();
            break;
        case 'about':
            about = new $.about();
            break;
        case 'contact':
            contact = new $.contact();
            break;
        default:
            break;
    }

    // Sroll
    win.scroll(function(){
        animeAppear.scrollTrigger();
    });

    // Resize
    // Resize window
    $(window).resize(function() {
        resizeBrowser();
    });

    resizeBrowser();
    
});

function initVariables() {
    win = $(window);
    large_width = $('.large-width');
    ratio_image = $('.ratio-image')
}

function resizeBrowser() {
    var marge = 100,
        video_width_ratio = 2;

    // Resize dynamic responsive
    if(viewport().width <= 1080) {

    } else {

    }

    // Resize ratio
    $('.ratio-resize-width').each(function() {
        $(this).width($(this).height() * $(this).attr('data-ratio'));
    });

    $('.ratio-resize-height').each(function() {
        $(this).height($(this).width() * $(this).attr('data-ratio'));
    });

    // Resize full-images
    $('.full-image').each(function() {
        var parent = $(this).parent('.full-image-parent');
        adaptImageToResolution(parent, $(this))
    });

    // Resize appear elts
    //animeAppear.initElts();

    // Specific pages resize
    var pageId = $('.page').attr('id');
    menu.resizePage();

    switch(pageId) {
        case 'home':
            break;
        case 'work-index':
            break;
        case 'work-view':
            break;
        case 'about':
            about.resizePage();
            break;
        case 'contact':
            break;
        default:
            break;
    }
}




/* ----------------------- */
/*          TOOLS          */
/* ----------------------- */
// Get viewport dimensions
function viewport() {
    var e = window, a = 'inner';
    if(!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ]};
}

/* adapte project detail cover to resolution */
function adaptImageToResolution(ref, target) {
    var newX;
    var newRealX;
    var newY;
    var newRealY;
    var deltaX;
    var newDeltaX;
    var deltaY;
    var newDeltaY;
    var deltaMax;
    var moveX;
    var moveY;

    var refWidth = ref.width();
    var targetWidth = target.width();
    deltaX = refWidth/targetWidth;
    deltaY = ref.height()/target.height();

    deltaMax = Math.max(deltaX, deltaY);

    newX = target.width()*deltaMax;
    newY = target.height()*deltaMax;
 
    moveX = (newX - ref.width())/2;
    moveY = (newY - ref.height())/2;


    target.width(newX);
    target.height(newY);

    target.css("margin-left", '-'+moveX+'px');
    target.css("margin-top", '-'+moveY+'px');
}

/* adapte project detail cover to resolution */
function adaptImageToResolutionPercent(ref, target) {
    var newX;
    var newRealX;
    var newY;
    var newRealY;
    var deltaX;
    var newDeltaX;
    var deltaY;
    var newDeltaY;
    var deltaMax;
    var moveX;
    var moveY;

    var refWidth = ref.width();
    var targetWidth = target.width();
    deltaX = refWidth/targetWidth;
    deltaY = ref.height()/target.height();

    deltaMax = Math.max(deltaX, deltaY);
    
    if(deltaX < deltaY) {
      newX = 'auto';
      newY = '100%';

      newRealX = targetWidth*deltaMax;
      newDeltaX = refWidth/newRealX;

    } else {
      newY = 'auto';
      newX = '100%';

      newRealY = target.height()*deltaMax;
      newDeltaY = ref.height()/newRealY;
    }
    
    if(deltaX < deltaY) {
      moveX = (1-newDeltaX)/2 * 100;
      moveY = 0;
    } else {
      moveY = (1-newDeltaY)/2 * 100;
      moveX = 0;
    }

    target.width(newX);
    target.height(newY);

    
    target.css("margin-left", '-'+moveX+'%');
    target.css("margin-top", '-'+moveY+'%');
}


/*
* Elts : list elements (array)
* Delay : delay between anims elts (int)
* Appear : appear or disappear (bool)
*/
function animeEltsDelay(elts, delay, direction, newClass, oldClass) {
  var localDelay = 0;
  var localElts = elts;

  if(!direction) {
    localElts = $(elts.get().reverse());
  }
  
  // Display elts
  localElts.each(function(index, value) {
    var $elt = $(this);
    
    $elt.queue('fade', function(next) {
      if(newClass) {
        $elt.delay(localDelay).addClass(newClass, next);
      }
      if(oldClass) {
        $elt.delay(localDelay).removeClass(oldClass, next);
      }
    });
    
    $elt.dequeue('fade');
    
    localDelay += delay;
  });
}

// Trigger scroll Uo od Down
function scrollTriggerFunction(topFunction, bottomFunction) {
  console.log("scroll");

  //Firefox
   win.bind('DOMMouseScroll', function(e){
      if(!isScrolling) {
        isScrolling = true;
         if(e.originalEvent.detail > 0) {
            console.log("scroll bottom");
            bottomFunction();
         }else {
            console.log("scroll top");
            topFunction();
         }

         //prevent page fom scrolling
         return false;
       }
   });

   //IE, Opera, Safari
   win.bind('mousewheel', function(e){
      if(!isScrolling) {
        isScrolling = true;
         if(e.originalEvent.wheelDelta < 0) {
            console.log("scroll bottom");
            bottomFunction();
         }else {
            console.log("scroll top");
            topFunction();
         }

         //prevent page fom scrolling
         return false;
       }
   });
}

/* Is elem into view (to check during scroll) */
function isScrolledIntoView(elem) {
  var verticalDecalage = 200;

  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();

  var elemTop = $(elem).offset().top + verticalDecalage;
  var elemBottom = elemTop + $(elem).height() + verticalDecalage;

  return (
    //((elemBottom <= docViewBottom) && (elemTop >= docViewTop)) ||
    //((elemBottom <= docViewBottom) && (elemBottom >= docViewTop)) ||
    //((elemTop <= docViewBottom) && (elemTop >= docViewTop))
    (elemTop <= docViewBottom)
    );
}

function scrollTo(target, decalage, speed) {
  $('html, body').animate({
    scrollTop: $(target).offset().top - decalage 
  }, {
    duration: speed,
    specialEasing: {
        scrollTop: "easeInOutQuart"
    }
  });
}